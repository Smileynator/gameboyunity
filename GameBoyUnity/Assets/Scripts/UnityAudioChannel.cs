using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityAudioChannel : MonoBehaviour, IGBAudioOutput
{
    private GBAudioBuffer buffer;
    private AudioSource audioSource;

    public void OnBufferInitialize(GBSettings settings, AudioChannel channel, ref GBAudioBuffer buffer)
    {
        this.buffer = buffer;

        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.spatialBlend = 0; //force 2D sound
            audioSource.volume = settings.masterVolume;
            if(!settings.audioChannelsOn[(int)channel])
            {
                audioSource.volume = 0;
            }
        }
    }
    
    private void OnAudioFilterRead(float[] data, int channels)
    {
        if (buffer == null)
            return;//not initialized
        
        int bufferSize = 0;
        int wait = 0;
        while (bufferSize < data.Length)
        {
            lock (buffer)
            {
                bufferSize = buffer.bufferPointer;
            }
            if(bufferSize < data.Length)
            {
                wait++;
                if (wait >= 5)
                {
                    return;//fuck it, not happening, take what you have instead
                }
                System.Threading.Thread.Sleep(1);
            }
        }
        lock (buffer) {
            Array.Copy(buffer.sampleBuffer, data, data.Length);
            int leftOver = buffer.bufferPointer - data.Length;
            Array.Copy(buffer.sampleBuffer, data.Length, buffer.sampleBuffer, 0, leftOver);
            buffer.bufferPointer = leftOver;
            if(buffer.bufferPointer > 4000)
            {
                buffer.bufferPointer -= 2;//drop some to prevent buffer overflow
            }
        }
    }
}
