﻿using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
#endif
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class DropdownScript : MonoBehaviour
#if UNITY_EDITOR
    , IPreprocessBuildWithReport
#endif
{
    public Dropdown dropdown;
    public UnityGBPlayer player;

    public List<string> results1 = new List<string>();

    public int callbackOrder => 0;

    void Start()
    {
#if UNITY_EDITOR
        UpdateList();
#endif
        dropdown.ClearOptions();
        dropdown.onValueChanged.AddListener(DropdownChanged);
        dropdown.AddOptions(results1);
    }

    private void DropdownChanged(int index)
    {
        if (index == 0)
            return;
        player.StartROM(results1[index]);
    }

    private void UpdateList()
    {
        results1.Clear();
        results1.Add("None");
        string path = Application.streamingAssetsPath;
        int lengthAssetPath = path.Length + 1;
        results1.AddRange(UnityGBPlayer.DirSearch(path, lengthAssetPath));
    }

#if UNITY_EDITOR
    public void OnPreprocessBuild(BuildReport report)
    {
        //UpdateList();
    //todo save?
    }
#endif
}
