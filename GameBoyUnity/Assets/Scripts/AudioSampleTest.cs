using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = System.Random;

public class AudioSampleTest : MonoBehaviour
{
    public int[] frequenciesInHz;
    public int creationSamplesPerSec = 4194304;
    [ReadOnly, ShowInInspector]
    private int consumeSamplesPerSec = 44100;
    public bool left = true, right = true, flip = false;
    public Transform readPoint, writePoint;
    public float fillBufferEvery = 1f;

    private int freqIndex = 0;
    
    private CircularAudioBuffer<float> buffer;
    private float[] writeBuffer = new float[1];
    private float[] readBuffer = new float[1];
    private int consumeSampleCounter;
    private float createTimer;
    
    private double resampleRatio;
    private System.Random random;

    private void Awake()
    {
        random = new System.Random();
        buffer = new CircularAudioBuffer<float>(consumeSamplesPerSec*3, 1);
        consumeSamplesPerSec = AudioSettings.outputSampleRate;
        resampleRatio = (double)creationSamplesPerSec / consumeSamplesPerSec;
    }
    
    void Update()
    {
        //visualize
        //readPoint.localPosition = Vector3.right * ((float)buffer.ReadPointer/buffer.BufferSize);
        //writePoint.localPosition = Vector3.right * ((float)buffer.WritePointer/buffer.BufferSize);

        //check if timer expired to simulate audio creation
        createTimer -= Time.deltaTime;
        //create a full second of sinewave, once every second
        if (createTimer <= 0f)
        {
            createTimer += fillBufferEvery;
            double ratioCounter = 0;
            double bufferEntry = 0f;
            //write buffer
            for (int i = 0; i < creationSamplesPerSec; i++)
            {
                bufferEntry += CreateAudio(i, frequenciesInHz[freqIndex], creationSamplesPerSec);
                ratioCounter++;
                if (ratioCounter >= resampleRatio)
                {
                    writeBuffer[0] = (float)(bufferEntry / (int)ratioCounter);
                    buffer.Write(ref writeBuffer);
                    bufferEntry = 0f;
                    ratioCounter -= resampleRatio;
                }
            }
            //last entry cleanup
            writeBuffer[0] = (float)(bufferEntry / (int)ratioCounter);
            buffer.Write(ref writeBuffer);
            //next freq
            freqIndex++;
            if (freqIndex >= frequenciesInHz.Length)
            {
                freqIndex = 0;
            }
        }
    }

    private void OnAudioFilterRead(float[] data, int channels)
    { 
        for (int i = 0; i < data.Length; i++)
        {
            int channel = i % channels;
            bool chanPicker = channel == 0;
            if (flip)
                chanPicker = !chanPicker;
            if (chanPicker)
            {
                consumeSampleCounter++;
                //generate a sinewave
                if(left)
                    data[i] = CreateAudio(consumeSampleCounter, frequenciesInHz[freqIndex], consumeSamplesPerSec);
            }
            else
            {
                //write to channel
                if(right)
                {
                    while (buffer.ReadBehindCounter == 0)
                    {
                        Thread.Sleep(1);
                    }
                    buffer.Read(ref readBuffer);
                    data[i] = readBuffer[0];
                }
            }
        }
        
        //Roll back the sample counter, to ensure we never wrap the number and make an audio pop.
        if (consumeSampleCounter >= consumeSamplesPerSec)
        {
            //a full second of samples passed, substract the counter and mark that part of the buffer read.
            consumeSampleCounter -= consumeSamplesPerSec;
        }
    }
    
    public float CreateAudio(int timeIndex, float frequency, int sampleRate)
    {
        return (random.Next(0, 2)*2)-1;
        //return Mathf.Sin(2 * Mathf.PI * timeIndex * frequency / sampleRate);
    }
}
