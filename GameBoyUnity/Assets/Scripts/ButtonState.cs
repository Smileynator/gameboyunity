using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonState : Button
{
    private bool isDown;
    public bool IsHeld => isDown;

    public override void OnPointerDown(PointerEventData eventData)
    {
        isDown = true;
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        isDown = false;
    }
}
