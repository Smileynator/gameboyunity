using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;


public class UnityGBJsonTests : MonoBehaviour
{
    public string cpuPath;
    public string startAtFile;
    public UnityButtonInput buttons;
    public UnityScreen screenRenderer;
    public UnityAudioChannel[] channels;
    public GBSettings settings;
    
    private GBMB motherboard;
    
    void Start () {
#if !DEBUGCPU
        Debug.LogError("You need to enable the DEBUGCPU define to run this test!");
#else
        motherboard = new GBMB(screenRenderer, buttons, channels, settings, null, null, null);
        StartCoroutine(ProcessTests());
#endif
    }

#if DEBUGCPU
    private IEnumerator ProcessTests()
    {
        var jsonFiles = EnumerateJsons(cpuPath);
        bool skipping = !string.IsNullOrEmpty(startAtFile);
        foreach (var jsonFile in jsonFiles)
        {
            if (skipping && Path.GetFileNameWithoutExtension(jsonFile) != startAtFile)
            {
                Debug.Log($"Test skipped: OPCODE {Path.GetFileNameWithoutExtension(jsonFile)}");
                continue;
            }
            else
            {
                skipping = false;
            }
            string json = File.ReadAllText(jsonFile);
            CPUJsonData[] statusBytes = JsonConvert.DeserializeObject<CPUJsonData[]>(json);
            int failed = 0;
            for (int i = 0; i < statusBytes.Length; i++)
            {
                CPUJsonData test = statusBytes[i];
                //set up the data
                InternalStatusBytes initStatus = new InternalStatusBytes(test.initial);
                //Set the values in the motherboard
                motherboard.SetTestingRegisters(initStatus);
                //Run CPU 1 clock
                motherboard.RunSingleCPUCycle();
                //Check validity
                InternalStatusBytes finalStatus = new InternalStatusBytes(test.final);
                InternalStatusBytes currentStatus = motherboard.GetTestingRegisters(finalStatus.ram);
                bool match = currentStatus.AreEqual(finalStatus);
                if (!match)
                {
                    Debug.LogError($"Test failed: {test.name}\n" +
                                   $"INI: A:{initStatus.a} B:{initStatus.b} C:{initStatus.c} D:{initStatus.d} E:{initStatus.e} F:{initStatus.f} H:{initStatus.h} L:{initStatus.l} SP:{initStatus.sp} PC:{initStatus.pc}\n" +
                                   $"EXP: A:{finalStatus.a} B:{finalStatus.b} C:{finalStatus.c} D:{finalStatus.d} E:{finalStatus.e} F:{finalStatus.f} H:{finalStatus.h} L:{finalStatus.l} SP:{finalStatus.sp} PC:{finalStatus.pc}\n" +
                                   $"CUR: A:{currentStatus.a} B:{currentStatus.b} C:{currentStatus.c} D:{currentStatus.d} E:{currentStatus.e} F:{currentStatus.f} H:{currentStatus.h} L:{currentStatus.l} SP:{currentStatus.sp} PC:{currentStatus.pc}");
                    failed++;
                }
            }

            if (failed != 0)
            {
                Debug.Log($"Test failed: OPCODE {Path.GetFileNameWithoutExtension(jsonFile)} Fails: {failed}/{statusBytes.Length}");
                break;
            }
            Debug.Log($"Test passed: OPCODE {Path.GetFileNameWithoutExtension(jsonFile)}");
            yield return null;
        }
    }
    
    private static IEnumerable<string> EnumerateJsons(string path)
    {
        if (Directory.Exists(path))
        {
            return Directory.EnumerateFiles(path, "*.json");
        }
        else
        {
            throw new DirectoryNotFoundException("Path does not exist.");
        }
    }
#endif
}

#if DEBUGCPU
[Serializable]
public class CPUJsonData
{
    public string name;
    public InternalStatus initial;
    public InternalStatus final;
}

[Serializable]
public class InternalStatus
{
    public CPUData cpu;
    public string[][] ram;
}

[Serializable]
public class CPUData
{
    public string a;
    public string b;
    public string c;
    public string d;
    public string e;
    public string f;
    public string h;
    public string l;
    public string pc;
    public string sp;
}

public class InternalStatusBytes
{
    public byte a;
    public byte b;
    public byte c;
    public byte d;
    public byte e;
    public byte f;
    public byte h;
    public byte l;
    public ushort pc;
    public ushort sp;
    public MemoryEntry[] ram;

    public InternalStatusBytes()
    {
        
    }
    
    public InternalStatusBytes(InternalStatus status)
    {
        a = ConvertHexToByte(status.cpu.a);
        b = ConvertHexToByte(status.cpu.b);
        c = ConvertHexToByte(status.cpu.c);
        d = ConvertHexToByte(status.cpu.d);
        e = ConvertHexToByte(status.cpu.e);
        f = ConvertHexToByte(status.cpu.f);
        h = ConvertHexToByte(status.cpu.h);
        l = ConvertHexToByte(status.cpu.l);
        pc = ConvertHexToShort(status.cpu.pc);
        sp = ConvertHexToShort(status.cpu.sp);
        ram = new MemoryEntry[status.ram.Length];
        for (int i = 0; i < status.ram.Length; i++)
        {
            string[] ramEntry = status.ram[i];
            ram[i] = new MemoryEntry()
            {
                address = ConvertHexToShort(ramEntry[0]),
                value = ConvertHexToByte(ramEntry[1])
            };
        }
    }

    private byte ConvertHexToByte(string hexValue)
    {
        return Convert.ToByte(hexValue, 16);
    }

    private ushort ConvertHexToShort(string hexValue)
    {
        return Convert.ToUInt16(hexValue, 16);
    }
    
    public bool AreEqual(InternalStatusBytes other)
    {
        if (a != other.a || b != other.b || c != other.c || d != other.d || e != other.e ||
            f != other.f || h != other.h || l != other.l || pc != other.pc || sp != other.sp)
        {
            return false;
        }

        if (ram.Length != other.ram.Length)
        {
            return false;
        }

        for (int i = 0; i < ram.Length; i++)
        {
            if (ram[i].address != other.ram[i].address || ram[i].value != other.ram[i].value)
            {
                return false;
            }
        }

        return true;
    }
}

public class MemoryEntry
{
    public ushort address;
    public byte value;
}
#endif