using UnityEngine;

public class ScaleToAspectMultiple : MonoBehaviour
{
    public RectTransform canvas;
    public RectTransform trans;
    public Vector2Int imageSize;
    public int maxMulti;
    private Vector2 lastScreen;
    
    void Update()
    {
        Vector2 screen = canvas.sizeDelta;
        if (screen.x != lastScreen.x)
        {
            int multiple = Mathf.FloorToInt(screen.x / imageSize.x);
            multiple = Mathf.Min(multiple, maxMulti);
            trans.sizeDelta = imageSize * multiple;
            lastScreen = screen;
        }
    }
}
