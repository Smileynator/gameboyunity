using System;

public interface IGBClock : IGBClockRead
{
    public void ClockMCycle();
}

public interface IGBClockRead
{
    public event Action MCycle;
}