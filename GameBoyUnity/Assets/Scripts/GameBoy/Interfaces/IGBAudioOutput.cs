using System;

public interface IGBAudioOutput
{
    /// <summary>
    /// Will provide you the audio data buffer that is pushed from the gameboy thread
    /// You can then read it when required.
    /// Use the buffer array as your Lock object to manage thread safety.
    /// </summary>
    public void OnBufferInitialize(GBSettings settings, AudioChannel channel, ref GBAudioBuffer buffer);
}

public class GBAudioBuffer
{
    public float[] sampleBuffer;
    public int bufferPointer;
}
