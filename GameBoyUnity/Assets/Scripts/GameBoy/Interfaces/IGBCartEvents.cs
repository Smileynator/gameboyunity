/// <summary>
/// Some cards have specific features that can be handled like rumble.
/// You can listen to these events through implementation of this interface.
/// </summary>
public interface IGBCartEvents
{
    /// <summary>
    /// Enables and disables rumble when the CPU calls for it.
    /// </summary>
    void SetRumble(bool enabled);
}
