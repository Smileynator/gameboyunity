using System;

public interface IGBMemory : IGBMemoryWrite, IGBMemoryRead
{
}

public interface IGBMemoryWrite
{
    public void Write(ushort address, byte data);
    public void WriteAsRegister(ushort address, byte data);
    public void SaveRAMNow();
    public void ClockSerialTransfer();
}

public interface IGBMemoryJoyPad
{
    public byte ReadJoyPadInput();
    public void WriteJoyPadInput(byte newInput);
}

public interface IGBMemoryPPU : IGBMemoryRead
{
    public void Write(ushort address, byte data);
    public void WriteAsRegister(ushort address, byte data);
    
    /// <summary>
    /// The PPU is allowed direct read privileges on OAM and VRAM, and graphics related registers
    /// </summary>
    public byte ReadPPU(ushort address);
    
    /// <summary>
    /// Temporary solution until we have everything properly decoupled
    /// </summary>
    event Action<ushort> TileMemoryChanged;
}

public interface IGBMemoryRead
{
    public bool HasBootRom { get; }
    public LCDControl LcdControl { get; }
    public byte Read(ushort address);
    public ReadOnlySpan<byte> GetMemorySegment(ushort beginAddress, int length);
    event Action<ushort, byte> AudioRegisterChanged;
    event Action ApuTick;
}