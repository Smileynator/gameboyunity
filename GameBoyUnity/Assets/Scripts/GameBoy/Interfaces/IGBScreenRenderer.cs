/// <summary>
/// These all get called from inside the gameboy thread
/// </summary>
public interface IGBScreenRenderer
{
    /// <summary>
    /// Initializes the screen with given width and height
    /// width*height is the screen buffer size
    /// </summary>
    public void OnInitialize(int width, int height);
    
    //TODO add more granular output for easier testing or not?
    
    /// <summary>
    /// Called when a full frame is ready for rendering
    /// </summary>
    public void OnFrameFinished(ref byte[] screenBuffer);
}
