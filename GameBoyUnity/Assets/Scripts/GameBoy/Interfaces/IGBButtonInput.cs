
public interface IGBButtonInput
{
    /// <summary>
    /// On calling, expects a filled out button state with the current "pressed" state for each button.
    /// </summary>
    public ButtonPressedStates GetButtonStates();
}