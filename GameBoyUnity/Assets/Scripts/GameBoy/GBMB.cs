﻿using System.Diagnostics;

/// <summary>
/// Couples all the gameboy bits together to make the main entrypoint of the gameboy emulator.
/// </summary>
public class GBMB {
    //TODO make this all private so we force interface to GBMB
    private readonly GBClock clock;
    private GBProcessor cpu;
    private GBMemory memory;
    private GBScreen screen;
    private GBInput input;
    private GBAudio audio;
    private GBSettings settings;

    private readonly Stopwatch cpuOverhead = new Stopwatch();
    private readonly Stopwatch outsideOverhead = new Stopwatch();
    private bool frameFinished = false;

    /// <summary>
    /// Initialize a new gameboy emulator.
    /// </summary>
    public GBMB(IGBScreenRenderer screenRenderer, IGBButtonInput buttons, IGBAudioOutput[] channels, IGBCartEvents cartEvents, GBSettings settings, string savePath, byte[] gameRom, byte[] bootRom = null)
    {
        this.settings = settings;
        clock = new GBClock();
        memory = new GBMemory(savePath, bootRom, gameRom, clock, cartEvents);
        screen = new GBScreen(memory, clock, screenRenderer);
        input = new GBInput(clock, memory, buttons);
        audio = new GBAudio(memory, clock, channels, settings);
        cpu = new GBProcessor(memory, clock);

        screen.FrameFinished += OnFrameFinished;

        cpuOverhead.Start();
    }

    /// <summary>
    /// properly cleans up the emulator internally (might be unnecessary and removed in the future)
    /// </summary>
    public void Destroy()
    {
        //clean up all internal references to anything we have.
        cpu = null;
        screen = null;
        input = null;
        memory = null;
        audio = null;
        settings = null;
    }

    /// <summary>
    /// Will run the processor of the gameboy emulator until 1 frame is finished
    /// </summary>
    public void ProcessFrame()
    {
        //Start frame time counter to measure CPU overhead
        outsideOverhead.Stop();
        cpuOverhead.Restart();
        UnityEngine.Profiling.Profiler.BeginSample("CPU Cycle");
        while (!frameFinished)
        {
            clock.ClockMCycle();
        }
        UnityEngine.Profiling.Profiler.EndSample();
    }

    /// <summary>
    /// To be called at the end of a full update loop, to sync framerate to match expected gameboy framerate.
    /// Can be ignored, but would run emulator at full blast every update.
    /// returns time it spend idle
    /// </summary>
    public double SyncFrameRate()
    {
        UnityEngine.Profiling.Profiler.BeginSample("Frame Sync Wait");
        double timeNeeded = settings.desiredFrameLength - outsideOverhead.Elapsed.TotalMilliseconds;
        double idleTime = 0;
        if (settings.limitFramerate && cpuOverhead.Elapsed.TotalMilliseconds < timeNeeded)
        {
            idleTime = timeNeeded - cpuOverhead.Elapsed.TotalMilliseconds;
            int sleepTime = (int)System.Math.Floor(idleTime) - 1;
            if (sleepTime > 0)
                System.Threading.Thread.Sleep(sleepTime);
            //Wait it out by force
            while (cpuOverhead.Elapsed.TotalMilliseconds < timeNeeded)
            {
                ;//Do absolutely nothing until we fit framerate
            }
        }
        frameFinished = false;
        outsideOverhead.Restart();
        UnityEngine.Profiling.Profiler.EndSample();
        return idleTime;
    }

    /// <summary>
    /// Writes the current ram data to disk immediately
    /// </summary>
    public void WriteSavedataToDisk()
    {
        memory.SaveRAMNow();
    }

    private void OnFrameFinished()
    {
        frameFinished = true;
    }
}
