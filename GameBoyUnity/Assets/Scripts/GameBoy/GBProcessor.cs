﻿using System.Runtime.CompilerServices;
using UnityEngine;

public class GBProcessor
{
    private readonly IGBMemory memory;
    private readonly IGBClockRead clock;

    private int mCyclesToWait;

#region Flags
    //ABDH = MSB, FCEL = LSB
    private byte Reg_A, Reg_B, Reg_C, Reg_D, Reg_E, Reg_H, Reg_L;
    //Register F has some special behaviour, so we implement that here.
    private byte Reg_F
    {
        get => _regf;
        set => _regf = (byte) (value & 0xF0);
    }

    private byte _regf;

    public ushort AF
    {
        get => (ushort)((Reg_A << 8) + Reg_F);
        private set
        {
            Reg_F = (byte)value;
            Reg_A = (byte)(value >> 8);
        }
    }

    public ushort BC {
        get => (ushort)((Reg_B << 8) + Reg_C);
        private set
        {
            Reg_C = (byte) value;
            Reg_B = (byte) (value >> 8);
        }
    }

    public ushort DE
    {
        get => (ushort)((Reg_D << 8) + Reg_E);
        private set
        {
            Reg_D = (byte)(value >> 8);
            Reg_E = (byte)value;
        }
    }

    public ushort HL
    {
        get => (ushort)((Reg_H << 8) + Reg_L);
        private set
        {
            Reg_H = (byte)(value >> 8);
            Reg_L = (byte)value;
        }
    }

    private ushort PC;//Program counter
    public ushort CheckPC => PC;
    
    private ushort SP;//Stack Pointer
    public ushort CheckSP => SP;

    private bool Z_zeroFlag
    {
        get => (Reg_F & 0x80)!= 0;
        set
        {
            if (value)
                Reg_F |= 0x80;
            else
                Reg_F &= 0x7F;
        }
    }

    private bool N_subFlag
    {
        get => (Reg_F & 0x40)!= 0;
        set
        {
            if (value)
                Reg_F |= 0x40;
            else
                Reg_F &= 0xBF;
        }
    }

    private bool H_HCFlag
    {
        get => (Reg_F & 0x20)!= 0;
        set
        {
            if (value)
                Reg_F |= 0x20;
            else
                Reg_F &= 0xDF;
        }
    }

    private bool C_FCFlag
    {
        get => (Reg_F & 0x10)!= 0;
        set
        {
            if (value)
                Reg_F |= 0x10;
            else
                Reg_F &= 0xEF;
        }
    }
#endregion

    private int interruptMasterEnableState;
    private bool interruptMasterEnableFlag;
    public bool CheckIMEFlag => interruptMasterEnableFlag;
    private bool isHalted;
    public bool CheckHalted => isHalted;
    private bool pendingInterruptFlag;

    public GBProcessor(IGBMemory memory, IGBClockRead clock)
    {
        this.memory = memory;
        this.clock = clock;
        clock.MCycle += MachineClocked;
        InitiateCPU(memory.HasBootRom);
    }

    ~GBProcessor()
    {
        clock.MCycle -= MachineClocked;
    }

    private void InitiateCPU(bool playBootRom)
    {
        if (playBootRom)
        {
            PC = 0x0000;
            SP = 0xFFFE;
        }
        else
        {
            PC = 0x0100;
            AF = 0x01B0;
            BC = 0x0013;
            DE = 0x00D8;
            HL = 0x014D;
            SP = 0xFFFE;
        }
    }
    
    /// <summary>
    /// Runs the processor for 1 operation.
    /// </summary>
    private void MachineClocked()
    {
        if (mCyclesToWait > 0)
        {
            mCyclesToWait--;
            return;
        }

        if (isHalted)
        {
            CheckStopHalting();
        }
        UpdateInterruptMasterFlag();
        if (!pendingInterruptFlag)
        {
            bool didInterrupt = CheckAnyInterrupt();
            if (didInterrupt)
                return;
        }
        else
        {
            ExecuteInterrupt();
            return;
        }

        if (!isHalted) //If we are halted, we execute NO CODE.
        {
            RunOpcode();
        }
        //The first cycle ends here (causing the other 3+ cycles to count down after this)
        mCyclesToWait--;
    }

    private bool CheckAnyInterrupt()
    {
        if (!interruptMasterEnableFlag) 
            return false;
        byte interruptTriggered = (byte) (memory.Read(0xFF0F) & 0b11111);
        byte interruptFlags = (byte)(interruptTriggered & memory.Read(0xFFFF));
        bool anyInterrupt = (interruptFlags & interruptTriggered) != 0;
        if (anyInterrupt)
        {
            //We don't need to wait the current cycle
            mCyclesToWait = 3;
            pendingInterruptFlag = true;
        }
        return anyInterrupt;
    }
    
    private void ExecuteInterrupt()
    {
        pendingInterruptFlag = false;
        interruptMasterEnableFlag = false;
        PushStack(PC);
        
        byte interruptTriggered = memory.Read(0xFF0F);
        byte interruptFlags = (byte)(interruptTriggered & memory.Read(0xFFFF));
        if ((interruptFlags & (byte)Interrupt.V_Blank) != 0)
        {
            memory.Write(0xFF0F, (byte)(interruptTriggered ^ (byte)Interrupt.V_Blank));
            PC = 0x40;
            return;
        }
        if ((interruptFlags & (byte)Interrupt.STAT_LCDC) != 0)
        {
            memory.Write(0xFF0F, (byte)(interruptTriggered ^ (byte)Interrupt.STAT_LCDC));
            PC = 0x48;
            return;
        }
        if ((interruptFlags & (byte)Interrupt.TimerOverflow) != 0)
        {
            memory.Write(0xFF0F, (byte)(interruptTriggered ^ (byte)Interrupt.TimerOverflow));
            PC = 0x50;
            return;
        }
        if ((interruptFlags & (byte)Interrupt.SerialTransfer) != 0)
        {
            memory.Write(0xFF0F, (byte)(interruptTriggered ^ (byte)Interrupt.SerialTransfer));
            PC = 0x58;
            return;
        }
        if ((interruptFlags & (byte)Interrupt.Joypad) != 0)
        {
            memory.Write(0xFF0F, (byte)(interruptTriggered ^ (byte)Interrupt.Joypad));
            PC = 0x60;
        }
    }

    private void CheckStopHalting()
    {
        //Halting NOOP
        byte interruptTriggered = (byte) (memory.Read(0xFF0F) & 0b11111);
        byte interruptFlags = (byte)(interruptTriggered & memory.Read(0xFFFF));
        if(interruptFlags != 0)
        {
            isHalted = false;
        }
    }

    private void UpdateInterruptMasterFlag()
    {
        if (interruptMasterEnableState == 1)
        {
            interruptMasterEnableState++;
            return;
        }
        if (interruptMasterEnableState == 2)
        {
            interruptMasterEnableFlag = true;
            interruptMasterEnableState = 0;
        }
    }

    private void RunOpcode()
    {
        //get opcode
            byte OP = ReadImmediateByte();
            //run opcode
            switch (OP)
            {
                case 0x00://the real NO OP
                    mCyclesToWait = 1;
                    break;
                //LD A into HL and -- 
                case 0x3A:
                    WriteByte(CPUReg8.A, memory.Read(HL));
                    HL--;
                    mCyclesToWait = 2;
                    break;
                //LD A into HL and ++ 
                case 0x2A:
                    WriteByte(CPUReg8.A, memory.Read(HL));
                    HL++;
                    mCyclesToWait = 2;
                    break;
                //LD HL into A and -- 
                case 0x32:
                    memory.Write(HL, Reg_A);
                    HL--;
                    mCyclesToWait = 2;
                    break;
                //LD HL into A and ++ 
                case 0x22:
                    memory.Write(HL, Reg_A);
                    HL++;
                    mCyclesToWait = 2;
                    break;
                //LD HL into SP
                case 0xF9:
                    SP = HL;
                    mCyclesToWait = 2;
                    break;
                //LD SP + immediate into HL
                case 0xF8:
                    {
                        ushort immediate = (ushort)(sbyte)ReadImmediateByte();
                        HL = (ushort)(SP + immediate);
                        Z_zeroFlag = false;
                        N_subFlag = false;
                        H_HCFlag = ((SP & 0x0F) + (immediate & 0x0F) & 0x10) != 0;
                        C_FCFlag = ((SP & 0xFF) + (immediate & 0xFF) & 0x0100) != 0;
                        mCyclesToWait = 3;
                    }
                    break;
                //LD SP to immediate 16bit address and the next.
                case 0x08:
                    {
                        ushort immediate = ReadImmediateShort();
                        memory.Write(immediate, ReadLSB(CPUReg16.SP));
                        memory.Write((ushort)(immediate + 1), ReadMSB(CPUReg16.SP));
                    }
                    mCyclesToWait = 5;
                    break;
                //16bit ADD
                case 0x09:
                    ADD_16_ToHL(BC);
                    mCyclesToWait = 2;
                    break;
                case 0x19:
                    ADD_16_ToHL(DE);
                    mCyclesToWait = 2;
                    break;
                case 0x29:
                    ADD_16_ToHL(HL); //expensive no-op i guess, but it sets flags
                    mCyclesToWait = 2;
                    break;
                case 0x39:
                    ADD_16_ToHL(SP);
                    mCyclesToWait = 2;
                    break;
                //LD immediate into registers
                case 0x06:
                    WriteByte(CPUReg8.B, ReadImmediateByte());
                    mCyclesToWait = 2;
                    break;
                case 0x0E:
                    WriteByte(CPUReg8.C, ReadImmediateByte());
                    mCyclesToWait = 2;
                    break;
                case 0x16:
                    WriteByte(CPUReg8.D, ReadImmediateByte());
                    mCyclesToWait = 2;
                    break;
                case 0x1E:
                    WriteByte(CPUReg8.E, ReadImmediateByte());
                    mCyclesToWait = 2;
                    break;
                case 0x26:
                    WriteByte(CPUReg8.H, ReadImmediateByte());
                    mCyclesToWait = 2;
                    break;
                case 0x2E:
                    WriteByte(CPUReg8.L, ReadImmediateByte());
                    mCyclesToWait = 2;
                    break;
                //LD A into 16bit adresses and back
                case 0x02:
                    memory.Write(BC, Reg_A);
                    mCyclesToWait = 2;
                    break;
                case 0x12:
                    memory.Write(DE, Reg_A);
                    mCyclesToWait = 2;
                    break;
                case 0xEA:
                    memory.Write(ReadImmediateShort(), Reg_A);
                    mCyclesToWait = 4;
                    break;
                case 0xFA:
                    WriteByte(CPUReg8.A, memory.Read(ReadImmediateShort()));
                    mCyclesToWait = 4;
                    break;
                //VAL INTO A reg
                case 0x78:
                    Reg_A = Reg_B;
                    mCyclesToWait = 1;
                    break;
                case 0x79:
                    Reg_A = Reg_C;
                    mCyclesToWait = 1;
                    break;
                case 0x7A:
                    Reg_A = Reg_D;
                    mCyclesToWait = 1;
                    break;
                case 0x7B:
                    Reg_A = Reg_E;
                    mCyclesToWait = 1;
                    break;
                case 0x7C:
                    Reg_A = Reg_H;
                    mCyclesToWait = 1;
                    break;
                case 0x7D:
                    Reg_A = Reg_L;
                    mCyclesToWait = 1;
                    break;
                case 0x7E:
                    Reg_A = memory.Read(HL);
                    mCyclesToWait = 2;
                    break;
                case 0x7F:
                    //Reg_A = Reg_A;
                    mCyclesToWait = 1;
                    break;
                case 0x0A:
                    Reg_A = memory.Read(BC);
                    mCyclesToWait = 2;
                    break;
                case 0x1A:
                    Reg_A = memory.Read(DE);
                    mCyclesToWait = 2;
                    break;
                case 0x3E:
                    Reg_A = ReadImmediateByte();
                    mCyclesToWait = 2;
                    break;
                //TO B
                case 0x40:
                    //Reg_B = Reg_B;
                    mCyclesToWait = 1;
                    break;
                case 0x41:
                    Reg_B = Reg_C;
                    mCyclesToWait = 1;
                    break;
                case 0x42:
                    Reg_B = Reg_D;
                    mCyclesToWait = 1;
                    break;
                case 0x43:
                    Reg_B = Reg_E;
                    mCyclesToWait = 1;
                    break;
                case 0x44:
                    Reg_B = Reg_H;
                    mCyclesToWait = 1;
                    break;
                case 0x45:
                    Reg_B = Reg_L;
                    mCyclesToWait = 1;
                    break;
                case 0x46:
                    Reg_B = memory.Read(HL);
                    mCyclesToWait = 2;
                    break;
                case 0x47:
                    Reg_B = Reg_A;
                    mCyclesToWait = 1;
                    break;
                //TO C
                case 0x48:
                    Reg_C = Reg_B;
                    mCyclesToWait = 1;
                    break;
                case 0x49:
                    //Reg_C = Reg_C;
                    mCyclesToWait = 1;
                    break;
                case 0x4A:
                    Reg_C = Reg_D;
                    mCyclesToWait = 1;
                    break;
                case 0x4B:
                    Reg_C = Reg_E;
                    mCyclesToWait = 1;
                    break;
                case 0x4C:
                    Reg_C = Reg_H;
                    mCyclesToWait = 1;
                    break;
                case 0x4D:
                    Reg_C = Reg_L;
                    mCyclesToWait = 1;
                    break;
                case 0x4E:
                    Reg_C = memory.Read(HL);
                    mCyclesToWait = 2;
                    break;
                case 0x4F:
                    Reg_C = Reg_A;
                    mCyclesToWait = 1;
                    break;
                //TO D
                case 0x50:
                    Reg_D = Reg_B;
                    mCyclesToWait = 1;
                    break;
                case 0x51:
                    Reg_D = Reg_C;
                    mCyclesToWait = 1;
                    break;
                case 0x52:
                    //Reg_D = Reg_D;
                    mCyclesToWait = 1;
                    break;
                case 0x53:
                    Reg_D = Reg_E;
                    mCyclesToWait = 1;
                    break;
                case 0x54:
                    Reg_D = Reg_H;
                    mCyclesToWait = 1;
                    break;
                case 0x55:
                    Reg_D = Reg_L;
                    mCyclesToWait = 1;
                    break;
                case 0x56:
                    Reg_D = memory.Read(HL);
                    mCyclesToWait = 2;
                    break;
                case 0x57:
                    Reg_D = Reg_A;
                    mCyclesToWait = 1;
                    break;
                //TO E
                case 0x58:
                    Reg_E = Reg_B;
                    mCyclesToWait = 1;
                    break;
                case 0x59:
                    Reg_E = Reg_C;
                    mCyclesToWait = 1;
                    break;
                case 0x5A:
                    Reg_E = Reg_D;
                    mCyclesToWait = 1;
                    break;
                case 0x5B:
                    //Reg_E = Reg_E;
                    mCyclesToWait = 1;
                    break;
                case 0x5C:
                    Reg_E = Reg_H;
                    mCyclesToWait = 1;
                    break;
                case 0x5D:
                    Reg_E = Reg_L;
                    mCyclesToWait = 1;
                    break;
                case 0x5E:
                    Reg_E = memory.Read(HL);
                    mCyclesToWait = 2;
                    break;
                case 0x5F:
                    Reg_E = Reg_A;
                    mCyclesToWait = 1;
                    break;
                //TO H
                case 0x60:
                    Reg_H = Reg_B;
                    mCyclesToWait = 1;
                    break;
                case 0x61:
                    Reg_H = Reg_C;
                    mCyclesToWait = 1;
                    break;
                case 0x62:
                    Reg_H = Reg_D;
                    mCyclesToWait = 1;
                    break;
                case 0x63:
                    Reg_H = Reg_E;
                    mCyclesToWait = 1;
                    break;
                case 0x64:
                    //Reg_H = Reg_H;
                    mCyclesToWait = 1;
                    break;
                case 0x65:
                    Reg_H = Reg_L;
                    mCyclesToWait = 1;
                    break;
                case 0x66:
                    Reg_H = memory.Read(HL);
                    mCyclesToWait = 2;
                    break;
                case 0x67:
                    Reg_H = Reg_A;
                    mCyclesToWait = 1;
                    break;
                //TO L
                case 0x68:
                    Reg_L = Reg_B;
                    mCyclesToWait = 1;
                    break;
                case 0x69:
                    Reg_L = Reg_C;
                    mCyclesToWait = 1;
                    break;
                case 0x6A:
                    Reg_L = Reg_D;
                    mCyclesToWait = 1;
                    break;
                case 0x6B:
                    Reg_L = Reg_E;
                    mCyclesToWait = 1;
                    break;
                case 0x6C:
                    Reg_L = Reg_H;
                    mCyclesToWait = 1;
                    break;
                case 0x6D:
                    //Reg_L = Reg_L;
                    mCyclesToWait = 1;
                    break;
                case 0x6E:
                    Reg_L = memory.Read(HL);
                    mCyclesToWait = 2;
                    break;
                case 0x6F:
                    Reg_L = Reg_A;
                    mCyclesToWait = 1;
                    break;
                //TO (HL)
                case 0x70:
                    memory.Write(HL, Reg_B);
                    mCyclesToWait = 2;
                    break;
                case 0x71:
                    memory.Write(HL, Reg_C);
                    mCyclesToWait = 2;
                    break;
                case 0x72:
                    memory.Write(HL, Reg_D);
                    mCyclesToWait = 2;
                    break;
                case 0x73:
                    memory.Write(HL, Reg_E);
                    mCyclesToWait = 2;
                    break;
                case 0x74:
                    memory.Write(HL, Reg_H);
                    mCyclesToWait = 2;
                    break;
                case 0x75:
                    memory.Write(HL, Reg_L);
                    mCyclesToWait = 2;
                    break;
                case 0x77:
                    memory.Write(HL, Reg_A);
                    mCyclesToWait = 2;
                    break;
                case 0x36:
                    memory.Write(HL, ReadImmediateByte());
                    mCyclesToWait = 3;
                    break;
                //ADD to A
                case 0x80:
                    ADD_A(Reg_B);
                    mCyclesToWait = 1;
                    break;
                case 0x81:
                    ADD_A(Reg_C);
                    mCyclesToWait = 1;
                    break;
                case 0x82:
                    ADD_A(Reg_D);
                    mCyclesToWait = 1;
                    break;
                case 0x83:
                    ADD_A(Reg_E);
                    mCyclesToWait = 1;
                    break;
                case 0x84:
                    ADD_A(Reg_H);
                    mCyclesToWait = 1;
                    break;
                case 0x85:
                    ADD_A(Reg_L);
                    mCyclesToWait = 1;
                    break;
                case 0x86:
                    ADD_A(ReadByte(CPUReg8.HLasPTR));
                    mCyclesToWait = 2;
                    break;
                case 0x87:
                    ADD_A(Reg_A);
                    mCyclesToWait = 1;
                    break;
                case 0xC6:
                    ADD_A(ReadByte(CPUReg8.ImmediateValue));
                    mCyclesToWait = 2;
                    break;

                //ADC add to A with carryflag.
                case 0x88:
                    ADC_A(CPUReg8.B);
                    mCyclesToWait = 1;
                    break;
                case 0x89:
                    ADC_A(CPUReg8.C);
                    mCyclesToWait = 1;
                    break;
                case 0x8A:
                    ADC_A(CPUReg8.D);
                    mCyclesToWait = 1;
                    break;
                case 0x8B:
                    ADC_A(CPUReg8.E);
                    mCyclesToWait = 1;
                    break;
                case 0x8C:
                    ADC_A(CPUReg8.H);
                    mCyclesToWait = 1;
                    break;
                case 0x8D:
                    ADC_A(CPUReg8.L);
                    mCyclesToWait = 1;
                    break;
                case 0x8E:
                    ADC_A(CPUReg8.HLasPTR);
                    mCyclesToWait = 2;
                    break;
                case 0x8F:
                    ADC_A(CPUReg8.A);
                    mCyclesToWait = 1;
                    break;
                case 0xCE:
                    ADC_A(CPUReg8.ImmediateValue);
                    mCyclesToWait = 2;
                    break;

                //SUB from A
                case 0x90:
                    SUB_A(CPUReg8.B);
                    mCyclesToWait = 1;
                    break;
                case 0x91:
                    SUB_A(CPUReg8.C);
                    mCyclesToWait = 1;
                    break;
                case 0x92:
                    SUB_A(CPUReg8.D);
                    mCyclesToWait = 1;
                    break;
                case 0x93:
                    SUB_A(CPUReg8.E);
                    mCyclesToWait = 1;
                    break;
                case 0x94:
                    SUB_A(CPUReg8.H);
                    mCyclesToWait = 1;
                    break;
                case 0x95:
                    SUB_A(CPUReg8.L);
                    mCyclesToWait = 1;
                    break;
                case 0x96:
                    SUB_A(CPUReg8.HLasPTR);
                    mCyclesToWait = 2;
                    break;
                case 0x97:
                    SUB_A(CPUReg8.A);
                    mCyclesToWait = 1;
                    break;
                case 0xD6:
                    SUB_A(CPUReg8.ImmediateValue);
                    mCyclesToWait = 2;
                    break;

                //ADC add to A with carryflag.
                case 0x98:
                    SBC_A(CPUReg8.B);
                    mCyclesToWait = 1;
                    break;
                case 0x99:
                    SBC_A(CPUReg8.C);
                    mCyclesToWait = 1;
                    break;
                case 0x9A:
                    SBC_A(CPUReg8.D);
                    mCyclesToWait = 1;
                    break;
                case 0x9B:
                    SBC_A(CPUReg8.E);
                    mCyclesToWait = 1;
                    break;
                case 0x9C:
                    SBC_A(CPUReg8.H);
                    mCyclesToWait = 1;
                    break;
                case 0x9D:
                    SBC_A(CPUReg8.L);
                    mCyclesToWait = 1;
                    break;
                case 0x9E:
                    SBC_A(CPUReg8.HLasPTR);
                    mCyclesToWait = 2;
                    break;
                case 0x9F:
                    SBC_A(CPUReg8.A);
                    mCyclesToWait = 1;
                    break;
                case 0xDE:
                    SBC_A(CPUReg8.ImmediateValue);
                    mCyclesToWait = 2;
                    break;

                //16 bit loads
                case 0x01:
                    BC = ReadImmediateShort();
                    mCyclesToWait = 3;
                    break;
                case 0x11:
                    DE = ReadImmediateShort();
                    mCyclesToWait = 3;
                    break;
                case 0x21:
                    HL = ReadImmediateShort();
                    mCyclesToWait = 3;
                    break;
                case 0x31:
                    SP = ReadImmediateShort();
                    mCyclesToWait = 3;
                    break;

                //AND A reg
                case 0xA0:
                    AND_A(CPUReg8.B);
                    mCyclesToWait = 1;
                    break;
                case 0xA1:
                    AND_A(CPUReg8.C);
                    mCyclesToWait = 1;
                    break;
                case 0xA2:
                    AND_A(CPUReg8.D);
                    mCyclesToWait = 1;
                    break;
                case 0xA3:
                    AND_A(CPUReg8.E);
                    mCyclesToWait = 1;
                    break;
                case 0xA4:
                    AND_A(CPUReg8.H);
                    mCyclesToWait = 1;
                    break;
                case 0xA5:
                    AND_A(CPUReg8.L);
                    mCyclesToWait = 1;
                    break;
                case 0xA6:
                    AND_A(CPUReg8.HLasPTR);
                    mCyclesToWait = 2;
                    break;
                case 0xA7:
                    AND_A(CPUReg8.A);
                    mCyclesToWait = 1;
                    break;
                case 0xE6:
                    AND_A(CPUReg8.ImmediateValue);
                    mCyclesToWait = 2;
                    break;

                //OR A reg
                case 0xB0:
                    OR_A(CPUReg8.B);
                    mCyclesToWait = 1;
                    break;
                case 0xB1:
                    OR_A(CPUReg8.C);
                    mCyclesToWait = 1;
                    break;
                case 0xB2:
                    OR_A(CPUReg8.D);
                    mCyclesToWait = 1;
                    break;
                case 0xB3:
                    OR_A(CPUReg8.E);
                    mCyclesToWait = 1;
                    break;
                case 0xB4:
                    OR_A(CPUReg8.H);
                    mCyclesToWait = 1;
                    break;
                case 0xB5:
                    OR_A(CPUReg8.L);
                    mCyclesToWait = 1;
                    break;
                case 0xB6:
                    OR_A(CPUReg8.HLasPTR);
                    mCyclesToWait = 2;
                    break;
                case 0xB7:
                    OR_A(CPUReg8.A);
                    mCyclesToWait = 1;
                    break;
                case 0xF6:
                    OR_A(CPUReg8.ImmediateValue);
                    mCyclesToWait = 2;
                    break;

                //XOR a reg with reg A
                case 0xA8://B
                    XOR_A(CPUReg8.B);
                    mCyclesToWait = 1;
                    break;
                case 0xA9://C
                    XOR_A(CPUReg8.C);
                    mCyclesToWait = 1;
                    break;
                case 0xAA://D
                    XOR_A(CPUReg8.D);
                    mCyclesToWait = 1;
                    break;
                case 0xAB://E
                    XOR_A(CPUReg8.E);
                    mCyclesToWait = 1;
                    break;
                case 0xAC://H
                    XOR_A(CPUReg8.H);
                    mCyclesToWait = 1;
                    break;
                case 0xAD://L
                    XOR_A(CPUReg8.L);
                    mCyclesToWait = 1;
                    break;
                case 0xAE://(HL)
                    XOR_A(CPUReg8.HLasPTR);
                    mCyclesToWait = 2;
                    break;
                case 0xAF://A
                    XOR_A(CPUReg8.A);
                    mCyclesToWait = 1;
                    break;
                case 0xEE:
                    XOR_A(CPUReg8.ImmediateValue);
                    mCyclesToWait = 2;
                    break;

                //INC reg 16bit
                case 0x03:
                    BC++;
                    mCyclesToWait = 2;
                    break;
                case 0x13:
                    DE++;
                    mCyclesToWait = 2;
                    break;
                case 0x23:
                    HL++;
                    mCyclesToWait = 2;
                    break;
                case 0x33:
                    SP++;
                    mCyclesToWait = 2;
                    break;
                //DEC reg 16bit
                case 0x0B:
                    BC--;
                    mCyclesToWait = 2;
                    break;
                case 0x1B:
                    DE--;
                    mCyclesToWait = 2;
                    break;
                case 0x2B:
                    HL--;
                    mCyclesToWait = 2;
                    break;
                case 0x3B:
                    SP--;
                    mCyclesToWait = 2;
                    break;
                //CP Compare with A
                case 0xB8:
                    COMP(Reg_B);
                    mCyclesToWait = 1;
                    break;
                case 0xB9:
                    COMP(Reg_C);
                    mCyclesToWait = 1;
                    break;
                case 0xBA:
                    COMP(Reg_D);
                    mCyclesToWait = 1;
                    break;
                case 0xBB:
                    COMP(Reg_E);
                    mCyclesToWait = 1;
                    break;
                case 0xBC:
                    COMP(Reg_H);
                    mCyclesToWait = 1;
                    break;
                case 0xBD:
                    COMP(Reg_L);
                    mCyclesToWait = 1;
                    break;
                case 0xBE:
                    COMP(ReadByte(CPUReg8.HLasPTR));
                    mCyclesToWait = 2;
                    break;
                case 0xBF:
                    COMP(Reg_A);
                    mCyclesToWait = 1;
                    break;
                case 0xFE:
                    COMP(ReadByte(CPUReg8.ImmediateValue));
                    mCyclesToWait = 2;
                    break;

                //INC reg 8bit
                case 0x3C:
                    INC(CPUReg8.A);
                    mCyclesToWait = 1;
                    break;
                case 0x04:
                    INC(CPUReg8.B);
                    mCyclesToWait = 1;
                    break;
                case 0x0C:
                    INC(CPUReg8.C);
                    mCyclesToWait = 1;
                    break;
                case 0x14:
                    INC(CPUReg8.D);
                    mCyclesToWait = 1;
                    break;
                case 0x1C:
                    INC(CPUReg8.E);
                    mCyclesToWait = 1;
                    break;
                case 0x24:
                    INC(CPUReg8.H);
                    mCyclesToWait = 1;
                    break;
                case 0x2C:
                    INC(CPUReg8.L);
                    mCyclesToWait = 1;
                    break;
                case 0x34:
                    INC(CPUReg8.HLasPTR);
                    mCyclesToWait = 3;
                    break;
                //DEC reg 8bit
                case 0x3D:
                    DEC(CPUReg8.A);
                    mCyclesToWait = 1;
                    break;
                case 0x05:
                    DEC(CPUReg8.B);
                    mCyclesToWait = 1;
                    break;
                case 0x0D:
                    DEC(CPUReg8.C);
                    mCyclesToWait = 1;
                    break;
                case 0x15:
                    DEC(CPUReg8.D);
                    mCyclesToWait = 1;
                    break;
                case 0x1D:
                    DEC(CPUReg8.E);
                    mCyclesToWait = 1;
                    break;
                case 0x25:
                    DEC(CPUReg8.H);
                    mCyclesToWait = 1;
                    break;
                case 0x2D:
                    DEC(CPUReg8.L);
                    mCyclesToWait = 1;
                    break;
                case 0x35:
                    DEC(CPUReg8.HLasPTR);
                    mCyclesToWait = 3;
                    break;

                #region JUMPS AND CALLS
                //JP Immediate
                case 0xC3:
                    PC = ReadImmediateShort();
                    mCyclesToWait = 4;
                    break;
                //JP Immediate conditions
                case 0xC2:
                    {
                        ushort jmp = ReadImmediateShort();
                        if (!Z_zeroFlag)
                        {
                            PC = jmp;
                            mCyclesToWait = 4;
                        }
                        else
                            mCyclesToWait = 3;
                        break;
                    }
                case 0xCA:
                    {
                        ushort jmp = ReadImmediateShort();
                        if (Z_zeroFlag)
                        {
                            PC = jmp;
                            //Debug.LogWarning("JZI " + PC.ToString("X4"));
                            mCyclesToWait = 4;
                        }
                        else
                            mCyclesToWait = 3;
                        break;
                    }
                case 0xD2:
                    {
                        ushort jmp = ReadImmediateShort();
                        if (!C_FCFlag)
                        {
                            PC = jmp;
                            //Debug.LogWarning("JNCI " + PC.ToString("X4"));
                            mCyclesToWait = 4;
                        }
                        else
                            mCyclesToWait = 3;
                        break;
                    }
                case 0xDA:
                    {
                        ushort jmp = ReadImmediateShort();
                        if (C_FCFlag)
                        {
                            PC = jmp;
                            //Debug.LogWarning("JCI " + PC.ToString("X4"));
                            mCyclesToWait = 4;
                        }
                        else
                            mCyclesToWait = 3;
                        break;
                    }
                //JR jump to HL
                case 0xE9:
                    PC = HL;
                    //Debug.Log("JMP " + PC.ToString("X4"));
                    mCyclesToWait = 1;
                    break;
                //JR add immediate value to PC
                case 0x18:
                    {
                        ushort s = (ushort)(sbyte)ReadImmediateByte();
                        PC += s;
                        //Debug.LogWarning("JMP " + PC.ToString("X4"));
                        mCyclesToWait = 3;
                        break;
                    }
                //JR (Jump is flag conidition is met)
                case 0x20:
                    {
                        ushort s = (ushort)(sbyte)ReadImmediateByte();//convert a byte to a value that makes it -129 to +128
                        if (!Z_zeroFlag)
                        {
                            PC += s;
                            //Debug.LogWarning("JNZ " + PC.ToString("X4"));
                            mCyclesToWait += 1;
                        }
                        mCyclesToWait += 2;
                        break;
                    }
                case 0x28:
                    {
                        ushort s = (ushort)(sbyte)ReadImmediateByte();
                        if (Z_zeroFlag)
                        {
                            PC += s;
                            //Debug.LogWarning("JZ " + PC.ToString("X4"));
                            mCyclesToWait += 1;
                        }
                        mCyclesToWait += 2;
                        break;
                    }
                case 0x30:
                    {
                        ushort s = (ushort)(sbyte)ReadImmediateByte();
                        if (!C_FCFlag)
                        {
                            PC += s;
                            //Debug.LogWarning("JNC " + PC.ToString("X4"));
                            mCyclesToWait += 1;
                        }
                        mCyclesToWait += 2;
                        break;
                    }
                case 0x38:
                    {
                        ushort s = (ushort)(sbyte)ReadImmediateByte();
                        if (C_FCFlag)
                        {
                            PC += s;
                            //Debug.LogWarning("JC " + PC.ToString("X4"));
                            mCyclesToWait += 1;
                        }
                        mCyclesToWait += 2;
                        break;
                    }
                //CALL Push and Jump
                case 0xCD://get point to jump to, then push the PC
                    {
                        ushort jmp = ReadImmediateShort();
                        //write to stack
                        PushStack(PC);
                        //jump
                        PC = jmp;
                        //Debug.LogWarning("CALLJ " + PC.ToString("X4"));
                        mCyclesToWait = 6;
                        break;
                    }
                //CALL with JMP conditions
                case 0xC4:
                    {
                        ushort jmp = ReadImmediateShort();
                        if (!Z_zeroFlag)
                        {
                            //write to stack
                            PushStack(PC);
                            //jump
                            PC = jmp;
                            //Debug.LogWarning("CALLJNZ " + PC.ToString("X4"));
                            mCyclesToWait = 3;
                        }
                        mCyclesToWait += 3;
                        break;
                    }
                case 0xCC:
                    {
                        ushort jmp = ReadImmediateShort();
                        if (Z_zeroFlag)
                        {
                            //write to stack
                            PushStack(PC);
                            //jump
                            PC = jmp;
                            //Debug.LogWarning("CALLJZ " + PC.ToString("X4"));
                            mCyclesToWait = 3;
                        }
                        mCyclesToWait += 3;
                        break;
                    }
                case 0xD4:
                    {
                        ushort jmp = ReadImmediateShort();
                        if (!C_FCFlag)
                        {
                            //write to stack
                            PushStack(PC);
                            //jump
                            PC = jmp;
                            //Debug.LogWarning("CALLJNC " + PC.ToString("X4"));
                            mCyclesToWait = 3;
                        }
                        mCyclesToWait += 3;
                        break;
                    }
                case 0xDC:
                    {
                        ushort jmp = ReadImmediateShort();
                        if (C_FCFlag)
                        {
                            //write to stack
                            PushStack(PC);
                            //jump
                            PC = jmp;
                            //Debug.LogWarning("CALLJC " + PC.ToString("X4"));
                            mCyclesToWait = 3;
                        }
                        mCyclesToWait += 3;
                        break;
                    }
                #endregion

                //PUSH registers to stack, decrement stack pointer
                case 0xF5:
                    PushStack(AF);
                    mCyclesToWait = 4;
                    break;
                case 0xC5:
                    PushStack(BC);
                    mCyclesToWait = 4;
                    break;
                case 0xD5:
                    PushStack(DE);
                    mCyclesToWait = 4;
                    break;
                case 0xE5:
                    PushStack(HL);
                    mCyclesToWait = 4;
                    break;
                //POP stack to register, increment stack pointer
                case 0xF1:
                    AF = PopStack();
                    mCyclesToWait = 3;
                    break;
                case 0xC1:
                    BC = PopStack();
                    mCyclesToWait = 3;
                    break;
                case 0xD1:
                    DE = PopStack();
                    mCyclesToWait = 3;
                    break;
                case 0xE1:
                    HL = PopStack();
                    mCyclesToWait = 3;
                    break;
                //RETURNS go back to whatever was on stack
                case 0xC9:
                    PC = PopStack();
                    mCyclesToWait = 4;
                    break;
                case 0xC0:
                    if (!Z_zeroFlag)
                    {
                        PC = PopStack();
                        mCyclesToWait = 3;
                    }
                    mCyclesToWait += 2;
                    break;
                case 0xC8:
                    if (Z_zeroFlag)
                    {
                        PC = PopStack();
                        mCyclesToWait = 3;
                    }
                    mCyclesToWait += 2;
                    break;
                case 0xD0:
                    if (!C_FCFlag)
                    {
                        PC = PopStack();
                        mCyclesToWait = 3;
                    }
                    mCyclesToWait += 2;
                    break;
                case 0xD8:
                    if (C_FCFlag)
                    {
                        PC = PopStack();
                        mCyclesToWait = 3;
                    }
                    mCyclesToWait += 2;
                    break;
                //LD 0xFF00 + C into A
                case 0xF2:
                    WriteByte(CPUReg8.A, memory.Read((ushort)(0xFF00 + Reg_C)));
                    mCyclesToWait = 2;
                    break;
                //LD A into 0xFF00 + C
                case 0xE2:
                    memory.Write((ushort)(0xFF00 + Reg_C), Reg_A);
                    mCyclesToWait = 2;
                    break;
                //LD A into 0xFF00 + (PC)
                case 0xE0:
                    memory.Write((ushort)(0xFF00 + ReadImmediateByte()), Reg_A);
                    mCyclesToWait = 3;
                    break;
                //LD 0xFF00 + (PC) into A
                case 0xF0:
                    WriteByte(CPUReg8.A, memory.Read((ushort)(0xFF00 + ReadImmediateByte())));
                    mCyclesToWait = 3;
                    break;
                //RLCA 
                case 0x07:
                    RotateLeft(CPUReg8.A, false);
                    mCyclesToWait = 1;
                    break;
                //RRCA 
                case 0x0F:
                    RotateRight(CPUReg8.A, false);
                    mCyclesToWait = 1;
                    break;
                //RLA like CB 17
                case 0x17:
                    RotateLeftCarry(CPUReg8.A, false);
                    mCyclesToWait = 1;
                    break;
                //RRA rotate a through carry
                case 0x1F:
                    RotateRightCarry(CPUReg8.A, false);
                    mCyclesToWait = 1;
                    break;

                //DAA if A nibbles are >9 add 6 for that nibble
                case 0x27:
                    {
                        //Basically it tries to correct an operation as if applied to seperate nibbles and that results in strange stuff.
                        byte b = Reg_A;
                        if (!N_subFlag)//added
                        {
                            if (C_FCFlag || b > 0x99)
                            {
                                b += 0x60;
                                C_FCFlag = true;
                            }
                            if (H_HCFlag || (b & 0x0f) > 0x09)
                            {
                                b += 0x6;
                            }
                        }
                        else//subbed
                        {
                            if (C_FCFlag) b -= 0x60;
                            if (H_HCFlag) b -= 0x6;
                        }
                        WriteByte(CPUReg8.A, b);
                        Z_zeroFlag = (b == 0);
                        H_HCFlag = false;
                    }
                    mCyclesToWait = 1;
                    break;
                //CPL Compliment (invert all bits) A reg
                case 0x2F:
                    {
                        byte b = (byte)~Reg_A;
                        WriteByte(CPUReg8.A, b);
                        N_subFlag = true;
                        H_HCFlag = true;
                    }
                    mCyclesToWait = 1;
                    break;
                //CCF compliment carry flag
                case 0x3F:
                    N_subFlag = false;
                    H_HCFlag = false;
                    C_FCFlag = !C_FCFlag;
                    mCyclesToWait = 1;
                    break;
                //SCF Set carry flag
                case 0x37:
                    N_subFlag = false;
                    H_HCFlag = false;
                    C_FCFlag = true;
                    mCyclesToWait = 1;
                    break;
                //ADD immediate to StackPointer
                case 0xE8:
                    {
                        ushort immediate = (ushort)(sbyte)ReadImmediateByte();
                        H_HCFlag = ((SP & 0x0F) + (immediate & 0x0F) & 0x10) != 0;
                        C_FCFlag = ((SP & 0xFF) + (immediate & 0xFF) & 0x0100) != 0;

                        SP += immediate;
                        Z_zeroFlag = false;
                        N_subFlag = false;
                    }
                    mCyclesToWait = 4;
                    break;
                case 0x10:
                    {
                        //Debug.LogError("CPU SHOULD STOP NOW!");
                        byte stop = ReadImmediateByte();//Eat a 0x00 which is 2nd part of this opcode.
                        //if (stop != 0x00)
                        //    Debug.LogError("BROKEN STOP COMMAND LOCKS THE SYSTEM!");
                        //Debug.Break();
                    }
                    mCyclesToWait = 1;
                    break;
                //INTERUPT COMMANDS
                case 0xD9://RETI
                    PC = PopStack();
                    interruptMasterEnableFlag = true;
                    mCyclesToWait = 4;
                    break;
                case 0xF3://DI
                    //This is immediate
                    interruptMasterEnableFlag = false;
                    interruptMasterEnableState = 0;
                    //TODO if not IME or HALT, set disabled time? (gambatte does this)
                    mCyclesToWait = 1;
                    break;
                case 0xFB://IE
                    if(interruptMasterEnableState == 0)
                        interruptMasterEnableState = 1;
                    mCyclesToWait = 1;
                    break;
                //HALT
                case 0x76:
                    isHalted = true;
                    mCyclesToWait = 1;
                    break;
                //RESTARTS current to stack and JMP 0x0000+0x##
                case 0xC7:
                    PushStack(PC);
                    PC = 0x0000;
                    mCyclesToWait = 4;
                    break;
                case 0xCF:
                    PushStack(PC);
                    PC = 0x0008;
                    //Debug.LogError("RESET TO 0x0008");
                    mCyclesToWait = 4;
                    break;
                case 0xD7:
                    PushStack(PC);
                    PC = 0x0010;
                    //Debug.LogError("RESET TO 0x0010");
                    mCyclesToWait = 4;
                    break;
                case 0xDF:
                    PushStack(PC);
                    PC = 0x0018;
                    //Debug.LogError("RESET TO 0x0018");
                    mCyclesToWait = 4;
                    break;
                case 0xE7:
                    PushStack(PC);
                    PC = 0x0020;
                    //Debug.LogError("RESET TO 0x0020");
                    mCyclesToWait = 4;
                    break;
                case 0xEF:
                    PushStack(PC);
                    PC = 0x0028;
                    //Debug.LogError("RESET TO 0x0028");
                    mCyclesToWait = 4;
                    break;
                case 0xF7:
                    PushStack(PC);
                    PC = 0x0030;
                    //Debug.LogError("RESET TO 0x0030");
                    mCyclesToWait = 4;
                    break;
                case 0xFF:
                    PushStack(PC);
                    PC = 0x0038;
                    //Debug.LogError("RESET TO 0x0038");
                    mCyclesToWait = 4;
                    break;

                //SPECIAL OPCODE, HAS 2nd PART!
                case 0xCB:
                    OP = ReadImmediateByte();
                    mCyclesToWait = 2;
                    switch (OP)
                    {
                        //Rotate bits
                        case 0x00:
                            RotateLeft(CPUReg8.B, true);
                            break;
                        case 0x01:
                            RotateLeft(CPUReg8.C, true);
                            break;
                        case 0x02:
                            RotateLeft(CPUReg8.D, true);
                            break;
                        case 0x03:
                            RotateLeft(CPUReg8.E, true);
                            break;
                        case 0x04:
                            RotateLeft(CPUReg8.H, true);
                            break;
                        case 0x05:
                            RotateLeft(CPUReg8.L, true);
                            break;
                        case 0x06:
                            RotateLeft(CPUReg8.HLasPTR, true);
                            mCyclesToWait = 4;
                            break;
                        case 0x07:
                            RotateLeft(CPUReg8.A, true);
                            break;
                        case 0x08:
                            RotateRight(CPUReg8.B, true);
                            break;
                        case 0x09:
                            RotateRight(CPUReg8.C, true);
                            break;
                        case 0x0A:
                            RotateRight(CPUReg8.D, true);
                            break;
                        case 0x0B:
                            RotateRight(CPUReg8.E, true);
                            break;
                        case 0x0C:
                            RotateRight(CPUReg8.H, true);
                            break;
                        case 0x0D:
                            RotateRight(CPUReg8.L, true);
                            break;
                        case 0x0E:
                            RotateRight(CPUReg8.HLasPTR, true);
                            mCyclesToWait = 4;
                            break;
                        case 0x0F:
                            RotateRight(CPUReg8.A, true);
                            break;
                        //Rotate bits through carry
                        case 0x10:
                            RotateLeftCarry(CPUReg8.B, true);
                            break;
                        case 0x11:
                            RotateLeftCarry(CPUReg8.C, true);
                            break;
                        case 0x12:
                            RotateLeftCarry(CPUReg8.D, true);
                            break;
                        case 0x13:
                            RotateLeftCarry(CPUReg8.E, true);
                            break;
                        case 0x14:
                            RotateLeftCarry(CPUReg8.H, true);
                            break;
                        case 0x15:
                            RotateLeftCarry(CPUReg8.L, true);
                            break;
                        case 0x16:
                            RotateLeftCarry(CPUReg8.HLasPTR, true);
                            mCyclesToWait = 4;
                            break;
                        case 0x17:
                            RotateLeftCarry(CPUReg8.A, true);
                            break;
                        case 0x18:
                            RotateRightCarry(CPUReg8.B, true);
                            break;
                        case 0x19:
                            RotateRightCarry(CPUReg8.C, true);
                            break;
                        case 0x1A:
                            RotateRightCarry(CPUReg8.D, true);
                            break;
                        case 0x1B:
                            RotateRightCarry(CPUReg8.E, true);
                            break;
                        case 0x1C:
                            RotateRightCarry(CPUReg8.H, true);
                            break;
                        case 0x1D:
                            RotateRightCarry(CPUReg8.L, true);
                            break;
                        case 0x1E:
                            RotateRightCarry(CPUReg8.HLasPTR, true);
                            mCyclesToWait = 4;
                            break;
                        case 0x1F:
                            RotateRightCarry(CPUReg8.A, true);
                            break;
                        //Bitshift no rotation
                        case 0x20:
                            ShiftLeftArithmetic(CPUReg8.B);
                            break;
                        case 0x21:
                            ShiftLeftArithmetic(CPUReg8.C);
                            break;
                        case 0x22:
                            ShiftLeftArithmetic(CPUReg8.D);
                            break;
                        case 0x23:
                            ShiftLeftArithmetic(CPUReg8.E);
                            break;
                        case 0x24:
                            ShiftLeftArithmetic(CPUReg8.H);
                            break;
                        case 0x25:
                            ShiftLeftArithmetic(CPUReg8.L);
                            break;
                        case 0x26:
                            ShiftLeftArithmetic(CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0x27:
                            ShiftLeftArithmetic(CPUReg8.A);
                            break;
                        case 0x28:
                            ShiftRightArithmetic(CPUReg8.B);
                            break;
                        case 0x29:
                            ShiftRightArithmetic(CPUReg8.C);
                            break;
                        case 0x2A:
                            ShiftRightArithmetic(CPUReg8.D);
                            break;
                        case 0x2B:
                            ShiftRightArithmetic(CPUReg8.E);
                            break;
                        case 0x2C:
                            ShiftRightArithmetic(CPUReg8.H);
                            break;
                        case 0x2D:
                            ShiftRightArithmetic(CPUReg8.L);
                            break;
                        case 0x2E:
                            ShiftRightArithmetic(CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0x2F:
                            ShiftRightArithmetic(CPUReg8.A);
                            break;
                        //SWAP nibbles
                        case 0x30:
                            SWAP(CPUReg8.B);
                            break;
                        case 0x31:
                            SWAP(CPUReg8.C);
                            break;
                        case 0x32:
                            SWAP(CPUReg8.D);
                            break;
                        case 0x33:
                            SWAP(CPUReg8.E);
                            break;
                        case 0x34:
                            SWAP(CPUReg8.H);
                            break;
                        case 0x35:
                            SWAP(CPUReg8.L);
                            break;
                        case 0x36:
                            SWAP(CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0x37:
                            SWAP(CPUReg8.A);
                            break;
                        //Shift R into carry
                        case 0x38:
                            ShiftRightLogical(CPUReg8.B);
                            break;
                        case 0x39:
                            ShiftRightLogical(CPUReg8.C);
                            break;
                        case 0x3A:
                            ShiftRightLogical(CPUReg8.D);
                            break;
                        case 0x3B:
                            ShiftRightLogical(CPUReg8.E);
                            break;
                        case 0x3C:
                            ShiftRightLogical(CPUReg8.H);
                            break;
                        case 0x3D:
                            ShiftRightLogical(CPUReg8.L);
                            break;
                        case 0x3E:
                            ShiftRightLogical(CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0x3F:
                            ShiftRightLogical(CPUReg8.A);
                            break;
                        //All bit checks.
                        case 0x40:
                            BIT(0, CPUReg8.B);
                            break;
                        case 0x41:
                            BIT(0, CPUReg8.C);
                            break;
                        case 0x42:
                            BIT(0, CPUReg8.D);
                            break;
                        case 0x43:
                            BIT(0, CPUReg8.E);
                            break;
                        case 0x44:
                            BIT(0, CPUReg8.H);
                            break;
                        case 0x45:
                            BIT(0, CPUReg8.L);
                            break;
                        case 0x46:
                            mCyclesToWait = 3;
                            BIT(0, CPUReg8.HLasPTR);
                            break;
                        case 0x47:
                            BIT(0, CPUReg8.A);
                            break;
                        case 0x48:
                            BIT(1, CPUReg8.B);
                            break;
                        case 0x49:
                            BIT(1, CPUReg8.C);
                            break;
                        case 0x4A:
                            BIT(1, CPUReg8.D);
                            break;
                        case 0x4B:
                            BIT(1, CPUReg8.E);
                            break;
                        case 0x4C:
                            BIT(1, CPUReg8.H);
                            break;
                        case 0x4D:
                            BIT(1, CPUReg8.L);
                            break;
                        case 0x4E:
                            mCyclesToWait = 3;
                            BIT(1, CPUReg8.HLasPTR);
                            break;
                        case 0x4F:
                            BIT(1, CPUReg8.A);
                            break;
                        case 0x50:
                            BIT(2, CPUReg8.B);
                            break;
                        case 0x51:
                            BIT(2, CPUReg8.C);
                            break;
                        case 0x52:
                            BIT(2, CPUReg8.D);
                            break;
                        case 0x53:
                            BIT(2, CPUReg8.E);
                            break;
                        case 0x54:
                            BIT(2, CPUReg8.H);
                            break;
                        case 0x55:
                            BIT(2, CPUReg8.L);
                            break;
                        case 0x56:
                            mCyclesToWait = 3;
                            BIT(2, CPUReg8.HLasPTR);
                            break;
                        case 0x57:
                            BIT(2, CPUReg8.A);
                            break;
                        case 0x58:
                            BIT(3, CPUReg8.B);
                            break;
                        case 0x59:
                            BIT(3, CPUReg8.C);
                            break;
                        case 0x5A:
                            BIT(3, CPUReg8.D);
                            break;
                        case 0x5B:
                            BIT(3, CPUReg8.E);
                            break;
                        case 0x5C:
                            BIT(3, CPUReg8.H);
                            break;
                        case 0x5D:
                            BIT(3, CPUReg8.L);
                            break;
                        case 0x5E:
                            mCyclesToWait = 3;
                            BIT(3, CPUReg8.HLasPTR);
                            break;
                        case 0x5F:
                            BIT(3, CPUReg8.A);
                            break;
                        case 0x60:
                            BIT(4, CPUReg8.B);
                            break;
                        case 0x61:
                            BIT(4, CPUReg8.C);
                            break;
                        case 0x62:
                            BIT(4, CPUReg8.D);
                            break;
                        case 0x63:
                            BIT(4, CPUReg8.E);
                            break;
                        case 0x64:
                            BIT(4, CPUReg8.H);
                            break;
                        case 0x65:
                            BIT(4, CPUReg8.L);
                            break;
                        case 0x66:
                            mCyclesToWait = 3;
                            BIT(4, CPUReg8.HLasPTR);
                            break;
                        case 0x67:
                            BIT(4, CPUReg8.A);
                            break;
                        case 0x68:
                            BIT(5, CPUReg8.B);
                            break;
                        case 0x69:
                            BIT(5, CPUReg8.C);
                            break;
                        case 0x6A:
                            BIT(5, CPUReg8.D);
                            break;
                        case 0x6B:
                            BIT(5, CPUReg8.E);
                            break;
                        case 0x6C:
                            BIT(5, CPUReg8.H);
                            break;
                        case 0x6D:
                            BIT(5, CPUReg8.L);
                            break;
                        case 0x6E:
                            mCyclesToWait = 3;
                            BIT(5, CPUReg8.HLasPTR);
                            break;
                        case 0x6F:
                            BIT(5, CPUReg8.A);
                            break;
                        case 0x70:
                            BIT(6, CPUReg8.B);
                            break;
                        case 0x71:
                            BIT(6, CPUReg8.C);
                            break;
                        case 0x72:
                            BIT(6, CPUReg8.D);
                            break;
                        case 0x73:
                            BIT(6, CPUReg8.E);
                            break;
                        case 0x74:
                            BIT(6, CPUReg8.H);
                            break;
                        case 0x75:
                            BIT(6, CPUReg8.L);
                            break;
                        case 0x76:
                            mCyclesToWait = 3;
                            BIT(6, CPUReg8.HLasPTR);
                            break;
                        case 0x77:
                            BIT(6, CPUReg8.A);
                            break;
                        case 0x78:
                            BIT(7, CPUReg8.B);
                            break;
                        case 0x79:
                            BIT(7, CPUReg8.C);
                            break;
                        case 0x7A:
                            BIT(7, CPUReg8.D);
                            break;
                        case 0x7B:
                            BIT(7, CPUReg8.E);
                            break;
                        case 0x7C:
                            BIT(7, CPUReg8.H);
                            break;
                        case 0x7D:
                            BIT(7, CPUReg8.L);
                            break;
                        case 0x7E:
                            mCyclesToWait = 3;
                            BIT(7, CPUReg8.HLasPTR);
                            break;
                        case 0x7F:
                            BIT(7, CPUReg8.A);
                            break;
                        //All bit RESETS
                        case 0x80:
                            RES(0, CPUReg8.B);
                            break;
                        case 0x81:
                            RES(0, CPUReg8.C);
                            break;
                        case 0x82:
                            RES(0, CPUReg8.D);
                            break;
                        case 0x83:
                            RES(0, CPUReg8.E);
                            break;
                        case 0x84:
                            RES(0, CPUReg8.H);
                            break;
                        case 0x85:
                            RES(0, CPUReg8.L);
                            break;
                        case 0x86:
                            RES(0, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0x87:
                            RES(0, CPUReg8.A);
                            break;
                        case 0x88:
                            RES(1, CPUReg8.B);
                            break;
                        case 0x89:
                            RES(1, CPUReg8.C);
                            break;
                        case 0x8A:
                            RES(1, CPUReg8.D);
                            break;
                        case 0x8B:
                            RES(1, CPUReg8.E);
                            break;
                        case 0x8C:
                            RES(1, CPUReg8.H);
                            break;
                        case 0x8D:
                            RES(1, CPUReg8.L);
                            break;
                        case 0x8E:
                            RES(1, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0x8F:
                            RES(1, CPUReg8.A);
                            break;
                        case 0x90:
                            RES(2, CPUReg8.B);
                            break;
                        case 0x91:
                            RES(2, CPUReg8.C);
                            break;
                        case 0x92:
                            RES(2, CPUReg8.D);
                            break;
                        case 0x93:
                            RES(2, CPUReg8.E);
                            break;
                        case 0x94:
                            RES(2, CPUReg8.H);
                            break;
                        case 0x95:
                            RES(2, CPUReg8.L);
                            break;
                        case 0x96:
                            RES(2, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0x97:
                            RES(2, CPUReg8.A);
                            break;
                        case 0x98:
                            RES(3, CPUReg8.B);
                            break;
                        case 0x99:
                            RES(3, CPUReg8.C);
                            break;
                        case 0x9A:
                            RES(3, CPUReg8.D);
                            break;
                        case 0x9B:
                            RES(3, CPUReg8.E);
                            break;
                        case 0x9C:
                            RES(3, CPUReg8.H);
                            break;
                        case 0x9D:
                            RES(3, CPUReg8.L);
                            break;
                        case 0x9E:
                            RES(3, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0x9F:
                            RES(3, CPUReg8.A);
                            break;
                        case 0xA0:
                            RES(4, CPUReg8.B);
                            break;
                        case 0xA1:
                            RES(4, CPUReg8.C);
                            break;
                        case 0xA2:
                            RES(4, CPUReg8.D);
                            break;
                        case 0xA3:
                            RES(4, CPUReg8.E);
                            break;
                        case 0xA4:
                            RES(4, CPUReg8.H);
                            break;
                        case 0xA5:
                            RES(4, CPUReg8.L);
                            break;
                        case 0xA6:
                            RES(4, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0xA7:
                            RES(4, CPUReg8.A);
                            break;
                        case 0xA8:
                            RES(5, CPUReg8.B);
                            break;
                        case 0xA9:
                            RES(5, CPUReg8.C);
                            break;
                        case 0xAA:
                            RES(5, CPUReg8.D);
                            break;
                        case 0xAB:
                            RES(5, CPUReg8.E);
                            break;
                        case 0xAC:
                            RES(5, CPUReg8.H);
                            break;
                        case 0xAD:
                            RES(5, CPUReg8.L);
                            break;
                        case 0xAE:
                            RES(5, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0xAF:
                            RES(5, CPUReg8.A);
                            break;
                        case 0xB0:
                            RES(6, CPUReg8.B);
                            break;
                        case 0xB1:
                            RES(6, CPUReg8.C);
                            break;
                        case 0xB2:
                            RES(6, CPUReg8.D);
                            break;
                        case 0xB3:
                            RES(6, CPUReg8.E);
                            break;
                        case 0xB4:
                            RES(6, CPUReg8.H);
                            break;
                        case 0xB5:
                            RES(6, CPUReg8.L);
                            break;
                        case 0xB6:
                            RES(6, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0xB7:
                            RES(6, CPUReg8.A);
                            break;
                        case 0xB8:
                            RES(7, CPUReg8.B);
                            break;
                        case 0xB9:
                            RES(7, CPUReg8.C);
                            break;
                        case 0xBA:
                            RES(7, CPUReg8.D);
                            break;
                        case 0xBB:
                            RES(7, CPUReg8.E);
                            break;
                        case 0xBC:
                            RES(7, CPUReg8.H);
                            break;
                        case 0xBD:
                            RES(7, CPUReg8.L);
                            break;
                        case 0xBE:
                            RES(7, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0xBF:
                            RES(7, CPUReg8.A);
                            break;
                        //All bit SETS
                        case 0xC0:
                            SET(0, CPUReg8.B);
                            break;
                        case 0xC1:
                            SET(0, CPUReg8.C);
                            break;
                        case 0xC2:
                            SET(0, CPUReg8.D);
                            break;
                        case 0xC3:
                            SET(0, CPUReg8.E);
                            break;
                        case 0xC4:
                            SET(0, CPUReg8.H);
                            break;
                        case 0xC5:
                            SET(0, CPUReg8.L);
                            break;
                        case 0xC6:
                            SET(0, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0xC7:
                            SET(0, CPUReg8.A);
                            break;
                        case 0xC8:
                            SET(1, CPUReg8.B);
                            break;
                        case 0xC9:
                            SET(1, CPUReg8.C);
                            break;
                        case 0xCA:
                            SET(1, CPUReg8.D);
                            break;
                        case 0xCB:
                            SET(1, CPUReg8.E);
                            break;
                        case 0xCC:
                            SET(1, CPUReg8.H);
                            break;
                        case 0xCD:
                            SET(1, CPUReg8.L);
                            break;
                        case 0xCE:
                            SET(1, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0xCF:
                            SET(1, CPUReg8.A);
                            break;
                        case 0xD0:
                            SET(2, CPUReg8.B);
                            break;
                        case 0xD1:
                            SET(2, CPUReg8.C);
                            break;
                        case 0xD2:
                            SET(2, CPUReg8.D);
                            break;
                        case 0xD3:
                            SET(2, CPUReg8.E);
                            break;
                        case 0xD4:
                            SET(2, CPUReg8.H);
                            break;
                        case 0xD5:
                            SET(2, CPUReg8.L);
                            break;
                        case 0xD6:
                            SET(2, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0xD7:
                            SET(2, CPUReg8.A);
                            break;
                        case 0xD8:
                            SET(3, CPUReg8.B);
                            break;
                        case 0xD9:
                            SET(3, CPUReg8.C);
                            break;
                        case 0xDA:
                            SET(3, CPUReg8.D);
                            break;
                        case 0xDB:
                            SET(3, CPUReg8.E);
                            break;
                        case 0xDC:
                            SET(3, CPUReg8.H);
                            break;
                        case 0xDD:
                            SET(3, CPUReg8.L);
                            break;
                        case 0xDE:
                            SET(3, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0xDF:
                            SET(3, CPUReg8.A);
                            break;
                        case 0xE0:
                            SET(4, CPUReg8.B);
                            break;
                        case 0xE1:
                            SET(4, CPUReg8.C);
                            break;
                        case 0xE2:
                            SET(4, CPUReg8.D);
                            break;
                        case 0xE3:
                            SET(4, CPUReg8.E);
                            break;
                        case 0xE4:
                            SET(4, CPUReg8.H);
                            break;
                        case 0xE5:
                            SET(4, CPUReg8.L);
                            break;
                        case 0xE6:
                            SET(4, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0xE7:
                            SET(4, CPUReg8.A);
                            break;
                        case 0xE8:
                            SET(5, CPUReg8.B);
                            break;
                        case 0xE9:
                            SET(5, CPUReg8.C);
                            break;
                        case 0xEA:
                            SET(5, CPUReg8.D);
                            break;
                        case 0xEB:
                            SET(5, CPUReg8.E);
                            break;
                        case 0xEC:
                            SET(5, CPUReg8.H);
                            break;
                        case 0xED:
                            SET(5, CPUReg8.L);
                            break;
                        case 0xEE:
                            SET(5, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0xEF:
                            SET(5, CPUReg8.A);
                            break;
                        case 0xF0:
                            SET(6, CPUReg8.B);
                            break;
                        case 0xF1:
                            SET(6, CPUReg8.C);
                            break;
                        case 0xF2:
                            SET(6, CPUReg8.D);
                            break;
                        case 0xF3:
                            SET(6, CPUReg8.E);
                            break;
                        case 0xF4:
                            SET(6, CPUReg8.H);
                            break;
                        case 0xF5:
                            SET(6, CPUReg8.L);
                            break;
                        case 0xF6:
                            SET(6, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0xF7:
                            SET(6, CPUReg8.A);
                            break;
                        case 0xF8:
                            SET(7, CPUReg8.B);
                            break;
                        case 0xF9:
                            SET(7, CPUReg8.C);
                            break;
                        case 0xFA:
                            SET(7, CPUReg8.D);
                            break;
                        case 0xFB:
                            SET(7, CPUReg8.E);
                            break;
                        case 0xFC:
                            SET(7, CPUReg8.H);
                            break;
                        case 0xFD:
                            SET(7, CPUReg8.L);
                            break;
                        case 0xFE:
                            SET(7, CPUReg8.HLasPTR);
                            mCyclesToWait = 4;
                            break;
                        case 0xFF:
                            SET(7, CPUReg8.A);
                            break;
                    }
                    break;

                //UNDEFINED OPCODES
                case 0xD3:
                case 0xDB:
                case 0xDD:
                case 0xE3:
                case 0xE4:
                case 0xEB:
                case 0xEC:
                case 0xED:
                case 0xF4:
                case 0xFC:
                case 0xFD:
                    Debug.LogError("UNDEFINED OPCODE " + OP.ToString("X2") + ", CPU LOCKS");
                    Debug.Break();
                    break;
            }
    }

    #region OPERATIONS
    /// <summary>
    /// add 16 bit registers A is target
    /// In reality it does LSB then MSB and takes MSB as result for flags
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void ADD_16_ToHL(ushort inB)
    {
        ushort a = HL;
        ushort b = inB;
        int result = a + b;
        HL = (ushort)result;
        N_subFlag = false;
        H_HCFlag = (((a & 0x0FFF) + (b & 0x0FFF)) & 0x1000) != 0;
        C_FCFlag = (result & 0x10000) != 0;
    }

    /// <summary>
    /// Compare a value against A, which is basically a SUB but without anything being written
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void COMP(byte reg)
    {
        byte a = Reg_A;
        byte b = reg;
        int val = a - b;
        byte res = (byte)val;
        Z_zeroFlag = (res == 0);
        N_subFlag = true;
        H_HCFlag = ((a & 0xF) - (b & 0xF) < 0);
        C_FCFlag = (val < 0);
    }

    /// <summary>
    /// INC register
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void INC(CPUReg8 reg)
    {
        byte b = ReadByte(reg);
        byte res = (byte)(b + 1);
        Z_zeroFlag = (res == 0);
        N_subFlag = false;
        H_HCFlag = (((b & 0x0F) + 1) & 0x10) != 0;
        WriteByte(reg, res);
    }

    /// <summary>
    /// DEC register
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void DEC(CPUReg8 reg)
    {
        byte a = ReadByte(reg);
        byte b = 1;
        int val = a - b;
        byte res = (byte)val;
        Z_zeroFlag = (res == 0);
        N_subFlag = true;
        H_HCFlag = (a & 0xF) - b < 0;
        WriteByte(reg, res);
    }

    /// <summary>
    /// ADD X to A
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void ADD_A(byte reg)
    {
        byte a = Reg_A;
        byte b = reg;
        int val = a + b;
        byte res = (byte)val;
        Z_zeroFlag = (res == 0);
        N_subFlag = false;
        H_HCFlag = (((a & 0x0F) + (b & 0x0F)) & 0x10) != 0;
        C_FCFlag = (val & 0x0100) != 0;
        Reg_A = res;
    }

    /// <summary>
    /// SUB X from A
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void SUB_A(CPUReg8 reg)
    {
        byte a = Reg_A;
        byte b = ReadByte(reg);
        int val = a - b;
        byte res = (byte)val;
        Z_zeroFlag = (res == 0);
        N_subFlag = true;
        H_HCFlag = ((a & 0xF) - (b & 0xF) < 0);
        C_FCFlag = (val < 0);
        Reg_A = res;
    }

    /// <summary>
    /// ADD X + Carry to A 
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void ADC_A(CPUReg8 reg)
    {
        int flag = C_FCFlag ? 1 : 0;
        byte a = Reg_A;
        byte b = ReadByte(reg);
        int val = a + b + flag;
        byte res = (byte)val;
        Z_zeroFlag = (res == 0);
        N_subFlag = false;
        H_HCFlag = (((a & 0x0F) + (b & 0x0F) + flag) & 0x10) != 0;
        C_FCFlag = (val & 0x0100) != 0;
        Reg_A = res;
    }

    /// <summary>
    /// SUB X + flag from A
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void SBC_A(CPUReg8 reg)
    {
        int flag = C_FCFlag ? 1 : 0;
        byte a = Reg_A;
        byte b = ReadByte(reg);
        int val = a - b - flag;
        byte res = (byte)val;
        Z_zeroFlag = (res == 0);
        N_subFlag = true;
        H_HCFlag = ((a & 0xF) - (b & 0xF) - flag < 0);
        C_FCFlag = (val < 0);
        Reg_A = res;
    }

    /// <summary>
    /// AND register with A
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void AND_A(CPUReg8 reg)
    {
        byte b = ReadByte(reg);
        byte a = Reg_A;
        a = (byte)(b & a);
        Z_zeroFlag = (a == 0);
        N_subFlag = false;
        H_HCFlag = true;
        C_FCFlag = false;
        Reg_A = a;
    }

    /// <summary>
    /// OR register with A
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void OR_A(CPUReg8 reg)
    {
        byte b = ReadByte(reg);
        byte a = Reg_A;
        a = (byte)(b | a);
        Z_zeroFlag = (a == 0);
        N_subFlag = false;
        H_HCFlag = false;
        C_FCFlag = false;
        Reg_A = a;
    }

    /// <summary>
    /// Exclusive OR register with A
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void XOR_A(CPUReg8 reg)
    {
        byte b = ReadByte(reg);
        byte a = Reg_A;
        a = (byte)(b ^ a);
        Z_zeroFlag = (a == 0);
        N_subFlag = false;
        H_HCFlag = false;
        C_FCFlag = false; 
        Reg_A = a;
    }

    /// <summary>
    /// Rotate register left through carry
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void RotateLeftCarry(CPUReg8 reg, bool setZeroProper)
    {
        byte b = ReadByte(reg);
        bool oldCarry = C_FCFlag;
        ushort res = (ushort)(b << 1);
        b = (byte)res;
        C_FCFlag = (res & 0x0100) != 0;
        if (oldCarry)
            b++;

        if (setZeroProper)
            Z_zeroFlag = (b == 0);
        else
            Z_zeroFlag = false;
        N_subFlag = false;
        H_HCFlag = false;
        WriteByte(reg, b);
    }

    /// <summary>
    /// Rotate register right through carry
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void RotateRightCarry(CPUReg8 reg, bool setZeroProper)
    {
        byte b = ReadByte(reg);
        bool oldCarry = C_FCFlag;
        C_FCFlag = (b & 0x1) != 0;
        b = (byte)(b >> 1);
        if (oldCarry)
            b |= 0x80;

        if (setZeroProper)
            Z_zeroFlag = (b == 0);
        else
            Z_zeroFlag = false;
        N_subFlag = false;
        H_HCFlag = false;
        WriteByte(reg, b);
    }

    /// <summary>
    /// Rotate left normally.
    /// Shift left, 0bit comes from 7bit, 7bit also goes to Carry.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void RotateLeft(CPUReg8 reg, bool setZeroProper)
    {
        byte b = ReadByte(reg);
        ushort res = (ushort)(b << 1);
        b = (byte)res;
        if ((res & 0x0100) != 0)
            b++;

        if (setZeroProper)
            Z_zeroFlag = (b == 0);
        else
            Z_zeroFlag = false;
        N_subFlag = false;
        H_HCFlag = false;
        C_FCFlag = (res & 0x0100) != 0;
        WriteByte(reg, b);
    }

    /// <summary>
    /// Rotate right normally. (0bit to 7bit, 0bit to CF)
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void RotateRight(CPUReg8 reg, bool setZeroProper)
    {
        byte b = ReadByte(reg);
        C_FCFlag = (b & 0x1) != 0;
        b = (byte)(b >> 1);
        if (C_FCFlag)
            b = (byte)(b | 0x80);

        if (setZeroProper)
            Z_zeroFlag = (b == 0);
        else
            Z_zeroFlag = false;
        N_subFlag = false;
        H_HCFlag = false;
        WriteByte(reg, b);
    }

    /// <summary>
    /// Shift left into carry, 0bit to 0
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void ShiftLeftArithmetic(CPUReg8 reg)
    {
        int val = ReadByte(reg);
        val = (val << 1);
        C_FCFlag = (val & 0x0100) != 0;
        byte b = (byte)val;
        Z_zeroFlag = (b == 0);
        N_subFlag = false;
        H_HCFlag = false;
        WriteByte(reg, b);
    }

    /// <summary>
    /// Shift right 0bit to carry, 7bit unchanged.
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void ShiftRightArithmetic(CPUReg8 reg)
    {
        byte b = ReadByte(reg);
        C_FCFlag = (b & 0x1) != 0;
        bool bit7 = (b & 0x80) != 0;
        b = (byte)(b >> 1);
        if (bit7)
            b = (byte)(b | 0x80);
        Z_zeroFlag = (b == 0);
        N_subFlag = false;
        H_HCFlag = false;
        WriteByte(reg, b);
    }

    /// <summary>
    /// Shift right 0bit to carry, 7bit changes to 0
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void ShiftRightLogical(CPUReg8 reg)
    {
        byte b = ReadByte(reg);
        C_FCFlag = (b & 0x1) != 0;
        b = (byte)(b >> 1);
        Z_zeroFlag = (b == 0);
        N_subFlag = false;
        H_HCFlag = false;
        WriteByte(reg, b);
    }

    /// <summary>
    /// Swap upper 4 and lower 4 bits
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void SWAP(CPUReg8 reg)
    {
        byte b = ReadByte(reg);
        byte upper = (byte)(b >> 4);
        byte lower = (byte)(b << 4);
        b = (byte)(upper | lower);

        Z_zeroFlag = (b == 0);
        N_subFlag = false;
        H_HCFlag = false;
        C_FCFlag = false;
        WriteByte(reg, b);
    }

    /// <summary>
    /// test a bit and write it to Zeroflag
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void BIT(int bit, CPUReg8 reg)
    {
        Z_zeroFlag = ((ReadByte(reg) & (1 << bit)) == 0);
        N_subFlag = false;
        H_HCFlag = true;
    }

    /// <summary>
    /// set a bit
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void SET(int bit, CPUReg8 reg)
    {
        byte b = ReadByte(reg);
        b = (byte)(b | (1 << bit));//set 1 bit
        WriteByte(reg, b);
    }

    /// <summary>
    /// reset a bit
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void RES(int bit, CPUReg8 reg)
    {
        byte b = ReadByte(reg);
        b = (byte)(b & ~(1 << bit));//reset 1 bit
        WriteByte(reg, b);
    }
    #endregion

    #region MEMORY INTERACTIONS
    /// <summary>
    /// Get 2 bytes from memory and advances PC 2x
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private ushort ReadImmediateShort()
    {
        //read LSB first, converts 0x13 and 0x02 into 0x0213
        ushort shrt = (ushort)(memory.Read((ushort)(PC + 1)) << 8);
        shrt += memory.Read(PC);
        PC += 2;
        return shrt;
    }

    /// <summary>
    /// Gets 1 byte from memory and advances PC
    /// </summary>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private byte ReadImmediateByte()
    {
        byte ret = memory.Read(PC);
        PC++;
        return ret;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void PushStack(ushort data)
    {
        //write to stack MSB then LSB
        SP--;
        memory.Write(SP, (byte)(data >> 8));
        SP--;
        memory.Write(SP, (byte)data);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private ushort PopStack()
    {
        ushort res = (ushort)(memory.Read(SP) | (memory.Read((ushort)(SP + 1)) << 8));
        SP += 2;
        return res;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private byte ReadLSB(CPUReg16 reg)
    {
        switch (reg)
        {
            case CPUReg16.AF:
                return Reg_F;
            case CPUReg16.BC:
                return Reg_C;
            case CPUReg16.DE:
                return Reg_E;
            case CPUReg16.HL:
                return Reg_L;
            case CPUReg16.SP:
                return (byte)SP;
            case CPUReg16.PC:
                return (byte)PC;
            default:
                return 0;
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private byte ReadMSB(CPUReg16 reg)
    {
        switch (reg)
        {
            case CPUReg16.AF:
                return Reg_A;
            case CPUReg16.BC:
                return Reg_B;
            case CPUReg16.DE:
                return Reg_D;
            case CPUReg16.HL:
                return Reg_H;
            case CPUReg16.SP:
                return (byte)(SP >> 8);
            case CPUReg16.PC:
                return (byte)(PC >> 8);
            default:
                return 0;
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private byte ReadByte(CPUReg8 reg)
    {
        switch (reg)
        {
            case CPUReg8.A:
                return Reg_A;
            case CPUReg8.B:
                return Reg_B;
            case CPUReg8.C:
                return Reg_C;
            case CPUReg8.D:
                return Reg_D;
            case CPUReg8.E:
                return Reg_E;
            case CPUReg8.H:
                return Reg_H;
            case CPUReg8.L:
                return Reg_L;
            case CPUReg8.BCasPTR://(BC)
                return memory.Read(BC);
            case CPUReg8.DEasPTR://(DE)
                return memory.Read(DE);
            case CPUReg8.HLasPTR://(HL)
                return memory.Read(HL);
            case CPUReg8.Immediate2AsPTR://program counter as loc for 1 byte
                return memory.Read(ReadImmediateShort());
            case CPUReg8.ImmediateValue://(PC)
                return ReadImmediateByte();//immediate value
            default:
                return 0;
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void WriteByte(CPUReg8 reg, byte val)
    {
        switch (reg)
        {
            case CPUReg8.A:
                Reg_A = val;
                return;
            case CPUReg8.B:
                Reg_B = val;
                return;
            case CPUReg8.C:
                Reg_C = val;
                return;
            case CPUReg8.D:
                Reg_D = val;
                return;
            case CPUReg8.E:
                Reg_E = val;
                return;
            case CPUReg8.H:
                Reg_H = val;
                return;
            case CPUReg8.L:
                Reg_L = val;
                return;
            case CPUReg8.BCasPTR://(BC)
                memory.Write(BC, val);
                return;
            case CPUReg8.DEasPTR://(DE)
                memory.Write(DE, val);
                return;
            case CPUReg8.HLasPTR://(HL)
                memory.Write(HL, val);
                return;
            default:
                return;
        }
    }
    #endregion
}