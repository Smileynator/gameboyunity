﻿using System;
using System.IO;

public class GBMemory : IGBMemory, IGBMemoryPPU, IGBMemoryJoyPad
{
    public CartridgeHeader header { get; private set; }
    public bool HasBootRom => bootRom != null;

    private LCDControl lcdControl;
    public LCDControl LcdControl => lcdControl;

    private readonly string ramSavePath;
    private readonly byte[] bootRom;
    private byte[] GBRom;
    private readonly byte[] memory = new byte[0xFFFF + 1];
    private byte[][] RAMBanks;
    private readonly IGBCartEvents cartEvents;
    private readonly IGBClockRead clock;
    private ushort DIVTimer;

    public bool RAMEnabled
    {
        get => _RAMEnabled;
        private set
        {
            if (RAMBanks == null)
            {
                LoadRAMFromFile();
                if(RAMBanks == null)
                    InitializeRAMBanks();
            }
            _RAMEnabled = value;
        }
    }
    private bool _RAMEnabled;

    private short loadedRAMBank = -1;

    private readonly MBC1Registers mbc1Regs = new ();
    private readonly MBC3Registers mbc3Regs = new ();
    private readonly MBC5Registers mbc5Regs = new ();

    /// <summary>
    /// Initializes memory, optionally takes a bootrom to use.
    /// </summary>
    public GBMemory(string ramSavePath, byte[] bootRom, byte[] gameRom, IGBClockRead clock, IGBCartEvents cartEvents)
    {
        this.ramSavePath = ramSavePath;
        this.bootRom = bootRom;
        this.clock = clock;
        clock.MCycle += AdvanceTimers;
        this.cartEvents = cartEvents;
        //Init specific memory
        LoadGameRom(gameRom);
        InitializeRegisters();
    }

    ~GBMemory()
    {
        clock.MCycle -= AdvanceTimers;
    }
    
    private bool apuFallingEdge = false;
    private int realTimeClockCounter = 0;
    private bool serialTransferFallingEdge = false;
    private bool timerBitLastValue = false;
    
    private void AdvanceTimers()
    {
        DIVTimer++;
        memory[0xFF04] = (byte)(DIVTimer >> 6);
        //APU
        bool apuCurrentEdge = (DIVTimer & 0b10000000000) != 0;
        if (apuFallingEdge && !apuCurrentEdge)
        {
            ApuTick();
        }
        apuFallingEdge = apuCurrentEdge;
        //RTC
        if (header != null && header.IsMBC3())
        {
            realTimeClockCounter++;
            if (realTimeClockCounter == 1048576)//one second passed
            {
                realTimeClockCounter = 0;
                mbc3Regs.ClockRTC();
            }
        }
        //Serial Transfer
        bool serialCurrentEdge = (DIVTimer & 0b10000000) != 0;
        if (serialTransferFallingEdge && !serialCurrentEdge)
        {
            ClockSerialTransfer();
        }
        serialTransferFallingEdge = serialCurrentEdge;
        //TIMER
        byte TAC = memory[0xFF07];
        byte timerControlFrequency = (byte)(TAC & 0b00000011);
        bool bitActive = false;
        switch (timerControlFrequency)
        {
            case 1:
                bitActive = (DIVTimer & 0b10) != 0;
                break;
            case 2:
                bitActive = (DIVTimer & 0b1000) != 0;
                break;
            case 3:
                bitActive = (DIVTimer & 0b100000) != 0;
                break;
            case 0:
                bitActive = (DIVTimer & 0b10000000) != 0;
                break;
        }
        bool timerControlOn = (TAC & 0b100) != 0;
        bool timerBitCurrentValue = bitActive && timerControlOn;
        bool fallingEdge = timerBitLastValue && !timerBitCurrentValue;
        if (fallingEdge)
        {
            byte TIMA = memory[0xFF05];
            memory[0xFF05]++;
            //Set the time overflow interrupt
            if (TIMA == 0xFF)
            {
                memory[0xFF0F] |= (byte) Interrupt.TimerOverflow;
                memory[0xFF05] = memory[0xFF06];
            }
        }
        timerBitLastValue = timerBitCurrentValue;
    }

    private void InitializeRegisters()
    {
        //Ensure all registers are written once, to set any fixed bits.
        for (ushort address = 0xFF00; address < 0xFFFF; address++)
        {
            Write(address, 0x00);
        }
        Write(0xFFFF, 0x00);
        
        //Set specific initialization values
        WriteAsRegister(0xFF02, 0x7E);
        WriteAsRegister(0xFF40, 0x91);//LCD Control
    }

    private void LoadGameRom(byte[] rom)
    {
        GBRom = rom;
        loadedRAMBank = -1;
        if (GBRom == null)
        {
            Console.WriteLine("No ROM passed to memory!");
        }
        else
        {
            LoadFirstROMBank(0);
            LoadSecondROMBank(1);
        }
        LoadBootRom();
    }

    private void LoadBootRom()
    {
        if (HasBootRom)
        {
            Array.Copy(bootRom, 0, memory, 0, bootRom.Length);
        }
    }

    private void LoadFirstROMBank(ushort bank)
    {
        Array.Copy(GBRom, 0x4000 * bank, memory, 0x0000, 0x4000);
        if (header == null && bank == 0)
        {
            header = new CartridgeHeader(GBRom);
        }
    }

    private void LoadSecondROMBank(short bank)
    {
        Array.Copy(GBRom, 0x4000 * bank, memory, 0x4000, 0x4000);
    }

    private void LoadRAMBank(byte bank, bool forced = false)
    {
        if (bank == loadedRAMBank && !forced)
            return;
        //Debug.LogWarning("Loading RAM bank: " + bank + " Unloading RAM bank: " + loadedRAMBank);
        //copy back RAM back to array 
        if (loadedRAMBank != -1)
        {
            //copy memory chunk to RAM bank
            Array.Copy(memory, 0xA000, RAMBanks[loadedRAMBank], 0, RAMBanks[loadedRAMBank].Length);
        }
        //load ram bank to memory chunk
        Array.Copy(RAMBanks[bank], 0, memory, 0xA000, RAMBanks[bank].Length);
        loadedRAMBank = bank;
        //Write to file
        //Debug.LogWarning("SAVING RAM bank: " + bank);
        SaveRAMtoFile();
    }

    private void InitializeRAMBanks()
    {
        //Debug.Log("Init RAM banks!");
        if (header.IsMBC2())
        {
            RAMBanks = new byte[1][];
            RAMBanks[0] = new byte[512];
        }
        else
        {
            switch (header.RAMInfo)
            {
                case 0x00:
                    RAMBanks = new byte[0][];
                    break;
                case 0x01:
                    RAMBanks = new byte[1][];
                    RAMBanks[0] = new byte[2048];
                    break;
                case 0x02:
                    RAMBanks = new byte[1][];
                    RAMBanks[0] = new byte[8192];
                    break;
                case 0x03:
                    RAMBanks = new byte[4][];
                    for (int i = 0; i < RAMBanks.Length; i++)
                    {
                        RAMBanks[i] = new byte[8192];
                    }
                    break;
                case 0x04:
                    RAMBanks = new byte[16][];
                    for (int i = 0; i < RAMBanks.Length; i++)
                    {
                        RAMBanks[i] = new byte[8192];
                    }
                    break;
                case 0x05:
                    RAMBanks = new byte[8][];
                    for (int i = 0; i < RAMBanks.Length; i++)
                    {
                        RAMBanks[i] = new byte[8192];
                    }
                    break;
                default:
                    RAMBanks = new byte[0][];
                    break;
            }
        }
    }

    public byte Read(ushort address)
    {
        //Note to self, to keep things speedy, and ensure memory correctness, make sure write handles all of this
        switch (address)
        {
            //VRAM
            case ushort i when (i >= 0x8000 && i < 0xA000):
            {
                PPUMode mode = GetPPUMode();
                if (mode == PPUMode.ScanlineVRAM_3 && lcdControl.LCDPower)
                    return 0xFF; //Reading blocked!
                return memory[address];
            }
            //OAM
            case ushort i when (i >= 0xFE00 && i < 0xFEA0):
            {
                PPUMode mode = GetPPUMode();
                if ((mode == PPUMode.ScanlineOAM_2 || mode == PPUMode.ScanlineVRAM_3) && lcdControl.LCDPower)
                    return 0xFF; //Reading blocked!
                return memory[address];
            }
            //Read ram
            case ushort i when (i >= 0xA000 && i < 0xC000):
            {
                if (RAMEnabled)
                {
                    switch (header.MBCType)
                    {
                        case GBCartridgeMBCType.MBC2:
                            return (byte) (memory[(address & 0xA1FF)] | 0xF0); //first 4 bits are unreliable because they float so return 1's
                        case GBCartridgeMBCType.MBC3:
                            if (mbc3Regs.RAMRegister <= 0x03)
                            {
                                return memory[address];
                            }
                            return mbc3Regs.GetRTCData();
                        default:
                            return memory[address];
                            
                    }
                }
                //DO NOTHING IF RAM DISABLED
                return 0xFF;
            }
            //Sound registers
            case ushort i when (i >= 0xFF10 && i < 0xFF40):
            {
                return ReadSoundAddress(address);
            }
            default:
            {
                return memory[address];
            }
        }
    }

    public byte ReadPPU(ushort address)
    {
        return memory[address];
    }

    /// <summary>
    /// Gets direct access to an area of memory, read only.
    /// </summary>
    public ReadOnlySpan<byte> GetMemorySegment(ushort beginAddress, int length)
    {
        return new ReadOnlySpan<byte>(memory, beginAddress, length);
    }

    public event Action<ushort> TileMemoryChanged = delegate { };
    public event Action<ushort, byte> AudioRegisterChanged = delegate { };
    public event Action ApuTick = delegate { };

    /// <summary>
    /// Circumvents all limits applied to the memory
    /// Usually only for registers that write to their own read-only areas.
    /// </summary>
    public void WriteAsRegister(ushort address, byte data)
    {
        memory[address] = data;
        UpdateMemoryMappedIO(address, data);
    }

    public void Write(ushort address, byte data)
    {
        //WRITE DATA
        if (address < 0x8000)
        {
            //Read only memory. NEVER WRITE. But can be used for MemoryBankControllers to read pointers.
            MemoryBankControllerInput(address, data);
        }
        else if(address < 0xA000)
        {//VRAM area
            //TODO To pass the PPU block tests, this must be on, but the CPU is not compliant yet in timing.
            PPUMode mode = GetPPUMode();
            if (mode == PPUMode.ScanlineVRAM_3 && lcdControl.LCDPower)
                return; //No write allowed!
            memory[address] = data;
        }
        else if (address < 0xC000)
        {//External RAM area
            if (RAMEnabled)
            {
                if (header.IsMBC2())
                {
                    memory[(address & 0xA1FF)] = (byte)(data & 0x0F); //first 4 bits are unreliable because they float so no write
                }
                else if (header.IsMBC3())
                {
                    if (mbc3Regs.RAMRegister <= 0x03)
                    {
                        memory[address] = data;
                    }
                    else
                    {
                        mbc3Regs.SetRTCData(data);
                    }
                }
                else
                {
                    memory[address] = data;
                }
            }
            //Ignore write if RAM is disabled
            return;
        }
        else if (address < 0xFE00)
        {//Work RAM & Echo RAM
            memory[address] = data;
            //Echo
            if (address >= 0xE000)
            {
                memory[address - 0x2000] = data;
            }
            else if (address < 0xDE00)
            {
                memory[address + 0x2000] = data;
            }
        }
        else if (address < 0xFEA0)
        {//OAM
            PPUMode mode = GetPPUMode();
            if ((mode == PPUMode.ScanlineOAM_2 || mode == PPUMode.ScanlineVRAM_3) && lcdControl.LCDPower)
                return; //No write allowed!
            memory[address] = data;
        }
        else if (address < 0xFF80)
        {
            WriteRegisterData(address, data);
        }
        else if(address < 0xFFFE)
        {
            //write to memory normally
            memory[address] = data;
        }
        else
        {
            WriteRegisterData(address, data);
        }

        UpdateMemoryMappedIO(address, data);
    }

    /// <summary>
    /// Because these have a lot of small exceptions, it has its own extracted method
    /// </summary>
    private void WriteRegisterData(ushort address, byte data)
    {
        //Not usable memory space
        if ((address >= 0xFEA0 && address < 0xFF00) ||
            (address >= 0xFF4C && address < 0xFF80))
        {
            memory[address] = 0xFF;
            return;
        }
        
        //Audio registers
        if (address >= 0xFF10 && address < 0xFF40)
        {
            //Read only when powered off except for the power control!
            bool ApuPowered = Util.BitCheck(7, memory[0xFF26]);
            if (ApuPowered || address == 0xFF26)
            {
                if (address == 0xFF26)
                {
                    //enable/disable the power to APU
                    memory[address] = (byte)((0xF0 & data) | (0x0F & memory[address]));
                    return;
                }
                else
                {
                    //all other registers are regular writes.
                    memory[address] = data;
                    return;
                }
            }
        }
        switch (address)
        {
            case 0xFF00:
            {
                //lower nibble doesn't change, highest bits forced on
                int lowerNibble = memory[address] & 0x0F;
                int upperNibble = data & 0b00110000;
                upperNibble |= 0b11000000;
                memory[address] = (byte) (upperNibble | lowerNibble);
                break;
            }
            case 0xFF02:
            {
                memory[address] = (byte)(data | 0b01111110);
                break;
            }
            case 0xFF04:
            {
                //reset timer
                DIVTimer = 0;
                memory[address] = 0;
                break;
            }
            case 0xFF07:
            {
                memory[address] = (byte)(data | 0b11111000);
                break;
            }
            case 0xFF0F:
            {
                memory[address] = (byte)(data | 0b11100000);
                break;
            }
            case 0xFF41:
            {
                //LCD register
                byte curr = memory[address];
                data &= 0xF8; //bit 0,1,2 are zero'ed out, they are read only.
                curr &= 0x07; //get only first 3 bits
                data |= curr; //merge the two into full byte and write that.
                memory[address] = data;
                break;
            }
            case 0xFFFF:
            {
                memory[address] = (byte)(data | 0b11100000);
                break;
            }
            case 0xFF03:
            case 0xFF08:
            case 0xFF09:
            case 0xFF0A:
            case 0xFF0B:
            case 0xFF0C:
            case 0xFF0D:
            case 0xFF0E:
            case 0xFF1F:
            case 0xFF27:
            case 0xFF28:
            case 0xFF29:
            case 0xFF2A:
            case 0xFF2B:
            case 0xFF2C:
            case 0xFF2D:
            case 0xFF2E:
            case 0xFF2F:
            case 0xFF50:
                //Unreadable Registers
                memory[address] = 0xFF;
                break;
            default:
                memory[address] = data;
                break;
        }
    }

    public byte ReadJoyPadInput()
    {
        return Read(0xFF00);
    }
    
    public void WriteJoyPadInput(byte newInput)
    {
        byte oldData = ReadJoyPadInput();
        WriteAsRegister(0xFF00, newInput);
        byte newData = ReadJoyPadInput();
        CheckJoyPadInterrupt(oldData, newData);
    }
    
    private void CheckJoyPadInterrupt(byte oldInput, byte newInput)
    {
        bool selectorBitActive = (oldInput & 0x30) != 0;
        int oldInputBits = oldInput & 0x0F;
        int newInputBits = newInput & 0x0f;
        bool fallingEdge = ((oldInputBits ^ newInputBits) & oldInputBits) != 0;
        if (selectorBitActive && fallingEdge)
        {
            //Set interrupt flag
            memory[0xFF0F] |= (byte) Interrupt.Joypad;
        }
    }

    private PPUMode GetPPUMode()
    {
        return (PPUMode) (Read(0xFF41) & 0x03);
    }

    /// <summary>
    /// Updates things outside of regular memory stuff.
    /// If a direct write happens, this will also be triggered to ensure the right systems are notified of the changed memory.
    /// </summary>
    private void UpdateMemoryMappedIO(ushort address, byte data)
    {
        //adresses to act on by machine/events        
        if (address >= 0x8000 && address < 0x9800)
        {
            TileMemoryChanged.Invoke(address);
        }
        else if (address == 0xFF46)
        {//DMA transfer, use data to copy 160 bytes from 0x##00 to 0xFE00
            //Usually takes 671 cycles, but lets see how this rolls instantly.
            ushort copyFrom = (ushort)(data << 8);
            //TODO actually simulate DMA copy times
            Array.Copy(memory, copyFrom, memory, 0xFE00, 160);
        }
        else if (address == 0xFF50 && data != 0x00)
        {
            //Disable the bootrom by overwriting the addresses with rom bank
            LoadFirstROMBank(0);
        }
        else if (address == 0xFF40)//LCDC (set LCD control stuff)
        {
            lcdControl = new LCDControl(data);
        }
        else if (address == 0xFF45)
        {
            //Always update LY concidence bit
            byte statRegister = (byte)(Read(0xFF41) & 0b11111011);
            byte LYval = memory[0xFF44];
            byte LYCval = memory[0xFF45];
            bool coincidence = LYval == LYCval;
            if (coincidence)
            {
                statRegister |= 1 << 2;
            }
            WriteAsRegister(0xFF41, statRegister);
        }
        else if (address >= 0xFF10 && address < 0xFF40)
        {
            AudioRegisterChanged(address, data);
        }
    }

    private byte ReadSoundAddress(ushort address)
    {
        //unused area
        if (address >= 0xFF27 && address < 0xFF30)
            return 0xFF;
        //wave data
        if (address >= 0xFF30 && address < 0xFF40)
        {
            if (GetAudioChannelActive(AudioChannel.Channel3))
                return 0xFF;
            return memory[address];
        }

        //adresses sound register
        switch (address)
        {
            case 0xFF10:
                return (byte)(memory[address] | 0x80);
            case 0xFF11:
                return (byte) (memory[address] | 0x3F);
            case 0xFF12:
                return memory[address];
            case 0xFF13:
                return 0xFF;
            case 0xFF14:
                return (byte)(memory[address] | 0xBF);
            case 0xFF15:
                return 0xFF;
            case 0xFF16:
                return (byte)(memory[address] | 0x3F);
            case 0xFF17:
                return memory[address];
            case 0xFF18:
                return 0xFF;
            case 0xFF19:
                return (byte)(memory[address] | 0xBF);
            case 0xFF1A:
                return (byte)(memory[address] | 0x7F);
            case 0xFF1B:
                return 0xFF;
            case 0xFF1C:
                return (byte)(memory[address] | 0x9F);
            case 0xFF1D:
                return 0xFF;
            case 0xFF1E:
                return (byte)(memory[address] | 0xBF);
            case 0xFF1F:
                return 0xFF;
            case 0xFF20:
                return 0xFF;
            case 0xFF21:
                return memory[address];
            case 0xFF22:
                return memory[address];
            case 0xFF23:
                return (byte)(memory[address] | 0xBF);
            case 0xFF24:
                return memory[address];
            case 0xFF25:
                return memory[address];
            case 0xFF26:
                return (byte)(memory[address] | 0x70);
            default:
                return memory[address];
        }
    }
    
    private bool GetAudioChannelActive(AudioChannel channel)
    {
        byte controlRegs = Read(0xFF26);
        byte flag = (byte) (1 << (int)channel);
        return (controlRegs & flag) != 0;
    }

    private void MemoryBankControllerInput(ushort address, byte data)
    {
        switch (header.CartridgeType)
        {
            case GBCartridgeType.ROM_ONLY:
            case GBCartridgeType.ROM_RAM:
            case GBCartridgeType.ROM_RAM_BAT:
                //Nothign to do here
                break;
            case GBCartridgeType.MBC1:
            case GBCartridgeType.MBC1_RAM:
            case GBCartridgeType.MBC1_RAM_BAT:
                {
                    if (address < 0x2000)//enable ram and load first ram bank?
                    {
                        RAMEnabled = (data & 0x0F) == 0x0A;
                        if(RAMBanks.Length != 0)
                            LoadRAMBank(0x00);
                    }
                    else if (address < 0x4000)//Load ROM bank lower bits
                    {
                        byte readValue = (byte)(data & 0x1F);//only 5 bits max read in
                        if (readValue == 0)
                            readValue++;
                        mbc1Regs.lowRomBank = readValue;
                        byte newBankToLoad = (byte)(mbc1Regs.lowRomBank | mbc1Regs.highRomBankOffset);
                        if (newBankToLoad >= header.ROMBankCount)//remove all unused bits depending on max bank count
                            newBankToLoad = (byte)(newBankToLoad % header.ROMBankCount);
                        LoadSecondROMBank(newBankToLoad);
                    }
                    else if (address < 0x6000)//Load ROM bank upper bits OR RAM bank
                    {
                        mbc1Regs.highRomBank = (byte)(data & 0x03);//read only the first 2 bits
                        //Always load normal banks as intended
                        byte newBankToLoad = (byte)(mbc1Regs.lowRomBank | mbc1Regs.highRomBankOffset);
                        if (newBankToLoad >= header.ROMBankCount)//remove all unused bits depending on max bank count
                            newBankToLoad = (byte)(newBankToLoad % header.ROMBankCount);
                        LoadSecondROMBank(newBankToLoad);
                        //if we are in mode1, check ram or extended ROM functionality
                        if(mbc1Regs.bankingMode)
                        {//advanced mode
                            if (header.LargeRAM)
                            {//large ram
                                LoadRAMBank(mbc1Regs.highRomBank);
                            }
                            else
                            {//large rom (load first bank with ROM)
                                //load ROM0 with 00/20/40/60
                                newBankToLoad = mbc1Regs.highRomBankOffset;
                                if (newBankToLoad >= header.ROMBankCount)//remove all unused bits depending on max bank count
                                    newBankToLoad = (byte)(newBankToLoad % header.ROMBankCount);
                                LoadFirstROMBank(newBankToLoad);
                            }
                        }
                    }
                    else if (address < 0x8000)//set ram mode
                    {
                        bool newState = (data & 0x01) == 1;
                        if (mbc1Regs.bankingMode != newState)
                        {
                            mbc1Regs.bankingMode = newState;
                            MBC1ModeChange();
                        }
                    }
                }
                break;
            case GBCartridgeType.MBC2:
            case GBCartridgeType.MBC2_BAT:
                {
                    if (address < 0x4000)//enable ram and load first ram bank?
                    {
                        byte readValue = (byte)(data & 0x0F);//only 4 bits max read in
                        if ((address & 0x0100) == 0x0000)
                        {//RAMG
                            RAMEnabled = readValue == 0x0A;
                            LoadRAMBank(0x00);
                        }
                        else
                        {//ROM2
                            if (readValue == 0)
                                readValue++;
                            if (readValue >= header.ROMBankCount)//remove all unused bits depending on max bank count
                                readValue = (byte)(readValue % header.ROMBankCount);
                            LoadSecondROMBank(readValue);
                        }
                    }
                }
                break;
            case GBCartridgeType.MBC3:
            case GBCartridgeType.MBC3_TIMER_BAT:
            case GBCartridgeType.MBC3_TIMER_RAM_BAT:
            case GBCartridgeType.MBC3_RAM:
            case GBCartridgeType.MBC3_RAM_BAT:
                {
                    if (address < 0x2000)//enable/disable RAM/RTC
                    {
                        if (header.RAMBankCount == 0)
                            return;
                        RAMEnabled = (data & 0x0F) == 0x0A;
                    }
                    else if (address < 0x4000)//switch ROM bank
                    {
                        byte bank = (byte)(data & 0x7F);//first 7 bits only
                        if (bank == 0x00)//always correct 00000 to 00001 because bank 0 is unavailable in MBC3
                            bank++;
                        if (bank >= header.ROMBankCount)//remove all unused bits depending on max bank count
                            bank = (byte)(bank % header.ROMBankCount);
                        LoadSecondROMBank(bank);
                    }
                    else if (address < 0x6000)//switch RAM bank or RTC
                    {
                        mbc3Regs.RAMRegister = data;
                        if (mbc3Regs.RAMRegister <= 0x03)
                        {
                            LoadRAMBank(mbc3Regs.RAMRegister);
                        }
                        /*if(mbc3Regs.RAMRegister > 0x07 && mbc3Regs.RAMRegister < 0x0D)
                        {
                            Debug.Log("RTC REG ACCESS " + mbc3Regs.RAMRegister.ToString("X2"));
                        }*/
                        //Also loads in RTC clock but we catch that at READ
                    }
                    else if (address < 0x8000)//latch in clock data (set current time to RTC)
                    {
                        if(mbc3Regs.latchRegister == 0x00 && data == 0x01)
                        {
                            mbc3Regs.LatchRTC();
                            //Debug.Log("RTC Latched: " + mbc3Regs.RTCLatched);
                        }
                        mbc3Regs.latchRegister = data;
                    }
                }
                break;
            case GBCartridgeType.MBC5:
            case GBCartridgeType.MBC5_RAM:
            case GBCartridgeType.MBC5_RAM_BAT:
            case GBCartridgeType.MBC5_RUMBLE:
            case GBCartridgeType.MBC5_RUMBLE_RAM:
            case GBCartridgeType.MBC5_RUMBLE_RAM_BAT:
                {
                    if (address < 0x2000)//enable/disable RAM/RTC
                    {
                        if (header.RAMBankCount == 0)
                            return;
                        RAMEnabled = data == 0x0A;
                    }
                    else if (address < 0x3000)//switch ROM bank lower
                    {
                        mbc5Regs.lowRomBank = data;
                        byte newBankToLoad = (byte)(mbc5Regs.lowRomBank | mbc5Regs.highRomBankOffset);
                        if (newBankToLoad >= header.ROMBankCount)//remove all unused bits depending on max bank count
                            newBankToLoad = (byte)(newBankToLoad % header.ROMBankCount);
                        LoadSecondROMBank(newBankToLoad);
                    }
                    else if (address < 0x4000)//switch ROM bank upper
                    {
                        mbc5Regs.highRomBank = (byte)(data & 0x01);//only 1 bit upper rom register
                        byte newBankToLoad = (byte)(mbc5Regs.lowRomBank | mbc5Regs.highRomBankOffset);
                        if (newBankToLoad >= header.ROMBankCount)//remove all unused bits depending on max bank count
                            newBankToLoad = (byte)(newBankToLoad % header.ROMBankCount);
                        LoadSecondROMBank(newBankToLoad);
                    }
                    else if (address < 0x6000)//switch RAM bank
                    {
                        byte ramBank = (byte)(data & 0x0F);
                        if (header.CartridgeType >= GBCartridgeType.MBC5_RUMBLE)
                        {
                            //3rd bit controls rumble
                            cartEvents.SetRumble((ramBank & 0x08) != 0);
                        }
                        if (ramBank >= header.RAMBankCount)//remove all unused bits depending on max bank count
                            ramBank = (byte)(ramBank % header.RAMBankCount);
                        LoadRAMBank(ramBank);
                    }
                }
                break;
            default:
                UnityEngine.Debug.LogError("Cartridge Type UNSUPPORTED: "+ header.CartridgeType.ToString());
                break;
        }
    }

    private void MBC1ModeChange()
    {
        if (mbc1Regs.bankingMode)
        {//advanced mode

            if (header.LargeRAM)
            {//large ram
                LoadRAMBank(mbc1Regs.highRomBank);
            }
            else
            {//large rom
                byte bankToLoad = mbc1Regs.highRomBankOffset;
                if (bankToLoad >= header.ROMBankCount)//remove all unused bits depending on max bank count
                    bankToLoad = (byte)(bankToLoad % header.ROMBankCount);
                LoadFirstROMBank(bankToLoad);
            }
        }
        else
        {//regular mode
            LoadRAMBank(0);
            LoadFirstROMBank(0);
        }
    }

    public void SaveRAMNow()
    {
#if DEBUGCPU
        return;
#endif
        //load current bank to force writing it away and saving
        if(loadedRAMBank != -1)
            LoadRAMBank(0x00, true);
    }

    private void SaveRAMtoFile()
    {
        using (FileStream fs = new FileStream(ramSavePath, FileMode.Create))
        {
            var bw = new BinaryWriter(fs);
            bw.Write(RAMBanks.Length);
            bw.Write(RAMBanks[0].Length);
            for (int i = 0; i < RAMBanks.Length; i++)
            {
                bw.Write(RAMBanks[i]);
            }
        }
    }

    private void LoadRAMFromFile()
    {
        if (!File.Exists(ramSavePath))
        {
            UnityEngine.Debug.Log($"Save file not found, starting without: {ramSavePath}");
            return;
        }
        using (FileStream fs = new FileStream(ramSavePath, FileMode.Open))
        {
            var br = new BinaryReader(fs);
            int banks = br.ReadInt32();
            int bankSize = br.ReadInt32();
            RAMBanks = new byte[banks][];
            for (int i = 0; i < RAMBanks.Length; i++)
            {
                RAMBanks[i] = br.ReadBytes(bankSize);
            }
            UnityEngine.Debug.Log("Loaded save file.");
        }
    }

    private int serialClockCounter;
    private const int cartridgeHeaderOffset = 0x134;

    public void ClockSerialTransfer()
    {
        byte serialControl = Read(0xFF02);
        //are we running internal clock and transfering?
        if ((serialControl & 0x01) != 0x00 && (serialControl & 0x80) != 0x00)
        {
            //send in 1
            byte serialData = Read(0xFF01);
            serialData = (byte)((serialData << 1) + 1);
            WriteAsRegister(0xFF01, serialData);
            serialClockCounter++;
            //byte transfered
            if(serialClockCounter >= 8)
            {
                serialClockCounter = 0;
                serialControl = (byte)(serialControl & 0x7F);
                Write(0x0FF02, serialControl);
            }
        }
        if (Read(0xFF02) == 0x01)
        {
            //Set the serial overflow interrupt
            memory[0xFF0F] |= (byte)Interrupt.SerialTransfer;
        }
    }
}