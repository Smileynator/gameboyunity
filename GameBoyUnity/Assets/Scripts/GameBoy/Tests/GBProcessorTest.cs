using System;
using System.IO;
using NUnit.Framework;
using UnityEngine;

public class GBProcessorTest
{
    private MockMemory memory;
    private GBClock clock;

    private byte EI = 0xFB;
    private byte DI = 0xF3;
    private byte RETI = 0xD9;
    private byte HALT = 0x76;
    private ushort IF = 0xFF0F;
    private ushort IE = 0xFFFF;
    private byte vblank = 0x40, lcd = 0x48, timer = 0x50, serial = 0x58, joypad = 0x60;

    private void SetAllInterruptAddressesToRETI()
    {
        memory.Write(vblank, RETI);
        memory.Write(lcd, RETI);
        memory.Write(timer, RETI);
        memory.Write(serial, RETI);
        memory.Write(joypad, RETI);
    }

    [SetUp]
    public void SetUp()
    {
        memory = new MockMemory();
        clock = new GBClock();
    }
    
    [Test]
    public void CheckIMEFlagNotEnabledIfQuicklyDisabled()
    {
        memory.Clear();
        memory.Write(0x0000, EI);
        memory.Write(0x0001, DI);
        var processor = new GBProcessor(memory, clock);
        Assert.IsFalse(processor.CheckIMEFlag, "Starts off");
        clock.ClockMCycle();
        Assert.IsFalse(processor.CheckIMEFlag, "Off for 1 operation");
        clock.ClockMCycle();
        Assert.IsFalse(processor.CheckIMEFlag, "Should be Off because of DI");
        clock.ClockMCycle();
        Assert.IsFalse(processor.CheckIMEFlag, "Should be Off because of DI");
    }
    
    [Test]
    public void CheckNoInterruptsWithoutIME()
    {
        memory.Clear();
        byte interruptFlags = 0xFF;
        memory.Write(IE, interruptFlags);
        memory.Write(IF, interruptFlags);
        var processor = new GBProcessor(memory, clock);
        Assert.IsFalse(processor.CheckIMEFlag, "Starts off");
        clock.ClockMCycles(2);
        Assert.IsFalse(processor.CheckIMEFlag, "Should be off still");
        clock.ClockMCycles(2);
        Assert.AreEqual(4, processor.CheckPC, "No interrupts should have happened");
        Assert.AreEqual(0xFF, memory.Read(IF), "No interrupt flags should have cleared");
    }
    
    [Test]
    public void CheckNoInterruptsWithoutEnables()
    {
        memory.Clear();
        memory.Write(0x0000, EI);
        byte interruptFlags = 0xFF;
        memory.Write(IE, 0x00);
        memory.Write(IF, interruptFlags);
        var processor = new GBProcessor(memory, clock);
        Assert.IsFalse(processor.CheckIMEFlag, "Starts off");
        clock.ClockMCycles(2);
        Assert.IsFalse(processor.CheckIMEFlag, "Should be off still");
        clock.ClockMCycles(2);
        Assert.AreEqual(4, processor.CheckPC, "No interrupts should have happened");
        Assert.AreEqual(0xFF, memory.Read(IF), "No interrupt flags should have cleared");
    }
    
    [Test]
    public void CheckIMEFlagEnabledAfter1Instr()
    {
        memory.Clear();
        memory.Write(0x0000, EI);
        var processor = new GBProcessor(memory, clock);
        Assert.IsFalse(processor.CheckIMEFlag);
        clock.ClockMCycle();
        Assert.IsFalse(processor.CheckIMEFlag);
        clock.ClockMCycle();
        Assert.IsFalse(processor.CheckIMEFlag);
        clock.ClockMCycle();
        Assert.IsTrue(processor.CheckIMEFlag);
    }
    
    [Test]
    public void CheckIMERepeatedEnable()
    {
        memory.Clear();
        memory.Write(0x0000, EI);
        memory.Write(0x0001, EI);
        memory.Write(0x0002, EI);
        memory.Write(0x0003, EI);
        var processor = new GBProcessor(memory, clock);
        Assert.IsFalse(processor.CheckIMEFlag);
        clock.ClockMCycle();
        Assert.IsFalse(processor.CheckIMEFlag);
        clock.ClockMCycle();
        Assert.IsFalse(processor.CheckIMEFlag);
        clock.ClockMCycle();
        Assert.IsTrue(processor.CheckIMEFlag);
        clock.ClockMCycle();
        Assert.IsTrue(processor.CheckIMEFlag);
    }
    
    [Test]
    public void CheckAllInterruptsHandling()
    {
        for (int i = 0; i < 5; i++)
        {
            byte jumpAddress = (byte) (0x40 + 0x08 * i);
            MockMemory memory = new MockMemory();
            memory.Clear();
            memory.Write(0x0000, EI);
            SetAllInterruptAddressesToRETI();
            byte interruptFlag = (byte) (1 << i);
            memory.Write(IE, interruptFlag);
            memory.Write(IF, interruptFlag);
            var processor = new GBProcessor(memory, clock);
            clock.ClockMCycles(2);//wait for interrupts to become active after 2 NOOP
            Assert.AreNotEqual(jumpAddress, processor.CheckPC, $"Jumping to Interrupt should take 5 cycles - {(Interrupt)interruptFlag}");
            for (int j = 0; j < 4; j++)
            {
                clock.ClockMCycle();
                Assert.AreNotEqual(jumpAddress, processor.CheckPC, $"Jumping to Interrupt should take 5 cycles - {(Interrupt)interruptFlag} failed at {j+1}");
            }
            clock.ClockMCycle();
            //We passed the 5th cycle, we should be at the interrupt address
            Assert.AreEqual(jumpAddress, processor.CheckPC, $"PC Should be at interrupt address after 5 cycles - {(Interrupt)interruptFlag}");
            Assert.IsFalse(processor.CheckIMEFlag, "Interrupt should disable IME");
            bool interruptFlagActive = (memory.Read(IF) & interruptFlag) != 0;
            Assert.IsFalse(interruptFlagActive, "Interrupt flag should be cleared after jumping");
        }
    }

    [Test]
    public void Noop()
    {
        memory.Clear();
        var processor = new GBProcessor(memory, clock);
        clock.ClockMCycle();
        Assert.AreEqual(0x0001, processor.CheckPC);
    }
    
    [Test]
    public void RegistersAfterBootRom()
    {
        string bootRomPath = Path.Join(Application.streamingAssetsPath, "GBBOOT_DMG_ROM.bin");
        string testRomPath = Path.Join(Application.streamingAssetsPath, "Tetris.gb");
        var bootRom = ReadROM(bootRomPath);
        var gameRom = ReadROM(testRomPath);
        //needs actual implementation to run to completion
        var realMemory = new GBMemory(Application.temporaryCachePath, bootRom, gameRom, clock, new MockGBCartEvents());
        var realScreen = new GBScreen(realMemory, clock, new TestingScreen());
        var processor = new GBProcessor(realMemory, clock);
        int bootRomCyclesNeeded = 5846143;
        while (processor.CheckPC != 0x100 && bootRomCyclesNeeded > 0)
        {
            clock.ClockMCycle();
            bootRomCyclesNeeded--;
        }
        Assert.AreEqual(0, bootRomCyclesNeeded, "Boot ROM did not finish within cycle limit");
        Assert.AreEqual(0x0100, processor.CheckPC);
        Assert.AreEqual(0x01B0, processor.AF);
        Assert.AreEqual(0x0013, processor.BC);
        Assert.AreEqual(0x00D8, processor.DE);
        Assert.AreEqual(0x014D, processor.HL);
        Assert.AreEqual(0xFFFE, processor.CheckSP);
    }
    
    [Test]
    public void RegistersNoBootRom()
    {
        string testRomPath = Path.Join(Application.streamingAssetsPath, "Tetris.gb");
        var gameRom = ReadROM(testRomPath);
        //needs actual implementation to run to completion
        var realMemory = new GBMemory(Application.temporaryCachePath, null, gameRom, clock, new MockGBCartEvents());
        var processor = new GBProcessor(realMemory, clock);
        Assert.AreEqual(0x0100, processor.CheckPC);
        Assert.AreEqual(0x01B0, processor.AF);
        Assert.AreEqual(0x0013, processor.BC);
        Assert.AreEqual(0x00D8, processor.DE);
        Assert.AreEqual(0x014D, processor.HL);
        Assert.AreEqual(0xFFFE, processor.CheckSP);
    }
    
    [Test]
    public void HaltBasic()
    {
        memory.Clear();
        memory.Write(0x0000, EI);
        memory.Write(0x0002, HALT);
        var processor = new GBProcessor(memory, clock);
        clock.ClockMCycle();
        clock.ClockMCycle();
        Assert.IsFalse(processor.CheckHalted);
        clock.ClockMCycle();
        Assert.IsTrue(processor.CheckHalted);
        Assert.AreEqual(0x0003, processor.CheckPC);
        clock.ClockMCycle();
        Assert.IsTrue(processor.CheckHalted);
        Assert.AreEqual(0x0003, processor.CheckPC);
        //Now we will try to wake the CPU, and that should happen without eating the interrupt
        //And then immediately services it before any other opcodes
        byte interruptFlag = 1;
        memory.Write(IE, interruptFlag);
        memory.Write(IF, interruptFlag);
        clock.ClockMCycles(5);
        Assert.AreEqual(vblank, processor.CheckPC); 
        
    }
    
    private byte[] ReadROM(string romPath)
    {
        using (FileStream fs = new FileStream(romPath, FileMode.Open))
        {
            byte[] gbRom = new byte[fs.Length];
            fs.Read(gbRom, 0, (int)fs.Length);
            return gbRom;
        }
    }
}
