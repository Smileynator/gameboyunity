using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class SpriteAttributeTest
{
    [Test]
    public void CheckBaseData()
    {
        SpriteAttribute sa = new SpriteAttribute(123, new byte[] { 0, 0, 13, 0 });
        Assert.AreEqual(123, sa.OAMIndex);
        Assert.AreEqual(-16, sa.yPos);
        Assert.AreEqual(-8, sa.xPos);
        Assert.AreEqual(13, sa.tileNumber);
    }
    
    [Test]
    public void CheckFlagsOn()
    {
        SpriteAttribute sa = new SpriteAttribute(0, new byte[] { 0, 0, 0, 0xF0});
        Assert.IsFalse(sa.priority);
        Assert.IsTrue(sa.verticalFlip);
        Assert.IsTrue(sa.horizontalFlip);
        Assert.IsTrue(sa.paletteOBP1);
        sa = new SpriteAttribute(0, new byte[] { 0, 0, 0, 0b10000000});
        Assert.IsFalse(sa.priority);
        sa = new SpriteAttribute(0, new byte[] { 0, 0, 0, 0b01000000});
        Assert.IsTrue(sa.verticalFlip);
        sa = new SpriteAttribute(0, new byte[] { 0, 0, 0, 0b00100000});
        Assert.IsTrue(sa.horizontalFlip);
        sa = new SpriteAttribute(0, new byte[] { 0, 0, 0, 0b00010000});
        Assert.IsTrue(sa.paletteOBP1);
    }
    
    [Test]
    public void CheckFlagsOff()
    {
        SpriteAttribute sa = new SpriteAttribute(0, new byte[] { 0, 0, 0, 0x00});
        Assert.IsTrue(sa.priority);
        Assert.IsFalse(sa.verticalFlip);
        Assert.IsFalse(sa.horizontalFlip);
        Assert.IsFalse(sa.paletteOBP1);
        sa = new SpriteAttribute(0, new byte[] { 0, 0, 0, 0b01111111});
        Assert.IsTrue(sa.priority);
        sa = new SpriteAttribute(0, new byte[] { 0, 0, 0, 0b10111111});
        Assert.IsFalse(sa.verticalFlip);
        sa = new SpriteAttribute(0, new byte[] { 0, 0, 0, 0b11011111});
        Assert.IsFalse(sa.horizontalFlip);
        sa = new SpriteAttribute(0, new byte[] { 0, 0, 0, 0b11101111});
        Assert.IsFalse(sa.paletteOBP1);
    }
}
