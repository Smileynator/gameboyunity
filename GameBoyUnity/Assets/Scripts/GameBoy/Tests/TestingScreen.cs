using System;
using UnityEngine;

public class TestingScreen : IGBScreenRenderer
{
    public Vector2Int screenSize;
    public byte[] screenData;
    
    public void OnInitialize(int width, int height)
    {
        screenSize = new Vector2Int(width, height);
        screenData = new byte[width * height];
    }

    public void OnFrameFinished(ref byte[] screenBuffer)
    {
        lock (screenData)
        {
            Array.Copy(screenBuffer, screenData, screenBuffer.Length);
        }
    }
}