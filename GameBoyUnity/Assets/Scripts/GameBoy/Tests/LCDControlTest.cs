using NUnit.Framework;

public class LCDControlTest
{
    [Test]
    public void EnabledBits()
    {
        LCDControl control = new LCDControl(0b00000001);
        Assert.True(control.BackgroundAndWindowOn);
        control = new LCDControl(0b00000010);
        Assert.True(control.ObjectsOn);
        control = new LCDControl(0b00000100);
        Assert.AreEqual(control.ObjectHeight, (byte)16);
        control = new LCDControl(0b00001000);
        Assert.True(control.BackgroundTileMapHigh);
        Assert.AreEqual(control.BackgroundTileMapAddress, 0x9C00);
        control = new LCDControl(0b00010000);
        Assert.True(control.BackgroundWindowTileDataMode);
        control = new LCDControl(0b00100000);
        Assert.True(control.WindowOn);
        control = new LCDControl(0b01000000);
        Assert.True(control.WindowTileMapHigh);
        Assert.AreEqual(control.WindowTileMapAddress, 0x9C00);
        control = new LCDControl(0b10000000);
        Assert.True(control.LCDPower);
    }
    
    [Test]
    public void DisabledBits()
    {
        LCDControl control = new LCDControl(0b11111110);
        Assert.False(control.BackgroundAndWindowOn);
        control = new LCDControl(0b11111101);
        Assert.False(control.ObjectsOn);
        control = new LCDControl(0b11111011);
        Assert.AreEqual(control.ObjectHeight, (byte)8);
        control = new LCDControl(0b11110111);
        Assert.False(control.BackgroundTileMapHigh);
        Assert.AreEqual(control.BackgroundTileMapAddress, 0x9800);
        control = new LCDControl(0b11101111);
        Assert.False(control.BackgroundWindowTileDataMode);
        control = new LCDControl(0b11011111);
        Assert.False(control.WindowOn);
        control = new LCDControl(0b10111111);
        Assert.False(control.WindowTileMapHigh);
        Assert.AreEqual(control.WindowTileMapAddress, 0x9800);
        control = new LCDControl(0b01111111);
        Assert.False(control.LCDPower);
    }
}