using NUnit.Framework;

public class GBClockTest
{
    private GBClock clock;
    
    [SetUp]
    public void SetUp()
    {
        clock = new GBClock();
    }
    
    [Test]
    public void CheckSingleClock()
    {
        int raises = 0;
        clock.MCycle += () => { raises++; };
        clock.ClockMCycle();
        Assert.AreEqual(1, raises);
    }
}
