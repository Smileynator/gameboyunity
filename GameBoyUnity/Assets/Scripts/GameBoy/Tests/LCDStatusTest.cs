using NUnit.Framework;

public class LCDStatusTest
{
    [Test]
    public void DisabledBits()
    {
        LCDStatus status = new LCDStatus(0b11111011);
        Assert.False(status.coincidenceLY);
        status = new LCDStatus(0b11110111);
        Assert.False(status.hBlankInterrupt);
        status = new LCDStatus(0b11101111);
        Assert.False(status.vBlankInterrupt);
        status = new LCDStatus(0b11011111);
        Assert.False(status.OAMInterrupt);
        status = new LCDStatus(0b10111111);
        Assert.False(status.coincidenceLYInterrupt);
    }

    [Test]
    public void StatusModes()
    {
        for (byte i = 0; i <= 0x03; i++)
        {
            LCDStatus status = new LCDStatus(i);
            Assert.AreEqual(status.mode, (PPUMode)i, $"Input was {i}");
        }   
    }

    [Test]
    public void EnabledBits()
    {
        LCDStatus status = new LCDStatus(0b00000100);
        Assert.True(status.coincidenceLY);
        status = new LCDStatus(0b00001000);
        Assert.True(status.hBlankInterrupt);
        status = new LCDStatus(0b00010000);
        Assert.True(status.vBlankInterrupt);
        status = new LCDStatus(0b00100000);
        Assert.True(status.OAMInterrupt);
        status = new LCDStatus(0b01000000);
        Assert.True(status.coincidenceLYInterrupt);
    }
}