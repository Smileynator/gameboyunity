using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using UnityEngine;

public class GBMemoryTest
{
    private readonly string testRomPath = Path.Join(Application.streamingAssetsPath, "Tetris.gb");
    private readonly string bootRomPath = Path.Join(Application.streamingAssetsPath, "GBBOOT_DMG_ROM.bin");
    private string savePath;
    private MockGBCartEvents cartEvents;
    private byte[] bootRom;
    private byte[] gameRom;
    private GBMemory memory;
    private GBClock clock;
    
    [SetUp]
    public void SetUp()
    {
        savePath = Path.Join("%TEMP%", "test.usav");
        if (File.Exists(savePath))
        {
            File.Delete(savePath);
        }
        bootRom = ReadROM(bootRomPath);
        gameRom = ReadROM(testRomPath);
        clock = new GBClock();
        cartEvents = new MockGBCartEvents();
    }

    private byte[] ReadROM(string romPath)
    {
        using (FileStream fs = new FileStream(romPath, FileMode.Open))
        {
            byte[] gbRom = new byte[fs.Length];
            fs.Read(gbRom, 0, (int)fs.Length);
            return gbRom;
        }
    }

    private void InitializeNoBootRom()
    {     
        memory = new GBMemory(savePath, null, gameRom, clock, cartEvents);
    }
    
    private void InitializeWithBootRom()
    {     
        memory = new GBMemory(savePath, bootRom, gameRom, clock, cartEvents);
    }
    
    [Test]
    public void CheckHasBootRom()
    {
        InitializeNoBootRom();
        Assert.False(memory.HasBootRom);
        InitializeWithBootRom();
        Assert.True(memory.HasBootRom);
    }
    
    [Test]
    public void CheckBootRomOverrideAndClear()
    {
        InitializeWithBootRom();
        var bootromData = memory.GetMemorySegment(0x0000, 0xFF);
        for (ushort i = 0; i < bootromData.Length; i++)
        {
            Assert.AreEqual(bootRom[i], bootromData[i]);
        }
        memory.Write(0xFF50, 0x01);
        for (ushort i = 0; i < bootromData.Length; i++)
        {
            Assert.AreEqual(gameRom[i], bootromData[i]);
        }
    }
    
    [Test]
    public void ReadRomBank0and1()
    {     
        InitializeNoBootRom();
        for (ushort i = 0x0000; i < 0x4000; i++)
        {
            byte read = memory.Read(i);
            Assert.AreEqual(gameRom[i], read, $"Bank 0 failure at {i:X4}");
        }
        for (ushort i = 0x4000; i < 0x8000; i++)
        {
            byte read = memory.Read(i);
            Assert.AreEqual(gameRom[i], read, $"Bank 1 failure at {i:X4}");
        }
    }
    
    [Test]
    public void WriteRomAreaFails()
    {     
        InitializeNoBootRom();
        for (ushort i = 0x0000; i < 0x8000; i++)
        {
            memory.Write(i, 0xF0);
            byte read = memory.Read(i);
            Assert.AreEqual(gameRom[i], read, $"Write was allowed in Read only area at {i:X4}");
        }
    }
    
    [Test]
    public void WriteReadInternalRam()
    {     
        InitializeNoBootRom();
        byte data = 1;
        for (ushort i = 0xC000; i < 0xE000; i++)
        {
            memory.Write(i, data);
            byte read = memory.Read(i);
            Assert.AreEqual(data, read, $"Work RAM failure at {i:X4}");
            data++;
        }
    }
    
    [Test]
    public void WriteReadVRamNormal()
    {     
        InitializeNoBootRom();
        byte data = 1;
        SetPPUMode(PPUMode.H_Blank_0);
        for (ushort i = 0x8000; i < 0xA000; i++)
        {
            memory.Write(i, data);
            byte read = memory.Read(i);
            Assert.AreEqual(data, read, $"Video RAM failure H_Blank_0 at {i:X4}");
            data++;
        }
        SetPPUMode(PPUMode.V_Blank_1);
        for (ushort i = 0x8000; i < 0xA000; i++)
        {
            memory.Write(i, data);
            byte read = memory.Read(i);
            Assert.AreEqual(data, read, $"Video RAM failure V_Blank_1 at {i:X4}");
            data++;
        }
        SetPPUMode(PPUMode.ScanlineOAM_2);
        for (ushort i = 0x8000; i < 0xA000; i++)
        {
            memory.Write(i, data);
            byte read = memory.Read(i);
            Assert.AreEqual(data, read, $"Video RAM failure ScanlineOAM_2 at {i:X4}");
            data++;
        }
    }
    
    [Test]
    public void WriteReadVRamBlocked()
    {     
        InitializeNoBootRom();
        for (ushort i = 0x8000; i < 0xA000; i++)
        {
            //write check
            SetPPUMode(PPUMode.H_Blank_0);
            memory.Write(i, 0x11);
            //write to blocked
            SetPPUMode(PPUMode.ScanlineVRAM_3);
            memory.Write(i, 0x55);
            byte read = memory.Read(i);
            Assert.AreEqual(0xFF, read, $"Video RAM Blocked failure at {i:X4}");
            //Read unblocked no change
            SetPPUMode(PPUMode.H_Blank_0);
            read = memory.Read(i);
            Assert.AreEqual(0x11, read, $"Video RAM Blocked failure at {i:X4}");
        }
        //Disabled LCD allows access
        memory.Write(0xFF40, 0x00);
        SetPPUMode(PPUMode.ScanlineVRAM_3);
        memory.Write(0x8000, 0x66);
        byte read2 = memory.Read(0x8000);
        Assert.AreEqual(0x66, read2, $"OAM O/I Blocked failure at {0x8000:X4}");
    }
    
    [Test]
    public void WriteReadExternalRamWithoutRamFails()
    {     
        InitializeNoBootRom();
        for (ushort i = 0xA000; i < 0xC000; i++)
        {
            memory.Write(i, 0x55);
            byte read = memory.Read(i);
            Assert.AreEqual(0xFF, read, $"External RAM should not exist without RAM {i:X4}");
        }
    }
    
    //TODO test with ram, various setups?

    [Test]
    public void ReadWriteEchoRAM()
    {
        InitializeNoBootRom();
        byte data = 1;
        //normal read/writes in echo location
        for (ushort i = 0xE000; i < 0xFE00; i++)
        {
            memory.Write(i, data);
            byte read = memory.Read(i);
            Assert.AreEqual(data, read, $"Direct Echo RAM read/write failure at {i:X4}");
            data++;
        }
        //Read normal RAM to check echo
        data = 1;
        for (ushort i = 0xC000; i < 0xDE00; i++)
        {
            byte read = memory.Read(i);
            Assert.AreEqual(data, read, $"Indirect Echo RAM read failure at {i:X4}");
            data++;
        }
        //Write normal RAM read echo location
        data = 128;
        for (ushort i = 0xC000; i < 0xDE00; i++)
        {
            memory.Write(i, data);
            data++;
        } 
        data = 128;
        for (ushort i = 0xE000; i < 0xFE00; i++)
        {
            byte read = memory.Read(i);
            Assert.AreEqual(data, read, $"Direct Echo RAM read failure at {i:X4}");
            data++;
        }
    }
    
    [Test]
    public void WriteReadOAMNormal()
    {     
        InitializeNoBootRom();
        SetPPUMode(PPUMode.H_Blank_0);
        byte data = 1;
        for (ushort i = 0xFE00; i < 0xFEA0; i++)
        {
            memory.Write(i, data);
            byte read = memory.Read(i);
            Assert.AreEqual(data, read, $"OAM O/I failure at {i:X4}");
            data++;
        }
        SetPPUMode(PPUMode.V_Blank_1);
        for (ushort i = 0xFE00; i < 0xFEA0; i++)
        {
            memory.Write(i, data);
            byte read = memory.Read(i);
            Assert.AreEqual(data, read, $"OAM O/I failure at {i:X4}");
            data++;
        }
    }
    
    [Test]
    public void WriteReadOAMBlocked()
    {     
        InitializeNoBootRom();
        for (ushort i = 0xFE00; i < 0xFEA0; i++)
        {
            //Write 0x11 as check
            SetPPUMode(PPUMode.H_Blank_0);
            memory.Write(i, 0x11);
            
            //No write, read 0xFF
            SetPPUMode(PPUMode.ScanlineOAM_2);
            memory.Write(i, 0x55);
            byte read = memory.Read(i);
            Assert.AreEqual(0xFF, read, $"OAM O/I Blocked failure at {i:X4}");
            
            //No write, read 0xFF
            SetPPUMode(PPUMode.ScanlineVRAM_3);
            memory.Write(i, 0x66);
            read = memory.Read(i);
            Assert.AreEqual(0xFF, read, $"OAM O/I Blocked failure at {i:X4}");
            
            //Write enabled, read old data 0x11
            SetPPUMode(PPUMode.H_Blank_0);
            read = memory.Read(i);
            Assert.AreEqual(0x11, read, $"OAM O/I Blocked failure at {i:X4}");
        }
        //Disabled LCD allows access
        memory.Write(0xFF40, 0x00);
        SetPPUMode(PPUMode.ScanlineVRAM_3);
        memory.Write(0xFE00, 0x66);
        byte read2 = memory.Read(0xFE00);
        Assert.AreEqual(0x66, read2, $"OAM O/I Blocked failure at {0xFE00:X4}");
    }

    private void SetPPUMode(PPUMode mode)
    {
        memory.WriteAsRegister(0xFF41, (byte)mode);
    }
    
    [Test]
    public void ReadProhibitedArea()
    {     
        InitializeNoBootRom();
        for (ushort i = 0xFEA0; i < 0xFF00; i++)
        {
            memory.Write(i, 0x55);
            byte read = memory.Read(i);
            Assert.AreEqual(0xFF, read, $"Prohibited area failure at {i:X4}");
        }
    }
    
    [Test]
    public void WriteReadUnusedIORegisters()
    {     
        InitializeNoBootRom();
        List<ushort> addresses = new List<ushort>();
        addresses.Add(0xFF03);
        for (ushort i = 0xFF08; i < 0xFF0F; i++)
        {
            addresses.Add(i);
        }
        addresses.Add(0xFF1F);
        for (ushort i = 0xFF27; i < 0xFF30; i++)
        {
            addresses.Add(i);
        }
        for (ushort i = 0xFF4C; i < 0xFF80; i++)
        {
            addresses.Add(i);
        }

        byte data = 0x55;
        foreach (ushort address in addresses)
        {
            memory.Write(address, data);
            Assert.AreEqual(0xFF, memory.Read(address), $"Unused Address failure at {address:X4}");
        }
    }
    
    [Test]
    public void FF00_JoypadRegister()
    {     
        InitializeNoBootRom();
        byte data = 0x00;
        ushort address = 0xFF00;
        //No write read
        Assert.AreEqual(0b11000000, memory.Read(address), $"Joypad must read unused bits as 1");
        //upper bits are read only
        memory.Write(address, data);
        Assert.AreEqual(0b11000000, memory.Read(address), $"Joypad must read unused bits as 1");
        //lower nibble can't write
        data = 0x0F;
        memory.Write(address, data);
        Assert.AreEqual(0b11000000, memory.Read(address), $"lower nibble is read only");
        //bit 4 5 writes
        data = 0x3F;
        memory.Write(address, data);
        Assert.AreEqual(0b11110000, memory.Read(address), $"Bit 4 and 5 must be writable");
    }
    
    [Test]
    public void FF01_SerialTransferData()
    {     
        InitializeNoBootRom();
        byte data = 0xFF;
        ushort address = 0xFF01;
        Assert.AreEqual(0x00, memory.Read(address), $"No bits should be high initially");
        memory.Write(address, data);
        Assert.AreEqual(data, memory.Read(address), $"Written data should be preserved");
    }
    
    [Test]
    public void FF02_SerialTransferControl()
    {     
        InitializeNoBootRom();
        ushort address = 0xFF02;
        memory.Write(address, 0x00);
        Assert.AreEqual(0b01111110, memory.Read(address), $"There are always high");
        memory.Write(address, 0xFF);
        Assert.AreEqual(0xFF, memory.Read(address), $"Written data should be preserved");
    }
    
    [Test]
    public void FF04_DividerRegister()
    {     
        InitializeNoBootRom();
        ushort address = 0xFF04;
        Assert.AreEqual(0x00, memory.Read(address), $"Should start at zero until time ticks");
        //Cycle just enough to not tick Divider register
        for (int i = 0; i < 0b111111; i++)
        {
            clock.ClockMCycle();
        }
        Assert.AreEqual(0x00, memory.Read(address), $"Divider should still be zero");
        clock.ClockMCycle();
        Assert.AreEqual(0x01, memory.Read(address), $"Divider should now be 1");
        for (int i = 0; i < 0b1000000; i++)
        {
            clock.ClockMCycle();
        }
        Assert.AreEqual(0x02, memory.Read(address), $"Divider should now be 2");
        memory.Write(address, 0xFF);
        Assert.AreEqual(0x00, memory.Read(address), $"Resets to 0x00 after write");
        clock.ClockMCycle();
        Assert.AreEqual(0x00, memory.Read(address), $"1 Tick after reset should not set the divider again");
    }
    
    [Test]
    public void APUTimerTick()
    {     
        InitializeNoBootRom();
        int raises = 0;
        memory.ApuTick += () => { raises++; };
        ushort address = 0xFF04;
        //tick till 4th Divider bit is high
        for (int i = 0; i < 0b11111111111; i++)
        {
            clock.ClockMCycle();
        }
        Assert.AreEqual(0b11111, memory.Read(address), $"Divider should be primed for a tick");
        Assert.AreEqual(0, raises);
        //Falling edge on 4th bit
        clock.ClockMCycle();
        Assert.AreEqual(0b100000, memory.Read(address));
    }
    
    [Test]
    public void FF05_TimerCounter()
    {     
        InitializeNoBootRom();
        ushort address = 0xFF05;
        //Check read write behavior
        Assert.AreEqual(0x00, memory.Read(address), $"should be zero until time ticks");
        memory.Write(address, 0xFF);
        Assert.AreEqual(0xFF, memory.Read(address));
    }
    
    [Test]
    public void FF06_TimerModulo()
    {     
        InitializeNoBootRom();
        ushort address = 0xFF06;
        memory.Write(address, 0x00);
        Assert.AreEqual(0x00, memory.Read(address));
        memory.Write(address, 0xFF);
        Assert.AreEqual(0xFF, memory.Read(address));
    }
    
    [Test]
    public void FF07_TimerControl()
    {     
        InitializeNoBootRom();
        ushort address = 0xFF07;
        //Check read/write
        memory.Write(address, 0x00);
        Assert.AreEqual(0b11111000, memory.Read(address));
        memory.Write(address, 0xFF);
        Assert.AreEqual(0xFF, memory.Read(address));
    }
    
    [Test]
    public void TimerDisabled()
    {     
        InitializeNoBootRom();
        ushort TimaAddress = 0xFF05;
        ushort TacAddress = 0xFF07;
        //Check timer disabled
        memory.Write(TacAddress, 0b0);
        for (int i = 0; i < 0xFFFF; i++)
        {
            clock.ClockMCycle();
        }
        Assert.AreEqual(0, memory.Read(TimaAddress));
        
        memory.Write(TacAddress, 0b11);
        for (int i = 0; i < 0xFFFF; i++)
        {
            clock.ClockMCycle();
        }
        Assert.AreEqual(0, memory.Read(TimaAddress));
    }
    
    [Test]
    public void TimerEnabled()
    {     
        InitializeNoBootRom();
        const ushort TimaAddress = 0xFF05;
        const ushort TacAddress = 0xFF07;
        //Check timer enabled
        memory.Write(TacAddress, 0b100);
        for (int i = 0; i < 0b11111111; i++)
        {
            clock.ClockMCycle();
        }
        Assert.AreEqual(0, memory.Read(TimaAddress));
        clock.ClockMCycle();
        Assert.AreEqual(1, memory.Read(TimaAddress));
        clock.ClockMCycle();
        Assert.AreEqual(1, memory.Read(TimaAddress));
        for (int i = 0; i < 0b11111111; i++)
        {
            clock.ClockMCycle();
        }
        Assert.AreEqual(2, memory.Read(TimaAddress));
    }
    
    [Test]
    public void TimerSpeeds()
    {     
        InitializeNoBootRom();
        ushort DivAddress = 0xFF04;
        ushort TimaAddress = 0xFF05;
        ushort TacAddress = 0xFF07;
        //speed 1
        memory.Write(TacAddress, 0b101);
        memory.Write(TimaAddress, 0);
        memory.Write(DivAddress, 1);
        clock.ClockMCycles(3);
        Assert.AreEqual(0, memory.Read(TimaAddress));
        clock.ClockMCycle();
        Assert.AreEqual(1, memory.Read(TimaAddress));
        clock.ClockMCycles(3);
        Assert.AreEqual(1, memory.Read(TimaAddress));
        clock.ClockMCycle();
        Assert.AreEqual(2, memory.Read(TimaAddress));
        //speed 2
        memory.Write(TacAddress, 0b110);
        memory.Write(TimaAddress, 0);
        memory.Write(DivAddress, 1);
        clock.ClockMCycles(15);
        Assert.AreEqual(0, memory.Read(TimaAddress));
        clock.ClockMCycle();
        Assert.AreEqual(1, memory.Read(TimaAddress));
        clock.ClockMCycles(15);
        Assert.AreEqual(1, memory.Read(TimaAddress));
        clock.ClockMCycle();
        Assert.AreEqual(2, memory.Read(TimaAddress));
        //speed 3
        memory.Write(TacAddress, 0b111);
        memory.Write(TimaAddress, 0);
        memory.Write(DivAddress, 1);
        clock.ClockMCycles(63);
        Assert.AreEqual(0, memory.Read(TimaAddress));
        clock.ClockMCycle();
        Assert.AreEqual(1, memory.Read(TimaAddress));
        clock.ClockMCycles(63);
        Assert.AreEqual(1, memory.Read(TimaAddress));
        clock.ClockMCycle();
        Assert.AreEqual(2, memory.Read(TimaAddress));
        //speed 0
        memory.Write(TacAddress, 0b100);
        memory.Write(TimaAddress, 0);
        memory.Write(DivAddress, 1);
        clock.ClockMCycles(255);
        Assert.AreEqual(0, memory.Read(TimaAddress));
        clock.ClockMCycle();
        Assert.AreEqual(1, memory.Read(TimaAddress));
        clock.ClockMCycles(255);
        Assert.AreEqual(1, memory.Read(TimaAddress));
        clock.ClockMCycle();
        Assert.AreEqual(2, memory.Read(TimaAddress));
    }
    
    [Test]
    public void TimaOverflowBase()//does not cover more advanced behavior
    {     
        InitializeNoBootRom();
        ushort TimaAddress = 0xFF05;
        ushort TmaAddress = 0xFF06;
        ushort TacAddress = 0xFF07;
        //Trigger overflow
        memory.Write(TacAddress, 0b101);
        memory.Write(TmaAddress, 0x55);
        memory.Write(TimaAddress, 0xFF);
        clock.ClockMCycles(4);
        Assert.AreEqual(0x55, memory.Read(TimaAddress));
        clock.ClockMCycles(4);
        Assert.AreEqual(0x56, memory.Read(TimaAddress));
    }
    
    [Test]
    public void FF0F_InterruptFlags()
    {     
        InitializeNoBootRom();
        ushort address = 0xFF0F;
        memory.Write(address, 0x00);
        Assert.AreEqual(0b11100000, memory.Read(address));
        memory.Write(address, 0xFF);
        Assert.AreEqual(0xFF, memory.Read(address));
    }
    
    [Test]
    public void TimaOverflowInterrupt()//does not cover more advanced behavior
    {     
        InitializeNoBootRom();
        ushort TimaAddress = 0xFF05;
        ushort TmaAddress = 0xFF06;
        ushort TacAddress = 0xFF07;
        ushort interruptFlagsAddress = 0xFF0F;
        //Trigger overflow
        memory.Write(TacAddress, 0b101);
        memory.Write(TmaAddress, 0x55);
        memory.Write(TimaAddress, 0xFF);
        memory.Write(interruptFlagsAddress, 0x00);
        //Times should roll after 4 ticks
        clock.ClockMCycles(3);
        Assert.AreEqual(0b11100000, memory.Read(interruptFlagsAddress));
        clock.ClockMCycle();
        Assert.AreEqual(0b11100100, memory.Read(interruptFlagsAddress));
    }
    
    [Test]
    public void FF50_DisableBootRom()
    {     
        InitializeNoBootRom();
        ushort address = 0xFF50;
        memory.Write(address, 0x00);
        Assert.AreEqual(0xFF, memory.Read(address));
    }
    
    [Test]
    public void WriteReadHighRAM()
    {     
        InitializeNoBootRom();
        byte data = 1;
        for (ushort i = 0xFF80; i < 0xFFFE; i++)
        {
            memory.Write(i, data);
            byte read = memory.Read(i);
            Assert.AreEqual(data, read, $"High RAM failure at {i:X4}");
            data++;
        }
    }
    
    [Test]
    public void FFFF_InterruptEnable()
    {     
        InitializeNoBootRom();
        ushort address = 0xFFFF;
        memory.Write(address, 0x00);
        Assert.AreEqual(0b11100000, memory.Read(address));
        memory.Write(address, 0xFF);
        Assert.AreEqual(0xFF, memory.Read(address));
    }
    
    [Test]
    public void ReadPPUDirect()
    {     
        InitializeNoBootRom();
        byte data = 1;
        for (ushort i = 0x8000; i < 0xA000; i++)
        {
            SetPPUMode(PPUMode.H_Blank_0);
            memory.Write(i, data);
            SetPPUMode(PPUMode.ScanlineVRAM_3);
            byte read = memory.ReadPPU(i);
            Assert.AreEqual(data, read, $"PPU read failure at {i:X4}");
            data++;
        }
        data = 1;
        for (ushort i = 0xFE00; i < 0xFEA0; i++)
        {
            SetPPUMode(PPUMode.H_Blank_0);
            memory.Write(i, data);
            SetPPUMode(PPUMode.ScanlineVRAM_3);
            byte read = memory.ReadPPU(i);
            Assert.AreEqual(data, read, $"PPU read failure at {i:X4}");
            data++;
        }
    }
    
    [Test]
    public void GetMemorySegment()
    {     
        InitializeNoBootRom();
        var span = memory.GetMemorySegment(0x0010, 0xFF);
        Assert.AreEqual(0xFF, span.Length);
        for (ushort i = 0; i < 0xFF; i++)
        {
            byte result = memory.Read((ushort)(0x0010 + i));
            Assert.AreEqual(result, span[i]);
        }
    }
}
