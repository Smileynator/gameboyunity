using NUnit.Framework;

public class UtilTest
{
    [Test]
    public void BitCheck_TrueChecks()
    {
        for (int i = 0; i < 7; i++)
        {
            Assert.True(Util.BitCheck(0, 0xFF));
        }
        
        Assert.True(Util.BitCheck(0, 0x01));
        Assert.True(Util.BitCheck(1, 0x02));
        Assert.True(Util.BitCheck(2, 0x04));
        Assert.True(Util.BitCheck(3, 0x08));
        Assert.True(Util.BitCheck(4, 0x10));
        Assert.True(Util.BitCheck(5, 0x20));
        Assert.True(Util.BitCheck(6, 0x40));
        Assert.True(Util.BitCheck(7, 0x80));
    }
    
    [Test]
    public void BitCheck_FalseChecks()
    {
        for (int i = 0; i < 7; i++)
        {
            Assert.False(Util.BitCheck(0, 0x00));
        }
        
        Assert.False(Util.BitCheck(0, 0b11111110));
        Assert.False(Util.BitCheck(1, 0b11111101));
        Assert.False(Util.BitCheck(2, 0b11111011));
        Assert.False(Util.BitCheck(3, 0b11110111));
        Assert.False(Util.BitCheck(4, 0b11101111));
        Assert.False(Util.BitCheck(5, 0b11011111));
        Assert.False(Util.BitCheck(6, 0b10111111));
        Assert.False(Util.BitCheck(7, 0b01111111));
    }
}
