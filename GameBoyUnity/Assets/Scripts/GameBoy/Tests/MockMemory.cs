using System;

public class MockMemory : IGBMemory, IGBMemoryPPU
{
    public byte[] memory;
    public MockMemory()
    {
        Clear();
    }

    public void Write(ushort address, byte data)
    {
        memory[address] = data;
    }

    public void WriteAsRegister(ushort address, byte data)
    {
        memory[address] = data;
    }

    public byte ReadPPU(ushort address)
    {
        return Read(address);
    }

    public event Action<ushort> TileMemoryChanged;

    public void SaveRAMNow()
    {
    }

    public void ClockSerialTransfer()
    {
    }

    public bool HasBootRom => true;//force start at 0x0000
    public LCDControl LcdControl { get; }
    public byte Read(ushort address)
    {
        return memory[address];
    }

    public ReadOnlySpan<byte> GetMemorySegment(ushort beginAddress, int length)
    {
        return new ReadOnlySpan<byte>(memory, beginAddress, length);
    }

    public event Action<ushort, byte> AudioRegisterChanged;
    public event Action ApuTick;

    public void Clear()
    {
        memory = new byte[0x10000];//as long as memory will go in a gameboy anyway
    }
}
