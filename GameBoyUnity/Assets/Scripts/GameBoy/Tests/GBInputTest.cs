using NUnit.Framework;

public class GBInputTest
{
    private MockButtonInput mockInput;
    private GBClock clock;
    private GBMemory memory;
    private GBInput gbInput;
    private MockGBCartEvents cartEvents;
    private string savePath;
    
    [SetUp]
    public void SetUp()
    {
        mockInput = new MockButtonInput();
        clock = new GBClock();
        cartEvents = new MockGBCartEvents();
        memory = new GBMemory(savePath, null, null, clock, cartEvents);
        gbInput = new GBInput(clock, memory, mockInput);
    }

    private byte UpdateJoypad(byte input)
    {
        //Cycle a wrong input to ensure we get an updated output (optimization reasons)
        memory.WriteJoyPadInput(0xFF);
        clock.ClockMCycle();
        memory.WriteJoyPadInput(input);
        clock.ClockMCycle();
        return memory.ReadJoyPadInput();
    }
    
    [Test]
    public void UpperNibbleUnaffected()
    {
        mockInput.buttonStates = new ButtonPressedStates();
        byte mask = 0b11110000;
        for (byte i = 0; i < byte.MaxValue; i++)
        {
            byte input = (byte) (i & mask);
            byte result = UpdateJoypad(input);
            Assert.AreEqual(input, result & mask);
        }
        mockInput.buttonStates = new ButtonPressedStates()
        {
            A= true,
            B = true,
            Down = true,
            Left = true,
            Right = true,
            Up = true,
            Start = true,
            Select = true
        };
        for (byte i = 0; i < byte.MaxValue; i++)
        {
            byte input = (byte) (i & mask);
            byte result = UpdateJoypad(input);
            Assert.AreEqual(input, result & mask);
        }
    }
    
    [Test]
    public void NoSelectorFullOutput()
    {
        //If this does not give 1111, it would result in mimicking SGB!
        mockInput.buttonStates = new ButtonPressedStates();
        byte outputMask = 0b00001111;
        
        byte result = UpdateJoypad(0b00110000);
        Assert.AreEqual(0x0F, result & outputMask);
    }
    
    [Test]
    public void SelectorNoButtonHeldReturnsFullSignal()
    {
        mockInput.buttonStates = new ButtonPressedStates();
        byte outputMask = 0b00001111;
        
        byte result = UpdateJoypad(0b00000000);
        Assert.AreEqual(outputMask, result & outputMask);
        
        result = UpdateJoypad(0b00010000);
        Assert.AreEqual(outputMask, result & outputMask);
        
        result = UpdateJoypad(0b00100000);
        Assert.AreEqual(outputMask, result & outputMask);
    }
    
    [Test]
    public void SelectorAllButtonHeldReturnsNoSignal()
    {
        mockInput.buttonStates = new ButtonPressedStates()
        {
            A= true,
            B = true,
            Down = true,
            Left = true,
            Right = true,
            Up = true,
            Start = true,
            Select = true
        };
        byte outputMask = 0b00001111;
        byte result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b0000, result & outputMask);
        
        result = UpdateJoypad(0b00010000);
        Assert.AreEqual(0b0000, result & outputMask);
        
        result = UpdateJoypad(0b00100000);
        Assert.AreEqual(0b0000, result & outputMask);
    }
    
    [Test]
    public void LowerNibbleReadOnly()
    {
        mockInput.buttonStates = new ButtonPressedStates();
        byte outputMask = 0b00001111;
        byte selectNone = 0b00110000;
        for (byte i = 0; i < 0b00001111; i++)
        {
            //selected both
            byte result = UpdateJoypad(i);
            Assert.AreEqual(0b00001111, result & outputMask, $"Input was: {i}");
            //selected none
            result = UpdateJoypad((byte)(i | selectNone));
            Assert.AreEqual(0b00001111, result & outputMask, $"Input was: {i}");
        }
    }

    [Test]
    public void InputUnsetsCorrectBits()
    {
        byte outputMask = 0b00001111;
        byte buttonSelector = 0b00010000;
        byte directionSelector = 0b00100000;
        //Buttons
        mockInput.buttonStates = new ButtonPressedStates() { A = true };
        byte result = UpdateJoypad(buttonSelector);
        Assert.AreEqual(0b00001110, result & outputMask);
        
        mockInput.buttonStates = new ButtonPressedStates() { B = true };
        result = UpdateJoypad(buttonSelector);
        Assert.AreEqual(0b00001101, result & outputMask);
        
        mockInput.buttonStates = new ButtonPressedStates() { Select = true };
        result = UpdateJoypad(buttonSelector);
        Assert.AreEqual(0b00001011, result & outputMask);
        
        mockInput.buttonStates = new ButtonPressedStates() { Start = true };
        result = UpdateJoypad(buttonSelector);
        Assert.AreEqual(0b000000111, result & outputMask);
        
        //Directionals
        mockInput.buttonStates = new ButtonPressedStates() { Right= true };
        result = UpdateJoypad(directionSelector);
        Assert.AreEqual(0b00001110, result & outputMask);
        
        mockInput.buttonStates = new ButtonPressedStates() { Left = true };
        result = UpdateJoypad(directionSelector);
        Assert.AreEqual(0b00001101, result & outputMask);
        
        mockInput.buttonStates = new ButtonPressedStates() { Up = true };
        result = UpdateJoypad(directionSelector);
        Assert.AreEqual(0b000001011, result & outputMask);
        
        mockInput.buttonStates = new ButtonPressedStates() { Down = true };
        result = UpdateJoypad(directionSelector);
        Assert.AreEqual(0b000000111, result & outputMask);
    }
    
    [Test]
    public void BothSelectorsUnsetsCorrectBits()
    {
        byte outputMask = 0b00001111;
        
        mockInput.buttonStates = new ButtonPressedStates() { A = true, Right = true};
        byte result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b00001110, result & outputMask);
        mockInput.buttonStates = new ButtonPressedStates() { B = true, Left = true };
        result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b00001101, result & outputMask);
        mockInput.buttonStates = new ButtonPressedStates() { Select = true, Up = true };
        result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b00001011, result & outputMask);
        mockInput.buttonStates = new ButtonPressedStates() { Start = true,  Down = true};
        result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b000000111, result & outputMask);
        
        mockInput.buttonStates = new ButtonPressedStates() { A = true};
        result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b00001110, result & outputMask);
        mockInput.buttonStates = new ButtonPressedStates() { B = true};
        result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b00001101, result & outputMask);
        mockInput.buttonStates = new ButtonPressedStates() { Select = true};
        result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b00001011, result & outputMask);
        mockInput.buttonStates = new ButtonPressedStates() { Start = true};
        result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b000000111, result & outputMask);
        
        mockInput.buttonStates = new ButtonPressedStates() {Right = true};
        result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b00001110, result & outputMask);
        mockInput.buttonStates = new ButtonPressedStates() {Left = true };
        result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b00001101, result & outputMask);
        mockInput.buttonStates = new ButtonPressedStates() {Up = true };
        result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b00001011, result & outputMask);
        mockInput.buttonStates = new ButtonPressedStates() {Down = true};
        result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b000000111, result & outputMask);
    }
    
    [Test]
    public void JoyPadInterrupt()
    {
        byte outputMask = 0b00001111;
        ushort interruptAddress = 0xFF0F;
        memory.Write(interruptAddress, 0x00);
        mockInput.buttonStates = new ButtonPressedStates() { A = true };
        byte result = UpdateJoypad(0b00000000);
        Assert.AreEqual(0b00001110, result & outputMask);
        bool joyInterrupt = (byte)(memory.Read(interruptAddress) | 0b10000) != 0;
        Assert.IsTrue(joyInterrupt);
    }
}
