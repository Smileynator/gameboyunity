using System.IO;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Profiling;

public class GBScreenTest
{
    private readonly string testRomPath = Path.Join(Application.streamingAssetsPath, "Tetris.gb");
    private readonly string bootRomPath = Path.Join(Application.streamingAssetsPath, "GBBOOT_DMG_ROM.bin");
    private string savePath;
    private byte[] gameRom;
    
    private GBScreen screen;
    private GBClock clock;
    private IGBScreenRenderer renderer;
    private GBMemory memory;

    //the timings are from docs in DOTS, and 4 DOTS happen per MCycle
    private int defaultMode0Cycles = 204 / 4;
    private int defaultMode1Cycles = 4560 / 4;
    private int defaultMode2Cycles = 80 / 4;
    private int defaultMode3Cycles = 172 / 4;
    private int lineCycles = 456 / 4;
    private int frameCycles = 70224 / 4;
    private int visibleLines = 144;
    private int totalLines = 154;

    private ushort LcdControlAddress = 0xFF40;
    private ushort LcdStatus = 0xFF41;
    private ushort LineAddress = 0xFF44;
    private ushort LineCompareAddress = 0xFF45;
    private ushort InterruptFlagsAddress = 0xFF0F;


    private void SetUp()
    {
        savePath = Path.Join("%TEMP%", "test.usav");
        if (File.Exists(savePath))
        {
            File.Delete(savePath);
        }
        gameRom = ReadROM(testRomPath);
        clock = new GBClock();
        memory = new GBMemory(savePath, null, null, clock, new MockGBCartEvents());
        renderer = new TestingScreen();
        screen = new GBScreen(memory, clock, renderer);
        memory.Write(LcdControlAddress, 0b10000000);
    }

    private byte[] ReadROM(string romPath)
    {
        using (FileStream fs = new FileStream(romPath, FileMode.Open))
        {
            byte[] gbRom = new byte[fs.Length];
            fs.Read(gbRom, 0, (int)fs.Length);
            return gbRom;
        }
    }

    private PPUMode GetPPUMode()
    {
        return (PPUMode)(memory.Read(LcdStatus) & 0b11);
    }

    [Test]
    public void PpuModeDurationNoQuirks()
    {
        SetUp();
        Assert.AreEqual(PPUMode.ScanlineOAM_2, GetPPUMode(), "PPU should start in mode 2");
        clock.ClockMCycles(defaultMode2Cycles);
        Assert.AreEqual(PPUMode.ScanlineVRAM_3, GetPPUMode(), $"PPU should be mode 3 after {defaultMode2Cycles} mcycles");
        clock.ClockMCycles(defaultMode3Cycles);
        Assert.AreEqual(PPUMode.H_Blank_0, GetPPUMode(), $"PPU should be mode 0 after {defaultMode3Cycles} mcycles");
        clock.ClockMCycles(defaultMode0Cycles);
        Assert.AreEqual(PPUMode.ScanlineOAM_2, GetPPUMode(), $"PPU should be mode 2 again after {defaultMode0Cycles} mcycles");
        clock.ClockMCycles(lineCycles * (visibleLines - 1));
        Assert.AreEqual(PPUMode.V_Blank_1, GetPPUMode(), $"PPU should be mode 1 after {visibleLines} lines");
        clock.ClockMCycles(lineCycles * 10);
        Assert.AreEqual(PPUMode.ScanlineOAM_2, GetPPUMode(), $"PPU should be mode 2 again after 10 vblank lines");
    }
    
    [Test]
    public void PpuLyProgressionRough()
    {
        SetUp();
        Assert.AreEqual(0, memory.Read(LineAddress));
        for (int i = 0; i < totalLines-1; i++)
        {
            clock.ClockMCycles(lineCycles);
            Assert.AreEqual(i+1, memory.Read(LineAddress), $"Line count should be {i+1}");
        }
        clock.ClockMCycles(lineCycles);
        Assert.AreEqual(0, memory.Read(LineAddress), "Line count should be back at 0");
    }
    
    [Test]
    public void PpuLyProgressionClockAccurate()
    {
        SetUp();
        for (int i = 0; i < totalLines; i++)
        {
            Assert.AreEqual(i, memory.Read(LineAddress), $"Line count should be {i}");
            for (int c = 0; c < lineCycles; c++)
            {
                Assert.AreEqual(i, memory.Read(LineAddress), $"Line count should be {i} at Clock {c}");
                clock.ClockMCycle();
            }
        }
        Assert.AreEqual(0, memory.Read(LineAddress), $"Line count should be 0");
    }
    
    [Test]
    public void PpuLyCompareStatus()
    {
        SetUp();
        Assert.AreEqual(0, memory.Read(LineAddress));
        Assert.AreEqual(0, memory.Read(LineCompareAddress));
        for (byte lyc = 0; lyc < totalLines; lyc++)
        {
            memory.Write(LineCompareAddress, lyc);
            bool compareBitHigh = (memory.Read(LcdStatus) & 0b100) != 0;
            Assert.AreEqual(lyc == 0, compareBitHigh, $"Writing to LYC should immediately update Status bit");
            for (byte ly = 0; ly < totalLines; ly++)
            {
                compareBitHigh = (memory.Read(LcdStatus) & 0b100) != 0;
                Assert.AreEqual(lyc == ly, compareBitHigh, $"Compare bit failed LY:{ly} LYC: {lyc}");
                clock.ClockMCycles(lineCycles);
            }
        }
    }
    
    [Test]
    public void StatInterruptLYC()
    {
        SetUp();
        memory.Write(LcdStatus, 0b01000000);
        memory.Write(InterruptFlagsAddress, 0x00);
        memory.Write(LineCompareAddress, 0x01);
        clock.ClockMCycles(lineCycles-1);
        //Primed for Compare
        bool interruptHigh = (memory.Read(InterruptFlagsAddress) & 0b10) != 0;
        Assert.IsFalse(interruptHigh, $"interrupt fired too early");
        clock.ClockMCycle();
        interruptHigh = (memory.Read(InterruptFlagsAddress) & 0b10) != 0;
        Assert.IsTrue(interruptHigh, $"interrupt did not fire");
        //Check refires
        memory.Write(InterruptFlagsAddress, 0x00);
        clock.ClockMCycles(lineCycles-1);
        interruptHigh = (memory.Read(InterruptFlagsAddress) & 0b10) != 0;
        Assert.IsFalse(interruptHigh, $"interrupt should not re-fire in same line");
        
        SetUp();
        memory.Write(LcdStatus, 0b01000000);
        for (byte lyc = 1; lyc < totalLines; lyc++)
        {
            memory.Write(LineCompareAddress, lyc);
            for (byte ly = 0; ly < totalLines; ly++)
            {
                clock.ClockMCycles(lineCycles-1);
                //Only trigger on rising edge of LY == LYC
                memory.Write(InterruptFlagsAddress, 0x00);
                //clock to go to new line
                clock.ClockMCycle();
                interruptHigh = (memory.Read(InterruptFlagsAddress) & 0b10) != 0;
                Assert.AreEqual(lyc == ly+1, interruptHigh, $"interrupt failed LY:{ly+1} LYC: {lyc}");
            }
        }
    }
    
    [Test]
    public void StatInterruptMode0()
    {
        SetUp();
        memory.Write(LcdStatus, 0b00001000);
        memory.Write(InterruptFlagsAddress, 0x00);
        
        clock.ClockMCycles(defaultMode2Cycles);
        clock.ClockMCycles(defaultMode3Cycles-1);
        
        //Should now be primed for Mode 0 to start
        bool interruptHigh = GetStatInterrupt();
        Assert.IsFalse(interruptHigh, $"interrupt should not fire yet");
        //next clock will trigger mode 1
        clock.ClockMCycle();
        interruptHigh = GetStatInterrupt();
        Assert.IsTrue(interruptHigh, $"interrupt failed");
        memory.Write(InterruptFlagsAddress, 0x00);
        
        //Disabling the flag for 1 clock and then turning it on, should trigger again
        memory.Write(LcdStatus, 0b00000000);
        clock.ClockMCycle();
        interruptHigh = GetStatInterrupt();
        Assert.IsFalse(interruptHigh, $"interrupt should not fire yet");
        
        memory.Write(LcdStatus, 0b00001000);
        clock.ClockMCycle();
        interruptHigh = GetStatInterrupt();
        Assert.IsTrue(interruptHigh, $"interrupt failed");
    }
    
    [Test]
    public void StatInterruptMode1()
    {
        SetUp();
        memory.Write(LcdStatus, 0b00010000);
        memory.Write(InterruptFlagsAddress, 0x00);
        for (int i = 0; i < visibleLines-1; i++)
        {
            clock.ClockMCycles(lineCycles);
        }
        clock.ClockMCycles(lineCycles-1);
        //Should now be primed for Mode 1 to start
        bool interruptHigh = GetStatInterrupt();
        Assert.IsFalse(interruptHigh, $"interrupt should not fire yet");
        //next clock will trigger mode 1
        clock.ClockMCycle();
        interruptHigh = GetStatInterrupt();
        Assert.IsTrue(interruptHigh, $"interrupt failed");
        memory.Write(InterruptFlagsAddress, 0x00);
        
        //Disabling the flag for 1 clock and then turning it on, should trigger again
        memory.Write(LcdStatus, 0b00000000);
        clock.ClockMCycle();
        interruptHigh = GetStatInterrupt();
        Assert.IsFalse(interruptHigh, $"interrupt should not fire yet");
        
        memory.Write(LcdStatus, 0b00010000);
        clock.ClockMCycle();
        interruptHigh = GetStatInterrupt();
        Assert.IsTrue(interruptHigh, $"interrupt failed");
    }
    
    [Test]
    public void StatInterruptMode2()
    {
        SetUp();
        memory.Write(LcdStatus, 0b00100000);
        memory.Write(InterruptFlagsAddress, 0x00);
        //First clock will trigger "mode 2 flag on" and cause interrupt once
        clock.ClockMCycle();
        bool interruptHigh = GetStatInterrupt();
        Assert.IsTrue(interruptHigh, $"interrupt failed");
        memory.Write(InterruptFlagsAddress, 0x00);
        
        clock.ClockMCycles(defaultMode2Cycles-1);
        interruptHigh = GetStatInterrupt();
        Assert.IsFalse(interruptHigh, $"interrupt should not re-fire during active mode 2");
        
        clock.ClockMCycles(defaultMode3Cycles);
        clock.ClockMCycles(defaultMode0Cycles-1);
        interruptHigh = GetStatInterrupt();
        Assert.IsFalse(interruptHigh, $"interrupt should not fire yet");
        
        //next clock will trigger mode 2
        clock.ClockMCycle();
        interruptHigh = GetStatInterrupt();
        Assert.IsTrue(interruptHigh, $"interrupt failed");
        memory.Write(InterruptFlagsAddress, 0x00);
        
        //Disabling the flag for 1 clock and then turning it on, should trigger again
        memory.Write(LcdStatus, 0b00000000);
        clock.ClockMCycle();
        interruptHigh = GetStatInterrupt();
        Assert.IsFalse(interruptHigh, $"interrupt should not fire yet");
        
        memory.Write(LcdStatus, 0b00100000);
        clock.ClockMCycle();
        interruptHigh = GetStatInterrupt();
        Assert.IsTrue(interruptHigh, $"interrupt failed");
    }

    private bool GetStatInterrupt()
    {
        return (memory.Read(InterruptFlagsAddress) & 0b10) != 0;
    }
    
    [Test]
    public void StatInterruptBlocking()
    {
        //If the interrupt line was high, no new interrupt can be requested due to no rising edge
        SetUp();
        memory.Write(LcdStatus, 0b00111000);
        //Skip the first "high" signal
        clock.ClockMCycle();
        memory.Write(InterruptFlagsAddress, 0x00);
        
        clock.ClockMCycles(defaultMode2Cycles-1);
        clock.ClockMCycles(defaultMode3Cycles-1);
        //Last cycle in mode 3 into mode 0 next frame
        bool interruptHigh = GetStatInterrupt();
        Assert.IsFalse(interruptHigh, $"interrupt should not fire yet");
        
        clock.ClockMCycle();
        interruptHigh = GetStatInterrupt();
        Assert.IsTrue(interruptHigh, $"interrupt should fire");
        memory.Write(InterruptFlagsAddress, 0x00);
        
        //nothing should fire for a full new loop
        clock.ClockMCycles(defaultMode0Cycles);
        clock.ClockMCycles(defaultMode2Cycles);
        clock.ClockMCycles(defaultMode3Cycles-1);
        //Last cycle in mode 3 into mode 0 next frame
        interruptHigh = GetStatInterrupt();
        Assert.IsFalse(interruptHigh, $"interrupt should not fire yet");
        
        clock.ClockMCycle();
        interruptHigh = GetStatInterrupt();
        Assert.IsTrue(interruptHigh, $"interrupt should fire");
    }
}
