﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

public class GBScreen
{
    private const int totalTiles = 384;//total tiles allowed (2 banks)
    private const int screenWidth = 160;
    private const int screenHeight = 144;
    private const byte OAMSpriteCount = 40;
    private const int OAMSpriteMemorySize = 4 * OAMSpriteCount;
    private const int OAMMemoryOffset = 0xFE00;
    private const int TileDataMemoryOffset = 0x8000;
    
    public event Action FrameFinished = delegate { };

    private byte[] bgPalette = new byte[4]; //colors background screen
    private byte[] obPalette0 = new byte[4]; //colors main sprites layer
    private byte[] obPalette1 = new byte[4]; //colors sprites and effects

    private byte[] screenBuffer = new byte[screenWidth * screenHeight];

    private int windowLYC;
    private byte scrollX, scrollY, windowX, windowY;
    private readonly byte[][] nativeTiles = new byte[totalTiles][];
    private readonly SpriteAttribute[] spriteAttributes = new SpriteAttribute[40];
    private readonly SpriteAttribute[] spriteRowWorkBuffer = new SpriteAttribute[10];
    private readonly HashSet<int> tilesToUpdate = new ();

    private readonly IGBMemoryPPU memory;
    private readonly IGBClock clock;
    private readonly IGBScreenRenderer screenRenderer;
    private LCDControl lcdControl => memory.LcdControl;
    private bool lcdPowered;
    private PPUMode ppuMode;
    private int DotsCounter;
    private bool lastStatInterruptLine = false;
    private readonly ushort lcdStatusRegister = 0xFF41;

    public GBScreen(IGBMemoryPPU memory, IGBClock clock, IGBScreenRenderer screenRenderer)
    {
        this.memory = memory;
        this.clock = clock;
        this.clock.MCycle += OnClock;
        this.screenRenderer = screenRenderer;
        this.screenRenderer.OnInitialize(screenWidth, screenHeight);
        
        MarkAllTilesForUpdate();
        memory.TileMemoryChanged += UpdateTileMemory;
        //Initialize status register
        memory.WriteAsRegister(lcdStatusRegister, 0b10000110);
        ppuMode = PPUMode.ScanlineOAM_2;
    }

    ~GBScreen()
    {
        clock.MCycle -= OnClock;
    }

    private void OnClock()
    {
        for (int i = 0; i < 4; i++)
        {
            DotsCounter++;
            switch (ppuMode)
            {
                case PPUMode.H_Blank_0: //Mode 0
                    if (DotsCounter >= 204) //finished a line
                    {
                        DotsCounter -= 204;
                        byte LCD_Line = memory.Read(0xFF44);
                        LCD_Line++;
                        memory.WriteAsRegister(0xFF44, LCD_Line);
                        if (LCD_Line == 144)
                        {
                            //vblank time
                            FrameFinished.Invoke();
                            ppuMode = PPUMode.V_Blank_1;
                            UpdateOnModeChange();
                            CreateInterrupt(Interrupt.V_Blank);
                        }
                        else
                        {
                            //just do another line
                            ppuMode = PPUMode.ScanlineOAM_2;
                            UpdateOnModeChange();
                        }
                    }
                    break;
                case PPUMode.V_Blank_1: //Mode 1
                    if (DotsCounter >= 456) //takes 10 lines of length from all the other ones combined
                    {
                        DotsCounter -= 456;
                        byte LCD_Line = memory.Read(0xFF44);
                        LCD_Line++;
                        if (LCD_Line == 154)
                        {
                            //new screen start rendering at line 0 again
                            LCD_Line = 0;
                            ppuMode = PPUMode.ScanlineOAM_2;
                            UpdateOnModeChange();
                        }
                        memory.WriteAsRegister(0xFF44, LCD_Line);
                    }
                    break;
                case PPUMode.ScanlineOAM_2: //Mode 2
                    if (DotsCounter >= 80)
                    {
                        DotsCounter -= 80;
                        ppuMode = PPUMode.ScanlineVRAM_3;
                        UpdateOnModeChange();
                    }
                    break;
                case PPUMode.ScanlineVRAM_3: //Mode 3
                    if (DotsCounter >= 172)
                    {
                        DotsCounter -= 172;
                        ppuMode = PPUMode.H_Blank_0;
                        UpdateOnModeChange();
                    }
                    break;
            }
        }

        //Update LCD Status register
        LCDStatus stat = UpdateStatRegister();

        //LCD STAT interrupts
        bool statInterruptLine = false;
        if (stat.hBlankInterrupt && stat.mode == PPUMode.H_Blank_0)
            statInterruptLine = true;
        if (stat.vBlankInterrupt && stat.mode == PPUMode.V_Blank_1)
            statInterruptLine = true;
        if (stat.OAMInterrupt && stat.mode == PPUMode.ScanlineOAM_2)
            statInterruptLine = true;
        if (stat.coincidenceLYInterrupt && stat.coincidenceLY)
            statInterruptLine = true;
        //Check stat interrupt line
        bool risingEdgeInterruptLine = !lastStatInterruptLine && statInterruptLine;
        if (risingEdgeInterruptLine)
        {
            CreateInterrupt(Interrupt.STAT_LCDC);
        }
        lastStatInterruptLine = statInterruptLine;
    }

    private LCDStatus UpdateStatRegister()
    {
        byte LYval = memory.Read(0xFF44);
        byte LYCval = memory.Read(0xFF45);
        bool coincidence = LYval == LYCval;
        
        byte statRegister = (byte)(memory.Read(lcdStatusRegister) & 0b11111000);
        statRegister |= (byte)ppuMode;
        if (coincidence)
        {
            statRegister |= 1 << 2;
        }
        memory.WriteAsRegister(lcdStatusRegister, statRegister);
        return new LCDStatus(statRegister);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void CreateInterrupt(Interrupt inter)
    {
        byte b = memory.Read(0xFF0F);
        b |= (byte)inter;
        memory.Write(0xFF0F, b);
    }

    private void MarkAllTilesForUpdate()
    {
        for (int i = 0; i < totalTiles; i++)
        {
            tilesToUpdate.Add(i);
        }
    }

    private void UpdateOnModeChange()
    {
        UpdateRegisters();
        //UnityEngine.Profiling.Profiler.BeginSample("Screen Update");
        switch (ppuMode)
        {
            case PPUMode.H_Blank_0://horizontal blank, if in debug could apply texture to screen
                //Update all tiles that need it
                UpdateTiles();
                UpdateScreenRow(memory.Read(0xFF44));
                break;
            case PPUMode.V_Blank_1: //vertical blank, Actually apply texture to screen
                screenRenderer.OnFrameFinished(ref screenBuffer);
                break;
            case PPUMode.ScanlineOAM_2:
                //access to OAM(Sprite memory) for scanline, Do sprite related reading now.
                UpdateOAMData();
                break;
            case PPUMode.ScanlineVRAM_3:
                //access to VRAM for scanline, Do VRAM Reading
                break;
        }
        //UnityEngine.Profiling.Profiler.EndSample();
    }

    private void UpdateRegisters()
    {
        WipeLCDWhenOff();
        UpdateScrollRegisters();
        UpdateColorPalettes();
    }

    private void WipeLCDWhenOff()
    {
        if (!lcdControl.LCDPower && lcdPowered)
        {
            for (int i = 0; i < screenBuffer.Length; i++)
            {
                screenBuffer[i] = 0; //All blank!
            }
            screenRenderer.OnFrameFinished(ref screenBuffer);
        }
        lcdPowered = memory.LcdControl.LCDPower;
    }

    private void UpdateTileMemory(ushort pixelAddress)
    {
        int tile = PixelAddressToTile(pixelAddress);
        tilesToUpdate.Add(tile);
    }
    
    private void UpdateColorPalettes()
    {
        byte BGColor = memory.Read(0xFF47);
        ByteToPalette(BGColor, ref bgPalette);
        byte OBP0 = memory.Read(0xFF48);
        ByteToPalette(OBP0, ref obPalette0);
        byte OBP1 = memory.Read(0xFF49);
        ByteToPalette(OBP1, ref obPalette1);
    }

    private void ByteToPalette(byte color, ref byte[] palette)
    {
        palette[0] = (byte)(color & 0x03);
        palette[1] = (byte)((color >> 2) & 0x03);
        palette[2] = (byte)((color >> 4) & 0x03);
        palette[3] = (byte)((color >> 6) & 0x03);
    }

    private int PixelAddressToTile(ushort pixelAddress)
    {
        return (pixelAddress - TileDataMemoryOffset) / 16;
    }
    
    private void UpdateScrollRegisters()
    {
        scrollX = memory.Read(0xFF43);
        scrollY = memory.Read(0xFF42);
        windowX = memory.Read(0xFF4B);
        windowY = memory.Read(0xFF4A);
    }

    private void UpdateScreenRow(byte pixelRow)
    {
        //no power, don't update the texture at all. Better yet the screen is cleared.
        if (!lcdControl.LCDPower)
            return;
        int rowOffset = pixelRow * screenWidth;
        //do BG work
        if (lcdControl.BackgroundAndWindowOn)
        {
            GetBGScreenPixel(pixelRow, rowOffset);//7.43ms
        }
        //Do window work
        if (pixelRow == 0)
            windowLYC = 0;
        if (lcdControl.WindowOn)
        {
            GetWindowScreenPixel(pixelRow, rowOffset);//2ms
        }
        //do the spritework
        if (lcdControl.ObjectsOn)
        {
            GetSpriteScreenRow(pixelRow, rowOffset);//0.81ms
        }
    }

    private void UpdateOAMData()
    {
        var OAMMemory = memory.GetMemorySegment(OAMMemoryOffset, OAMSpriteMemorySize);
        for (byte i = 0; i < OAMSpriteCount; i++)
        {
            int index = i * 4;
            var spriteData = OAMMemory.Slice(index, 4);
            spriteAttributes[i] = new SpriteAttribute(i, spriteData);
        }
        //sort sprites in a way where X prioritizes the sprite and use a tiebreaker OAMIndex
        Array.Sort(spriteAttributes, delegate (SpriteAttribute x, SpriteAttribute y)
        {
            int change = x.xPos.CompareTo(y.xPos);
            if (change == 0 && x.xPos == y.xPos)
            {
                change = x.OAMIndex.CompareTo(y.OAMIndex);
            }
            return change;
        });
    }

    //BG layer
    private void GetBGScreenPixel(int SCrow, int rowOffset)
    {
        //Set Y pixel location with scroll and wrap
        SCrow += scrollY;
        SCrow = (byte)SCrow;//rounding stuff
        //y tile flattened for array access
        int tileMapRowFlat = (SCrow / 8)*32;
        //y pixel flattened for array access
        int pixelRowFlat = (SCrow & 7)*8;

        //Loop over all columns
        for (int x = 0; x < screenWidth; x++)
        {
            int SCcol = x;
            //set X pixel location with scroll and wrap
            SCcol += scrollX;
            SCcol = (byte)SCcol;//rounding stuff

            //Get BG tile
            int tileMapCol = SCcol / 8;
            int tileID;
            //high or low tile calculation?
            if (lcdControl.BackgroundWindowTileDataMode)
            {
                //linear 0-255 on 0x9800
                tileID = memory.ReadPPU((ushort)(lcdControl.BackgroundTileMapAddress + (tileMapRowFlat + tileMapCol)));
            }
            else
            {
                //-128-127 on 0x9C00 + 255 tile offset
                tileID = (sbyte)memory.ReadPPU((ushort)(lcdControl.BackgroundTileMapAddress + (tileMapRowFlat + tileMapCol)));
                tileID += 256;
            }
            //Get BG tile pixel
            int pixelInTile = pixelRowFlat + (SCcol & 7);
            
            //write to screenbuffer
            byte color = nativeTiles[tileID][pixelInTile];
            screenBuffer[rowOffset + x] = bgPalette[color];
        }
    }

    //Window layer
    private void GetWindowScreenPixel(int SCrow, int rowOffset)
    {
        //return if we are not interested in this row.
        if (SCrow < windowY)
            return;
        //Set real Y pixel to read, based on current screen location
        SCrow = (byte)(windowLYC);
        //y tile flattened for array access
        int tileMapRowFlat = (SCrow / 8) * 32;
        //y pixel flattened for array access
        int pixelRowFlat = (SCrow % 8) * 8;
        bool didDrawThisRow = false;

        //Loop over all columns
        for (int x = 0; x < screenWidth; x++)
        {
            int SCcol = x+7;
            //skip if we are not interested in this column
            if (SCcol < windowX)
                continue;

            didDrawThisRow = true;
            //Set real X pixel to read, based on current screen location
            SCcol = (byte)(SCcol - windowX);

            //Get BG tile
            int tileMapCol = SCcol / 8;
            int tileID;
            //high or low tile calculation?
            if (lcdControl.BackgroundWindowTileDataMode)
            {
                //linear 0-255 on 0x9800
                tileID = memory.ReadPPU((ushort)(lcdControl.WindowTileMapAddress + (tileMapRowFlat + tileMapCol)));
            }
            else
            {
                //-128-127 on 0x9C00 + 255 tile offset
                tileID = (sbyte)memory.ReadPPU((ushort)(lcdControl.WindowTileMapAddress + (tileMapRowFlat + tileMapCol)));
                tileID += 256;
            }
            //Get BG tile pixel
            int pixelInTile = pixelRowFlat + (SCcol % 8);
            //write to screenbuffer
            byte color = nativeTiles[tileID][pixelInTile];
            screenBuffer[rowOffset + x] = bgPalette[color];
        }
        //did draw this line
        if (didDrawThisRow)
            windowLYC++;
    }

    //Sprite layer
    private void GetSpriteScreenRow(int row, int rowOffset)
    {
        //Y == up and down
        int height = lcdControl.ObjectHeight;
        
        int spritesIndex = 0;
        //figure out what 10 sprites we are allowed to draw this row
        for (int i = 0; i < OAMSpriteCount; i++)
        {
            if (spritesIndex == 10)
                break;
            var currentSprite = spriteAttributes[i];
            //get sprite row
            int spriteY = GetSpriteY(row, currentSprite);
            if (spriteY >= 0 && spriteY < height)
            {
                //sprite is inside Y range
                for (int col = 0; col < screenWidth; col++)
                {
                    //Get sprite column
                    int spriteX = GetSpriteX(currentSprite, col);
                    if (spriteX >= 0 && spriteX < 8)
                    {
                        //Sprite is going to be drawn, add to render list
                        spriteRowWorkBuffer[spritesIndex] = currentSprite;
                        spritesIndex++;
                        break;
                    }
                }
            }
        }
        //actually draw the sprites
        for (int s = spritesIndex; s-- > 0;)
        {
            var currentSprite = spriteRowWorkBuffer[s];

            //get sprite row
            int spriteY = GetSpriteY(row, currentSprite);
            if (spriteY >= 0 && spriteY < height)
            {
                if (currentSprite.verticalFlip)
                    spriteY = (height - 1) - spriteY;
                //for every column
                for (int col = 0; col < screenWidth; col++)
                {
                    //Get sprite column
                    int spriteX = GetSpriteX(currentSprite, col);
                    if (spriteX >= 0 && spriteX < 8)
                    {
                        if (currentSprite.horizontalFlip)
                            spriteX = 7 - spriteX;
                        //Get sprite tile pixel
                        int pixelInTile = (spriteY * 8) + (spriteX);
                        byte c;
                        if (lcdControl.ObjectHeight == 8)
                        {
                            c = nativeTiles[currentSprite.tileNumber][pixelInTile];
                        }
                        else
                        {
                            if (pixelInTile < 64)
                            {
                                c = nativeTiles[(currentSprite.tileNumber & 0xFE)][pixelInTile];
                            }
                            else
                            {
                                c = nativeTiles[(currentSprite.tileNumber | 0x01)][pixelInTile - 64];
                            }
                        }

                        //transparency correction
                        if (c == 0)
                            continue;
                        if (currentSprite.paletteOBP1)
                        {
                            c = obPalette1[c];
                        }
                        else
                        {
                            c = obPalette0[c];
                        }
                        if (currentSprite.priority || screenBuffer[rowOffset + col] == 0)//priority or BG color 0? Render Transparency.
                        {
                            screenBuffer[rowOffset + col] = c;
                        }
                    }
                }
            }
        }
    }

    private static int GetSpriteX(SpriteAttribute currentSprite, int col)
    {
        int spriteX;
        if (currentSprite.xPos < 0)
        {
            spriteX = Math.Abs(col - currentSprite.xPos);
        }
        else
        {
            spriteX = col - currentSprite.xPos;
        }

        return spriteX;
    }

    private static int GetSpriteY(int row, SpriteAttribute currentSprite)
    {
        int spriteY;
        if (currentSprite.yPos < 0)
        {
            spriteY = Math.Abs(row - currentSprite.yPos);
        }
        else
        {
            spriteY = row - currentSprite.yPos;
        }

        return spriteY;
    }

    private void UpdateTiles()
    {
        foreach (int tileId in tilesToUpdate)
        {
            if (nativeTiles[tileId] == null)
            {
                nativeTiles[tileId] = new byte[8 * 8];
            }
            UpdateTileWithVRAM(tileId, ref nativeTiles[tileId]);
        }
        tilesToUpdate.Clear();
    }

    private void UpdateTileWithVRAM(int tileID, ref byte[] tileColorData)
    {
        //Hardcoded for reading all tile id's 0-383
        var tileData = memory.GetMemorySegment((ushort)(TileDataMemoryOffset + (tileID * 16)), 16);
        ConvertMemoryToTileColorData(tileData, ref tileColorData);
    }

    private void ConvertMemoryToTileColorData(ReadOnlySpan<byte> tileData, ref byte[] tileColorData)
    {
        for (int row = 0; row < 8; row++)
        {
            int rowtimes = row * 2;
            int rowBit = row * 8;
            for (int bit = 0; bit < 8; bit++)
            {
                tileColorData[rowBit + bit] =
                    ConvertBitsToNativeColor(bit, tileData[rowtimes], tileData[rowtimes + 1]);
            }
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private byte ConvertBitsToNativeColor(int bit, byte a, byte b)
    {
        bit = 7 - bit;//Invert bits?
        int result = (a & (1 << bit)) != 0 ? 1 : 0;//lsb
        result += (b & (1 << bit)) != 0 ? 2 : 0;//msb
        return (byte)result;
    }

}
