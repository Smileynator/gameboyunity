public struct LCDControl
{
    public bool LCDPower;
    public bool BackgroundAndWindowOn;
    public bool WindowOn;
    public bool ObjectsOn;
    public byte ObjectHeight;
    
    public ushort BackgroundTileMapAddress;
    public bool BackgroundTileMapHigh;

    public bool BackgroundWindowTileDataMode;

    public ushort WindowTileMapAddress;
    public bool WindowTileMapHigh;

    public LCDControl(byte data)
    {
        BackgroundAndWindowOn = Util.BitCheck(0, data);
        ObjectsOn = Util.BitCheck(1, data);
        ObjectHeight = (byte) (Util.BitCheck(2, data) ? 16 : 8);
        BackgroundTileMapHigh = Util.BitCheck(3, data);
        BackgroundTileMapAddress = (ushort)(BackgroundTileMapHigh ? 0x9C00 : 0x9800);
        BackgroundWindowTileDataMode = Util.BitCheck(4, data);
        WindowOn = Util.BitCheck(5, data);
        WindowTileMapHigh = Util.BitCheck(6, data);
        WindowTileMapAddress = (ushort)(WindowTileMapHigh ? 0x9C00 : 0x9800);
        LCDPower = Util.BitCheck(7, data);
    }
}