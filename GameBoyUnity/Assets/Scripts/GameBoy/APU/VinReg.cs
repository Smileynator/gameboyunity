﻿public class VinReg
{
    public float so1_volume, so2_volume;
    public bool out_Vin_so1, out_Vin_so2;

    public void UpdateRegister(ushort address, byte data)
    {
        switch (address)
        {
            case 0xFF24:
                {
                    out_Vin_so1 = Util.BitCheck(3, data);
                    out_Vin_so2 = Util.BitCheck(7, data);
                    //volume 0 to 7 int
                    int volumeSO1 = data & 0x07;
                    so1_volume = volumeSO1/7f;
                    int volumeSO2 = (data >> 4) & 0x07;
                    so2_volume = volumeSO2 / 7f;
                    break;
                }
        }
    }
}
