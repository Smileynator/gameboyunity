﻿using Random = System.Random;

public class GBAudioFilterNoise : GBAudioFilterBase
{
    private AudioCh4 channelData;
    
    public GBAudioFilterNoise(GBAudio audio, AudioChannel channel, int sampleRate, IGBAudioOutput output) : base(audio, channel, sampleRate, output)
    {
    }

    public void SetChannelData(AudioCh4 channel)
    {
        channelData = channel;
    }

    protected override void CreateAudioBufferSegment(float[] data, int channels)
    {
        channelData.CreateAudioBufferSegment(data, channels);
    }
}
