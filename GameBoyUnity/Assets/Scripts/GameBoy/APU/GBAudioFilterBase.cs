﻿using System;

public class GBAudioFilterBase
{
    protected GBAudio audio;
    protected double phase;
    protected float sampleRate;//44100hz usually
    private readonly float ticksPerSample;
    private float tickCounter;
    protected static double doublePI = Math.PI * 2;
    //wave dropoff
    protected float wavePeak = 0f;
    protected float peakDrop = 0f;
    protected const float dropoff = 0.998f;
    //audio buffer
    private readonly float[] copyBuffer = new float[2];
    private readonly GBAudioBuffer buffer = new GBAudioBuffer()
    {
        sampleBuffer = new float[22050]
    };

    public GBAudioFilterBase(GBAudio audio, AudioChannel channel, int sampleRate, IGBAudioOutput output)
    {
        this.audio = audio;
        this.sampleRate = sampleRate;
        ticksPerSample = ((float)4194304 / sampleRate);
        ticksPerSample -= 0.000001f;//slight overspeed may help consistency?
        
        output.OnBufferInitialize(audio.settings, channel, ref buffer);
    }

    /// <summary>
    /// Called once for every tick
    /// </summary>
    internal void ProcessTick()
    {
        //This code fills the buffer based on real processor ticks so that effects like the "pikachu noise" works properly
        //Otherwise we just try to balance the buffer we create with the unity audio buffer, which is a pain with lag spikes.
        tickCounter++;
        if(tickCounter >= ticksPerSample)
        {
            CreateAudioBufferSegment(copyBuffer, 2);
            lock (buffer)
            {
                //failsafe to keep overwriting the last bytes, we need a ringbuffer!
                if (buffer.bufferPointer + 2 > buffer.sampleBuffer.Length)
                {
                    buffer.bufferPointer -= 2;
                }
                Array.Copy(copyBuffer, 0, buffer.sampleBuffer, buffer.bufferPointer, 2);
                buffer.bufferPointer += 2;
            }
            tickCounter -= ticksPerSample;
        }
    }

    internal virtual void TriggerEvent()
    {
    }

    internal virtual void FrequencyChanged()
    {
    }

    internal virtual void TriggerFreqSweepClock()
    {
    }

    internal virtual void TriggerVolEnvClock()
    {
    }

    internal virtual void ResetDutyStep()
    {
        phase = 0;
    }

    protected virtual void CreateAudioBufferSegment(float[] data, int channels)
    {

    }

    /// <summary>
    /// Will pull the audio waveform towards 0 gradually as post processing effect.
    /// </summary>
    protected internal float AudioPostProcessingFilter(float data)
    {
        float result = data;
        //pull to 0
        if (data != wavePeak)
        {
            peakDrop = wavePeak = data;

        }
        else
        {
            peakDrop *= dropoff;
            result = peakDrop;
        }
        return result;
    }
}
