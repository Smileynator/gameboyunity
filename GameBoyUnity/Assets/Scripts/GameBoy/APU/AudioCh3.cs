﻿
public class AudioCh3 : GBAudioChannel
{
    public byte outputLevel;
    public ushort frequencyData;
    public float[] waveData = new float[32];

    //derived values
    public float outputVolume;
    private readonly GBAudioFilterWave waveFilter;

    public AudioCh3(GBAudio audio, AudioChannel channel, int sampleRate, IGBAudioOutput audioOutput)
        : base(audio, channel, sampleRate, audioOutput)
    {
        maxSoundLength = 256;
        waveFilter = new GBAudioFilterWave(audio, channel, sampleRate, audioOutput);
        waveFilter.SetChannelData(this);
        filter = waveFilter;
    }

    internal override void OnRegisterChanged(ushort address, byte data)
    {
        switch (address)
        {
            case 0xFF1A:
                {
                    //DAC control with bit 7
                    DacIsEnabled = (data & 0x80) != 0;
                    break;
                }
            case 0xFF1B:
                {
                    lengthTimer = (ushort)(maxSoundLength - data);
                    break;
                }
            case 0xFF1C:
                {
                    outputLevel = (byte)(data >> 5);
                    outputLevel &= 0x03;
                    //der
                    switch (outputLevel)
                    {
                        case 0:
                            outputVolume = 0f;
                            break;
                        case 1:
                            outputVolume = 1f;
                            break;
                        case 2:
                            outputVolume = 0.5f;
                            break;
                        case 3:
                            outputVolume = 0.25f;
                            break;
                        default:
                            break;
                    }
                    break;
                }
            case 0xFF1D:
                {
                    frequencyData = (ushort) (frequencyData & 0xFF00);
                    frequencyData = (ushort) (frequencyData | data);
                    //der
                    outputFrequency = 65536 / (2048 - frequencyData);
                    FrequencyChanged();
                    break;
                }
            case 0xFF1E:
                {
                    frequencyData = (ushort)(frequencyData & 0xF8FF);
                    int d = (data & 0x07) << 8;
                    frequencyData = (ushort)(frequencyData | d);
                    bool previousLengthEnable = lengthEnable;
                    lengthEnable = Util.BitCheck(6, data);
                    bool trigger = Util.BitCheck(7, data);
                    //der
                    outputFrequency = 65536 / (2048 - frequencyData);
                    FrequencyChanged();
                    //Extra length clock? (lengthEnable and Length > 0 are checked already inside)
                    if (!previousLengthEnable && audio.SequencerStep % 2 == 1)
                        ClockLengthCtr();
                    if (trigger)
                    {
                        DoTrigger();
                    }
                    break;
                }
        }
    }

    internal override void UpdateWaveData(ushort address, byte data)
    {
        address -= 0xFF30;
        address *= 2;
        //upper nibble first
        waveData[address] = ((data >> 4)/15f)*2-1;
        //lower nibble
        waveData[address+1] = ((data & 0x0F)/15f)*2-1;
        //data converted from 0/15, into -1/+1
    }
}