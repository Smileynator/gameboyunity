﻿
public class GBAudio : IGBAudio
{
    public bool AudioPowered => poweredAPU;
    public readonly IGBMemory memory;//TODO get rid of write access?
    private readonly IGBClockRead clock;
    private readonly GBAudioChannel[] channels = new GBAudioChannel[4];
    public readonly GBSettings settings;
    private readonly VinReg vinReg = new VinReg();
    private bool oldDivBit4;
    private bool poweredAPU;
    private readonly bool[][] channelPanning = new bool[4][];
    public int SequencerStep => sequenceStep;
    private int sequenceStep = 0;

    public GBAudio(IGBMemory memory, IGBClockRead clock, IGBAudioOutput[] gbAudioOutputs, GBSettings settings)
    {
        this.memory = memory;
        this.clock = clock;
        this.settings = settings;
        clock.MCycle += GenerateAudioSamples;
        memory.ApuTick += ApuSequencer;
        memory.AudioRegisterChanged += OnRegisterChanged;
        //init jagged array
        for (int i = 0; i < channelPanning.Length; i++)
        {
            channelPanning[i] = new bool[2];
        }
        //init channels
        channels[0] = new AudioCh1(this, AudioChannel.Channel1, settings.audioSampleRate, gbAudioOutputs[0]);
        channels[1] = new AudioCh2(this, AudioChannel.Channel2, settings.audioSampleRate, gbAudioOutputs[1]);
        channels[2] = new AudioCh3(this, AudioChannel.Channel3, settings.audioSampleRate, gbAudioOutputs[2]);
        channels[3] = new AudioCh4(this, AudioChannel.Channel4, settings.audioSampleRate, gbAudioOutputs[3]);
    }

    ~GBAudio()
    {
        clock.MCycle -= GenerateAudioSamples;
        memory.ApuTick -= ApuSequencer;
    }

    private void OnRegisterChanged(ushort address, byte data)
    {
        //we always need to update these to match the registers
        switch (address)
        {
            case 0xFF10:
            case 0xFF11:
            case 0xFF12:
            case 0xFF13:
            case 0xFF14:
                channels[0].OnRegisterChanged(address, data);
                break;
            case 0xFF16:
            case 0xFF17:
            case 0xFF18:
            case 0xFF19:
                channels[1].OnRegisterChanged(address, data);
                break;
            case 0xFF1A:
            case 0xFF1B:
            case 0xFF1C:
            case 0xFF1D:
            case 0xFF1E:
                channels[2].OnRegisterChanged(address, data);
                break;
            case 0xFF20:
            case 0xFF21:
            case 0xFF22:
            case 0xFF23:
                channels[3].OnRegisterChanged(address, data);
                break;
            case 0xFF24:
                vinReg.UpdateRegister(address, data);
                break;
            case 0xFF25:
            {
                channelPanning[0][0] = Util.BitCheck(0, data);
                channelPanning[1][0] = Util.BitCheck(1, data);
                channelPanning[2][0] = Util.BitCheck(2, data);
                channelPanning[3][0] = Util.BitCheck(3, data);
                channelPanning[0][1] = Util.BitCheck(4, data);
                channelPanning[1][1] = Util.BitCheck(5, data);
                channelPanning[2][1] = Util.BitCheck(6, data);
                channelPanning[3][1] = Util.BitCheck(7, data);
                break;
            }
            case 0xFF26:
                bool prevSound = poweredAPU;
                poweredAPU = Util.BitCheck(7, data);
                if (prevSound != poweredAPU)
                    SoundPowerChanged(poweredAPU);
                break;
            case >= 0xFF30 and <= 0xFF3F:
                //channel 3 wave data
                channels[2].UpdateWaveData(address, data);
                break;
        }
    }

    /// <summary>
    /// Enables or disables an audio channel register
    /// </summary>
    public void SetChannelActiveFlag(AudioChannel channel, bool state)
    {
        byte controlRegs = memory.Read(0xFF26);
        byte flag = (byte) (1 << (int)channel);
        //If enabling and dac is active, set, otherwise clear.
        if (state && channels[(int)channel].DacIsEnabled)
        {
            controlRegs |= flag;
        }
        else
        {
            controlRegs &= (byte)~flag;
        }
        memory.WriteAsRegister(0xFF26, controlRegs);
    }

    /// <summary>
    /// Returns if the side is active of any channel due to panning
    /// </summary>
    public bool ChannelSideActive(AudioChannel channel, int side)
    {
        return channelPanning[(int)channel][side];
    }

    private void ApuSequencer()
    {
        //Length
        if(sequenceStep % 2 == 0)
        {
            foreach (var ch in channels)
            {
                ch.ClockLengthCtr();
            }
        }
        //Envelope
        if (sequenceStep == 7)
        {
            foreach (var ch in channels)
            {
                ch.ClockVolEnv();
            }
        }
        //Sweep
        else if (sequenceStep == 2  || sequenceStep == 6)
        {
            foreach (var ch in channels)
            {
                ch.ClockSweep();
            }
        }
        //step
        sequenceStep++;
        if (sequenceStep > 7)
            sequenceStep = 0;
    }

    private void GenerateAudioSamples()
    {
        for (int j = 0; j < 4; j++)
        {
            for (int i = 0; i < channels.Length; i++)
            {
                channels[i].ProcessTick();
            }
        }
    }

    private void SoundPowerChanged(bool soundOn)
    {
        if (!soundOn)
        {
            //all audio regs to 0 except for WAVE
            for (ushort i = 0xFF10; i < 0xFF30; i++)
            {
                memory.WriteAsRegister(i, 0x00); //no checks needed, just all to 0 just like init.
            }

            //reset sequencer
            sequenceStep = 0;
            //reset all the channel related registers
            foreach (var ch in channels)
            {
                ch.ApuTurnedOff();
            }
        }
    }
}
