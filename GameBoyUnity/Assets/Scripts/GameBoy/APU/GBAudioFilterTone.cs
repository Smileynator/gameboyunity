﻿using System;

public class GBAudioFilterTone : GBAudioFilterBase
{
    private AudioCh2 channelData;

    private double sampleIncrease;

    private bool envVolEnabled = false;
    private byte envVolCounter = 0;
    private byte currentEnvVol;
    
    public GBAudioFilterTone(GBAudio audio, AudioChannel channel, int sampleRate, IGBAudioOutput output) : base(audio, channel, sampleRate, output)
    {
        
    }

    public void SetChannelData(AudioCh2 chan)
    {
        channelData = chan;
    }

    internal override void TriggerEvent()
    {
        //Frequency+Envelope timer to period
        phase = 0;
        //volume envelope timer to max envelope sweep time
        envVolCounter = channelData.envelopeSweepTime;
        envVolEnabled = true;
        //Reset volume
        currentEnvVol = channelData.envelopeStartVolume;
    }

    internal override void FrequencyChanged()
    {
        //Set sample increase based on frequency
        sampleIncrease = channelData.outputFrequency * doublePI / sampleRate;
    }

    internal override void TriggerVolEnvClock()
    {
        //envVol active, and sweep not 0?
        if (!envVolEnabled || channelData.envelopeSweepTime == 0)
            return;
        envVolCounter--;
        if (envVolCounter == 0)
        {
            if (channelData.envelopeIncr)
            {
                currentEnvVol++;
                if (currentEnvVol >= 15)
                {
                    currentEnvVol = 15;
                    envVolEnabled = false;
                }
            }
            else
            {
                currentEnvVol--;
                if (currentEnvVol <= 0)
                {
                    currentEnvVol = 0;
                    envVolEnabled = false;
                }
            }
            envVolCounter = channelData.envelopeSweepTime;
        }
    }

    protected override void CreateAudioBufferSegment(float[] data, int channels)
    {
        if (!audio.settings.audioChannelsOn[1] || !audio.AudioPowered || !channelData.DacIsEnabled)//DACs on?
            return;
        //THE BELOW SHOULD TECHNICALLY BE COVERED BY DAC ENABLE? (length = 0 disables DAC)
        //if (channelData.lengthEnable && channelData.lengthTimer == 0)//did audio finish lengthwise?
        //return;

        int totalSamples = data.Length / channels;
        int sample = 0;
        while (sample < totalSamples)
        {
            phase += sampleIncrease;
            int channel = 0;
            while (channel < channels)
            {
                if (!audio.ChannelSideActive(channelData.channel,channel))
                {
                    channel++;
                    continue;
                }
                //random square wave noise.
                int length = channelData.GetWavePattern().Length;
                int ptr = (int) Math.Floor(phase * length / doublePI);//current sample
                if (ptr >= length)
                    ptr = length-1;
                float d = data[sample * channels + channel] = channelData.GetWavePattern()[ptr] * (currentEnvVol / 15f);
                //dropoff
                data[sample * channels + channel] = AudioPostProcessingFilter(d);
                channel++;
            }
            if(phase > doublePI)
            {
                phase -= doublePI;
            }
            sample++;
        }
    }
}