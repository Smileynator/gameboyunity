﻿
public class AudioCh1 : GBAudioChannel
{
    public byte freqSweepTime;
    public bool freqSweepSub;
    public byte freqSweepShift;
    public byte currentWavePattern;
    public byte envelopeSweepTime;
    public bool envelopeIncr;
    public byte envelopeStartVolume;//0-F (0 to 15)
    private ushort registerFrequency;

    public float[][] wavePatterns = new float[][]
    {
        new float[]{ -1,-1,-1,-1,-1,-1,-1,1 },
        new float[]{ 1,-1,-1,-1,-1,-1,-1,1 },
        new float[]{ 1, 1,-1,-1,-1,-1,1,1 },
        new float[]{ -1,1,1,1,1,1,1,-1 }
    };

    public AudioCh1(GBAudio audio, AudioChannel channel, int sampleRate, IGBAudioOutput audioOutput)
        : base(audio, channel, sampleRate, audioOutput)
    {
        var myFilter = new GBAudioFilterToneSweep(audio, channel, sampleRate, audioOutput);
        myFilter.SetChannelData(this);
        filter = myFilter;
    }

    public ushort RegisterFrequency
    {
        get => registerFrequency;

        set
        {
            registerFrequency = value;
            UpdateOutputFrequency();
            //Write back to registery
            audio.memory.WriteAsRegister(0xFF13, (byte)value);//LSB
            byte nr14 = audio.memory.Read(0xFF14);
            nr14 = (byte)(nr14 & 0x07);
            byte freqMSB = (byte)(value >> 8);
            audio.memory.WriteAsRegister(0xFF14, (byte)(nr14 | freqMSB));//MSB
        }
    }

    internal float[] GetWavePattern()
    {
        return wavePatterns[currentWavePattern];
    }

    internal void UpdateOutputFrequency()
    {
        outputFrequency = 131072 / (2048 - registerFrequency);
        FrequencyChanged();
    }

    internal override void OnRegisterChanged(ushort address, byte data)
    {
        switch (address)
        {
            case 0xFF10:
                {
                    freqSweepShift = (byte)(data & 0x07);
                    freqSweepSub = Util.BitCheck(3, data);
                    freqSweepTime = (byte)((data >> 4) & 0x07);
                    break;
                }
            case 0xFF11:
                {
                    lengthTimer = (byte)(64 - (0x3F & data));//0-5bit
                    currentWavePattern = (byte)(0x03 & (data >> 6));//6-7 bit
                    break;
                }
            case 0xFF12:
                {
                    envelopeSweepTime = (byte)(data & 0x07);
                    envelopeIncr = Util.BitCheck(3, data);
                    envelopeStartVolume = (byte)((data >> 4) & 0x0F);
                    //enable/disable DAC
                    DacIsEnabled = (data & 0xF8) != 0;
                    break;
                }
            case 0xFF13:
                {
                    registerFrequency = (ushort)(registerFrequency & 0xFF00);
                    registerFrequency = (ushort)(registerFrequency | data);
                    //der
                    UpdateOutputFrequency();
                    break;
                }
            case 0xFF14:
                {
                    registerFrequency = (ushort)(registerFrequency & 0xF8FF);
                    int d = (data & 0x07) << 8;
                    registerFrequency = (ushort)(registerFrequency | d);
                    bool previousLengthEnable = lengthEnable;
                    lengthEnable = Util.BitCheck(6, data);
                    bool trigger = Util.BitCheck(7, data);
                    //der
                    UpdateOutputFrequency();
                    //Extra length clock? (lengthEnable and Length > 0 are checked already inside)
                    if (!previousLengthEnable && audio.SequencerStep % 2 == 1)
                        ClockLengthCtr();
                    if (trigger)
                    {
                        DoTrigger();
                    }
                    break;
                }
        }
    }
}