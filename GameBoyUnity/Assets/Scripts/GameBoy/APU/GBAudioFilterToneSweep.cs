﻿using System;

public class GBAudioFilterToneSweep : GBAudioFilterBase
{
    private AudioCh1 channelData;
    
    private double sampleIncrease;

    private bool envVolEnabled = false;
    private byte envVolCounter = 0;
    private byte currentEnvVol;

    private bool freqSweepEnabled = false;
    private ushort shadowSweepFreq;
    private byte freqSweepTimer = 0;

    public GBAudioFilterToneSweep(GBAudio audio, AudioChannel channel, int sampleRate, IGBAudioOutput output) : base(audio, channel, sampleRate, output)
    {
        
    }

    public void SetChannelData(AudioCh1 chan)
    {
        channelData = chan;
    }

    internal override void TriggerEvent()
    {
        //Frequency+Envelope timer to period
        phase = 0;
        //Freq sweep setup
        shadowSweepFreq = channelData.RegisterFrequency;
        ReloadSweepTimer();
        freqSweepEnabled = (channelData.freqSweepTime != 0) || (channelData.freqSweepShift != 0);
        if(channelData.freqSweepShift != 0)
        {
            CalculateNewFrequency(false);
        }
        //volume envelope timer to max envelope sweep time
        envVolCounter = channelData.envelopeSweepTime;
        envVolEnabled = (channelData.envelopeSweepTime != 0);
        //Reset volume
        currentEnvVol = channelData.envelopeStartVolume;
    }

    internal override void FrequencyChanged()
    {
        //Set sample increase based on frequency
        sampleIncrease = channelData.outputFrequency * doublePI / sampleRate;
    }

    internal override void TriggerFreqSweepClock()
    {
        //Special case for sweep, if this is true, we disable the channel by force.
        //if(didCalculateSinceTrigger && wasNegativeModeAtTrigger && !channelData.freqSweepSub)
        //    GBMB.gameboy.memory.SetChannelActiveFlag(channelData.channel, false);
        //decrement sweep timer
        if (freqSweepTimer > 0)
            freqSweepTimer--;
        //reload sweep timer
        if (freqSweepTimer == 0)
        {
            ReloadSweepTimer();
            //sweep active?
            if (freqSweepEnabled && channelData.freqSweepTime > 0)
            {
                //Recalc freq
                //Debug.LogError("[" + GBPlayer.instance.frameCount + "] CalcClockFreq");
                CalculateNewFrequency(true);
            }
        }
    }

    private void ReloadSweepTimer()
    {
        freqSweepTimer = channelData.freqSweepTime;
        if (freqSweepTimer == 0)//obscure behaviour timer 0 is treated as 8
            freqSweepTimer = 8;
    }

    private void CheckOverflow(ushort shiftResult)
    {
        if (shiftResult > 0x07FF)
        {
            //Debug.LogError("[" + GBPlayer.instance.frameCount + "] OVERFLOW FreqStopChannel1");
            audio.SetChannelActiveFlag(channelData.channel, false);
        }
    }

    private void CalculateNewFrequency(bool writeBack = false)
    {
        if (!freqSweepEnabled)
            return;
        //Shift frequency
        //Debug.LogError("[" + GBPlayer.instance.frameCount + "] PRECALC " + shadowSweepFreq.ToString("X4"));
        ushort shiftResult = (ushort)(shadowSweepFreq >> channelData.freqSweepShift);
        if (!channelData.freqSweepSub)
        {
            shiftResult = (ushort)(shiftResult + shadowSweepFreq);
        }
        else
        {
            shiftResult = (ushort)(shiftResult - shadowSweepFreq);
        }
        //Debug.LogError("[" + GBPlayer.instance.frameCount + "] POSTCALC " + shiftResult.ToString("X4"));
        //Check overflow
        CheckOverflow(shiftResult);
        //didCalculateSinceTrigger = true;
        //Write back to registers?
        if (!writeBack)
            return;
        if (shiftResult <= 0x07FF && channelData.freqSweepShift != 0)
        {
            //Debug.LogWarning("[" + GBPlayer.instance.frameCount + "] WRITE FREQ BACK " + shiftResult.ToString("X4"));
            shadowSweepFreq = shiftResult;
            channelData.RegisterFrequency = shadowSweepFreq;
            //IMMEDIATELY recalculate but don't write back.
            //Debug.LogError("[" + GBPlayer.instance.frameCount + "] IMMEDIATE RE-CalcClockFreq");
            CalculateNewFrequency(false);
        }
    }

    internal override void TriggerVolEnvClock()
    {
        //envVol active, and sweep not 0?
        if (!envVolEnabled)
            return;
        envVolCounter--;
        if (envVolCounter == 0)
        {
            if (channelData.envelopeIncr)
            {
                currentEnvVol++;
                if (currentEnvVol >= 15)
                {
                    currentEnvVol = 15;
                    envVolEnabled = false;
                }
            }
            else
            {
                currentEnvVol--;
                if (currentEnvVol <= 0)
                {
                    currentEnvVol = 0;
                    envVolEnabled = false;
                }
            }
            envVolCounter = channelData.envelopeSweepTime;
        }
    }

    protected override void CreateAudioBufferSegment(float[] data, int channels)
    {
        if (!audio.settings.audioChannelsOn[0] || !audio.AudioPowered || !channelData.DacIsEnabled)//DACs on?
            return;
        //THE BELOW SHOULD TECHNICALLY BE COVERED BY DAC ENABLE? (length = 0 disables DAC)
        //if (channelData.lengthEnable && channelData.lengthTimer == 0)//did audio finish lengthwise?
            //return;
        int totalSamples = data.Length / channels;
        int sample = 0;
        while (sample < totalSamples)
        {
            phase += sampleIncrease;
            int channel = 0;
            while (channel < channels)
            {
                if (!audio.ChannelSideActive(channelData.channel,channel))
                {
                    channel++;
                    continue;
                }
                //random square wave noise.
                int length = channelData.GetWavePattern().Length;
                int ptr = (int)Math.Floor(phase * length / doublePI);//current sample
                if (ptr >= length)
                    ptr = length - 1;
                float d = data[sample * channels + channel] = channelData.GetWavePattern()[ptr] * (currentEnvVol / 15f);
                //dropoff
                data[sample * channels + channel] = AudioPostProcessingFilter(d);
                channel++;
            }
            if (phase > doublePI)
            {
                phase -= doublePI;
            }
            sample++;
        }
    }
}