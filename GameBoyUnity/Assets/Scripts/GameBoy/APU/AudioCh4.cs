﻿using UnityEngine;

public class AudioCh4 : GBAudioChannel
{
    //register inforamtion
    public byte envelopeSweepTime;
    public bool envelopeIncr;
    public byte envelopeStartVolume;//0-F (0 to 15)
    public float divRatio;
    public bool shortMode;
    public byte shiftClockFreq;
    
    //filter generation information
    private double samplesPerChange = 0;
    private double nextSampleChangeCounter = 0;
    private bool envVolEnabled = false;
    private byte envVolCounter = 0;
    private byte currentEnvVol;
    private ushort noiseLFSR = 0;
    //move to base
    private readonly float sampleRate;

    public AudioCh4(GBAudio audio, AudioChannel channel, int sampleRate, IGBAudioOutput audioOutput)
        : base(audio, channel, sampleRate, audioOutput)
    {
        var myFilter = new GBAudioFilterNoise(audio, channel, sampleRate, audioOutput);
        myFilter.SetChannelData(this);
        filter = myFilter;

        this.sampleRate = sampleRate;
    }

    internal override void OnRegisterChanged(ushort address, byte data)
    {
        switch (address)
        {
            case 0xFF20:
                {
                    lengthTimer = (byte)(64 - (0x3F & data));//0-5bit
                    break;
                }
            case 0xFF21:
                {
                    envelopeSweepTime = (byte)(data & 0x07);
                    envelopeIncr = Util.BitCheck(3, data);
                    envelopeStartVolume = (byte)((data >> 4) & 0x0F);
                    //enable/disable DAC
                    DacIsEnabled = (data & 0xF8) != 0;
                    break;
                }
            case 0xFF22:
                {
                    divRatio = data & 0x07;
                    if (divRatio == 0f)
                        divRatio = 0.5f;
                    shortMode = Util.BitCheck(3, data);
                    shiftClockFreq = (byte)((data >> 4) & 0x07);
                    //der
                    outputFrequency = 524288 / divRatio / Mathf.Pow(2f, shiftClockFreq + 1);
                    FrequencyChanged();
                    break;
                }
            case 0xFF23:
                {
                    bool previousLengthEnable = lengthEnable;
                    lengthEnable = Util.BitCheck(6, data);
                    bool trigger = Util.BitCheck(7, data);
                    //Extra length clock? (lengthEnable and Length > 0 are checked already inside)
                    if (!previousLengthEnable && audio.SequencerStep % 2 == 1)
                        ClockLengthCtr();
                    if (trigger)
                    {
                        DoTrigger();
                    }
                    break;
                }
        }
    }
    
    internal override void ProcessTick()
    {
        filter.ProcessTick();
    }

    internal override void DoTrigger()
    {
        base.DoTrigger();
        //Frequency to period
        //TODO
        //volume envelope timer to max envelope sweep time
        envVolCounter = envelopeSweepTime;
        envVolEnabled = true;
        //Reset channel volume
        currentEnvVol = envelopeStartVolume;
        //Noise channel LFSR bits reset
        noiseLFSR = 0;
    }

    internal override void FrequencyChanged()
    {
        //Set sample increase based on frequency
        samplesPerChange = (sampleRate / outputFrequency);
    }

    internal override void ClockSweep()
    {
        //envVol active, and sweep not 0?
        if (!envVolEnabled || envelopeSweepTime == 0)
            return;
        envVolCounter--;
        if (envVolCounter == 0)
        {
            if (envelopeIncr)
            {
                currentEnvVol++;
                if (currentEnvVol >= 15)
                {
                    currentEnvVol = 15;
                    envVolEnabled = false;
                }
            }
            else
            {
                currentEnvVol--;
                if (currentEnvVol <= 0)
                {
                    currentEnvVol = 0;
                    envVolEnabled = false;
                }
            }
            envVolCounter = envelopeSweepTime;
        }
    }
    
    protected internal void CreateAudioBufferSegment(float[] data, int channels)
    {
        if (!audio.settings.audioChannelsOn[(int)channel] || !audio.AudioPowered || !DacIsEnabled)
            return;
        
        int totalSamples = data.Length / channels;
        int sample = 0;
        while (sample < totalSamples)
        {
            int outputChannel = 0;
            while (outputChannel < channels)
            {
                if (!audio.ChannelSideActive(channel,outputChannel))
                {
                    outputChannel++;
                    continue;
                }
                //random square wave noise.
                int noiseValue = noiseLFSR & 0x0001;
                float d = data[sample * channels + outputChannel] = noiseValue * (currentEnvVol / 15f);//stomp down volume
                //dropoff
                data[sample * channels + outputChannel] = filter.AudioPostProcessingFilter(d);
                outputChannel++;
            }
            if (nextSampleChangeCounter <= 0)
            {
                ProgressLFSR();
                //TODO setting this up properly somehow breaks the noise. Maybe we need to simulate proper noise for this to work?
                nextSampleChangeCounter = (int)(sampleRate / outputFrequency);
            }
            else
            {
                nextSampleChangeCounter--;
            }
            sample++;
        }
    }

    /// <summary>
    /// Progresses the noise buffer
    /// </summary>
    private void ProgressLFSR()
    {
        int check = noiseLFSR & 0x0003;
        ushort newBit = (ushort) (check == 0x00 || check == 0x03 ? 0x8000 : 0x0000);
        noiseLFSR |= newBit;
        if(shortMode)
            noiseLFSR |= (ushort)(newBit >> 8);
        noiseLFSR = (ushort)(noiseLFSR >> 1);
    }
}