﻿using System;
using UnityEngine;

public class GBAudioFilterWave : GBAudioFilterBase
{
    private AudioCh3 channelData;
    
    private double sampleIncrease;

    public GBAudioFilterWave(GBAudio audio, AudioChannel channel, int sampleRate, IGBAudioOutput output) : base(audio, channel, sampleRate, output)
    {
        
    }

    public void SetChannelData(AudioCh3 chan)
    {
        channelData = chan;
    }

    internal override void TriggerEvent()
    {
        //Frequency+Envelope timer to period
        phase = 0;
        //Reset volume
        // Not applicable in wave
    }

    internal override void FrequencyChanged()
    {
        //Set sample increase based on frequency
        sampleIncrease = channelData.outputFrequency * doublePI / sampleRate;
    }

    protected override void CreateAudioBufferSegment(float[] data, int channels)
    {
        if (!audio.settings.audioChannelsOn[2] || !audio.AudioPowered || !channelData.DacIsEnabled)//DACs on?
            return;
        //THE BELOW SHOULD TECHNICALLY BE COVERED BY DAC ENABLE? (length = 0 disables DAC)
        //if (channelData.lengthEnable && channelData.lengthTimer == 0)//did audio finish lengthwise?
        //return;
        int totalSamples = data.Length / channels;
        int sample = 0;
        while (sample < totalSamples)
        {
            phase += sampleIncrease;
            int channel = 0;
            while (channel < channels)
            {
                if (!audio.ChannelSideActive(channelData.channel,channel))
                {
                    channel++;
                    continue;
                }
                //random square wave noise.
                int length = channelData.waveData.Length;
                int ptr = (int)Math.Floor(phase * length / doublePI);//current sample
                if (ptr >= length)
                    ptr = length - 1;
                float d = data[sample * channels + channel] = channelData.waveData[ptr] * channelData.outputVolume;
                //dropoff
                data[sample * channels + channel] = AudioPostProcessingFilter(d);
                channel++;
            }
            if(phase > doublePI)
            {
                phase -= doublePI;
            }
            sample++;
        }
    }
}
