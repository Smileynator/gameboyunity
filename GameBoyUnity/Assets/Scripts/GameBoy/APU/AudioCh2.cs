﻿
public class AudioCh2 : GBAudioChannel
{
    public byte wavePattern;
    public byte envelopeSweepTime;
    public bool envelopeIncr;
    public byte envelopeStartVolume;//0-F (0 to 15)
    public ushort frequencyData;
    public float[][] wavePatterns = new float[][]
    {
        new float[]{ -1,-1,-1,-1,-1,-1,-1,1 },
        new float[]{ 1,-1,-1,-1,-1,-1,-1,1 },
        new float[]{ 1, 1,-1,-1,-1,-1,1,1 },
        new float[]{ -1,1,1,1,1,1,1,-1 }
    };

    public AudioCh2(GBAudio audio, AudioChannel channel, int sampleRate, IGBAudioOutput audioOutput)
        : base(audio, channel, sampleRate, audioOutput)
    {
        var myFilter = new GBAudioFilterTone(audio, channel, sampleRate, audioOutput);
        myFilter.SetChannelData(this);
        filter = myFilter;
    }

    internal float[] GetWavePattern()
    {
        return wavePatterns[wavePattern];
    }

    internal override void OnRegisterChanged(ushort address, byte data)
    {
        switch (address)
        {
            case 0xFF16:
                {
                    lengthTimer = (byte)(64 - (0x3F & data));//0-5bit
                    wavePattern = (byte)(0x03 & (data >> 6));//6-7 bit
                    break;
                }
            case 0xFF17:
                {
                    envelopeSweepTime = (byte)(data & 0x07);
                    envelopeIncr = Util.BitCheck(3, data);
                    envelopeStartVolume = (byte)((data >> 4) & 0x0F);
                    //enable/disable DAC
                    DacIsEnabled = (data & 0xF8) != 0;
                    break;
                }
            case 0xFF18:
                {
                    frequencyData = (ushort)(frequencyData & 0xFF00);
                    frequencyData = (ushort)(frequencyData | data);
                    //der
                    outputFrequency = 131072 / (2048 - frequencyData);
                    FrequencyChanged();
                    break;
                }
            case 0xFF19:
                {
                    frequencyData = (ushort)(frequencyData & 0xF8FF);
                    int d = (data & 0x07) << 8;
                    frequencyData = (ushort)(frequencyData | d);
                    bool previousLengthEnable = lengthEnable;
                    lengthEnable = Util.BitCheck(6, data);
                    bool trigger = Util.BitCheck(7, data);
                    //der
                    outputFrequency = 131072 / (2048 - frequencyData);
                    FrequencyChanged();
                    //Extra length clock? (lengthEnable and Length > 0 are checked already inside)
                    if (!previousLengthEnable && audio.SequencerStep % 2 == 1)
                        ClockLengthCtr();
                    if (trigger)
                    {
                        DoTrigger();
                    }
                    break;
                }
        }
    }
}
