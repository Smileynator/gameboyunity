﻿
public class GBAudioChannel
{
    public bool DacIsEnabled
    {
        get => dacIsEnabled;
        set
        {
            dacIsEnabled = value;
            if (!dacIsEnabled)
            {//disable the channel
                audio.SetChannelActiveFlag(channel,false);
            }
        }
    }

    public bool lengthEnable;
    public ushort lengthTimer;
    public ushort maxSoundLength = 64;
    //shadow registers
    public double outputFrequency;
    public AudioChannel channel;

    private bool dacIsEnabled;
    protected GBAudio audio;
    protected GBAudioFilterBase filter;

    protected GBAudioChannel(GBAudio audio, AudioChannel channel, int sampleRate, IGBAudioOutput audioOutput)
    {
        this.audio = audio;
        this.channel = channel;
    }

    internal virtual void OnRegisterChanged(ushort address, byte data)
    {

    }

    /// <summary>
    /// Called once per cpu cycle that passes
    /// </summary>
    internal virtual void ProcessTick()
    {
        filter.ProcessTick();
    }

    internal virtual void DoTrigger()
    {
        //set max sound length
        if (lengthTimer == 0)
        {
            lengthTimer = maxSoundLength;
            if (audio.SequencerStep % 2 == 1)
                ClockLengthCtr();
        }
        //Debug.LogError("[" + GBPlayer.instance.frameCount + "] TrigEnable "+channel.ToString());
        audio.SetChannelActiveFlag(channel, true);
        //Do all other trigger logic.
        filter.TriggerEvent();

    }

    internal virtual void FrequencyChanged()
    {
        filter.FrequencyChanged();
    }

    internal virtual void ClockSweep()
    {
        filter.TriggerFreqSweepClock();
    }

    internal virtual void ClockVolEnv()
    {
        filter.TriggerVolEnvClock();
    }

    internal virtual void ClockLengthCtr()
    {
        //should do counter?
        if (!lengthEnable)
            return;
        //count down
        if (lengthTimer > 0)
        {
            lengthTimer--;
            if (lengthTimer == 0)
            {
                //Debug.LogError("[" + GBPlayer.instance.frameCount + "] LengthDisable " + channel.ToString());
                audio.SetChannelActiveFlag(channel, false);
            }
        }
    }

    /// <summary>
    /// When the APU is turned on you likely want to reset some things
    /// </summary>
    internal virtual void ApuTurnedOff()
    {
        filter.ResetDutyStep();
    }
    
    internal virtual void UpdateWaveData(ushort address, byte data)
    {
        //only for the wave channel
    }
}