﻿using System;

public struct SpriteAttribute
{
    public byte OAMIndex;
    public short yPos;
    public short xPos;
    public byte tileNumber;
    public bool priority;
    public bool verticalFlip;
    public bool horizontalFlip;
    public bool paletteOBP1;

    /// <summary>
    /// Pass in for the 4 bytes of data that make up the sprite attributes
    /// </summary>
    public SpriteAttribute(byte index, ReadOnlySpan<byte> data)
    {
        OAMIndex = index;
        //correct for the upper right corner
        yPos = (short)(data[0] - 16);
        xPos = (short)(data[1] - 8);
        tileNumber = data[2];
        byte flags = data[3];
        priority = (flags & 0b10000000) == 0;
        verticalFlip = (flags & 0b01000000) != 0;
        horizontalFlip = (flags & 0b00100000) != 0;
        paletteOBP1 = (flags & 0b00010000) != 0;
    }
}
