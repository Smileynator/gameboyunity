using System.Runtime.CompilerServices;

public static class Util
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool BitCheck(int bit, byte data)
    {
        return (data & (1 << bit)) != 0;
    }
}
