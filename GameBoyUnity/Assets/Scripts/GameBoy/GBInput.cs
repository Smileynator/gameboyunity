﻿
public class GBInput
{
    private readonly IGBClockRead clock;
    private readonly IGBMemoryJoyPad memory;
    private readonly IGBButtonInput buttonImp;

    private byte lastRead = 0xFF;

    public GBInput(IGBClockRead clock, IGBMemoryJoyPad memory, IGBButtonInput buttonImp)
    {
        this.clock = clock;
        this.memory = memory;
        this.buttonImp = buttonImp;
        clock.MCycle += OnClock;
    }

    ~GBInput()
    {
        clock.MCycle -= OnClock;
    }

    private void OnClock()
    {
        byte joyPadRegister = memory.ReadJoyPadInput();
        if (joyPadRegister == lastRead)
        {
            return;
        }
        lastRead = joyPadRegister;
        var buttonStates = buttonImp.GetButtonStates();
        //get the selector bits
        int selectorBits = joyPadRegister & 0x30;
        //clear the lower 4 input bits
        byte inputBits = 0x0F;
        if (selectorBits != 0x30)
        {
            //PIN 4 - A B SELECT START
            if ((selectorBits & 0b00100000) == 0)
            {
                inputBits = buttonStates.ButtonsByte();
            }
            //PIN 5 - RIGHT LEFT UP DOWN
            if ((selectorBits & 0b00010000) == 0)
            {
                inputBits &= buttonStates.DirectionByte();
            }
        }
        joyPadRegister &= 0xF0;//clear input bits
        joyPadRegister |= inputBits;//write input bits
        
        memory.WriteJoyPadInput(joyPadRegister);
    }
}
