﻿
public struct LCDStatus
{
    public PPUMode mode; //0-3
    public bool coincidenceLY;
    public bool hBlankInterrupt;
    public bool vBlankInterrupt;
    public bool OAMInterrupt;
    public bool coincidenceLYInterrupt;

    public LCDStatus(byte data)
    {
        mode = (PPUMode)(data & 0x03);
        coincidenceLY = (data & (1 << 2)) != 0;
        hBlankInterrupt = (data & (1 << 3)) != 0;
        vBlankInterrupt = (data & (1 << 4)) != 0;
        OAMInterrupt = (data & (1 << 5)) != 0;
        coincidenceLYInterrupt = (data & (1 << 6)) != 0;
    }
}