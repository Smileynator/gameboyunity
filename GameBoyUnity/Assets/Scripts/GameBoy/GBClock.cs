using System;

public class GBClock : IGBClock
{
    public event Action MCycle = delegate { };
    private const int mCyclesPerFrame = 17556;
    
    public void ClockMCycle()
    {
        MCycle();
    }
    
    public void ClockMCycles(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            MCycle();
        }
    }
}
