﻿using System;

public enum GBCartridgeType
{
    ROM_ONLY = 0x00,
    MBC1 = 0x01,
    MBC1_RAM = 0x02,
    MBC1_RAM_BAT = 0x03,
    MBC2 = 0x05,
    MBC2_BAT = 0x06,
    ROM_RAM = 0x08,
    ROM_RAM_BAT = 0x09,
    MMM01 = 0x0B,
    MMM01_RAM = 0x0C,
    MMM01_RAM_BAT = 0x0D,
    MBC3_TIMER_BAT = 0x0F,
    MBC3_TIMER_RAM_BAT = 0x10,
    MBC3 = 0x11,
    MBC3_RAM = 0x12,
    MBC3_RAM_BAT = 0x13,
    MBC5 = 0x19,
    MBC5_RAM = 0x1A,
    MBC5_RAM_BAT = 0x1B,
    MBC5_RUMBLE = 0x1C,
    MBC5_RUMBLE_RAM = 0x1D,
    MBC5_RUMBLE_RAM_BAT = 0x1E,
    MBC6 = 0x20,
    MBC7_SENS_RUMBLE_RAM_BAT = 0x22,

    POCKET_CAMERA = 0xFC,
    BANDAI_TAMA5 = 0xFD,
    HuC3 = 0xFE,
    HuC1_RAM_BAT = 0xFF
}

public enum GBCartridgeMBCType
{
    Unknown,
    MBC2,
    MBC3,
    MBC5,
    MBC6,
    MBC7,
}

public class CartridgeHeader
{
    public string Title { get; private set; }
    public byte[] ManufacturerCode { get; private set; }
    public byte ColorGBFlag { get; private set; }
    public short LicenseeCode { get; private set; }
    public bool SuperGBFlag { get; private set; }
    public GBCartridgeType CartridgeType { get; private set; }


    public GBCartridgeMBCType MBCType => GetMBCType();
    public byte ROMInfo { get; private set; }//4 = 32 banks, 5 = 64
    public byte RAMInfo { get; private set; }
    public bool LargeRAM => RAMInfo > 2;
    public bool LargeROM => ROMInfo > 4;
    public bool JapanGame { get; private set; }
    public byte OldLicenseeCode { get; private set; }
    public byte GameVersion { get; private set; }

    public int ROMBankCount { get; private set; }
    public int RAMBankCount { get; private set; }
    public int RAMBankSize { get; private set; }

    public CartridgeHeader(byte[] GBRom)
    {
        byte[] titleBytes = new byte[16];
        Array.Copy(GBRom, 0x0134, titleBytes, 0, 16);
        byte zeroByte = 0;
        for (byte i = 0; i < titleBytes.Length; i++)
        {
            if (titleBytes[i] == 0x00)
            {
                zeroByte = i;
                break;
            }
        }
        Title = System.Text.Encoding.ASCII.GetString(titleBytes, 0, zeroByte);
        ManufacturerCode = new byte[4];
        Array.Copy(GBRom, 0x013F, ManufacturerCode, 0, 4);
        ColorGBFlag = GBRom[0x0143];
        LicenseeCode = (short)((GBRom[0x0144] << 8) | GBRom[0x0145]);
        SuperGBFlag = (GBRom[0x0146] == 0x03);
        CartridgeType = (GBCartridgeType)GBRom[0x0147];
        ROMInfo = GBRom[0x0148];
        RAMInfo = GBRom[0x0149];
        JapanGame = GBRom[0x014A] == 0x00;
        OldLicenseeCode = GBRom[0x014B];
        GameVersion = GBRom[0x014C];
        SetupBankSizes();
    }

    public GBCartridgeMBCType GetMBCType()
    {
        switch (CartridgeType)
        {
            //any of these we continue, otherwise return without doing anything
            case GBCartridgeType.MBC2:
            case GBCartridgeType.MBC2_BAT:
                return GBCartridgeMBCType.MBC2;
            case GBCartridgeType.MBC3:
            case GBCartridgeType.MBC3_RAM:
            case GBCartridgeType.MBC3_RAM_BAT:
            case GBCartridgeType.MBC3_TIMER_BAT:
            case GBCartridgeType.MBC3_TIMER_RAM_BAT:
                return GBCartridgeMBCType.MBC3;
            case GBCartridgeType.MBC5:
            case GBCartridgeType.MBC5_RAM:
            case GBCartridgeType.MBC5_RAM_BAT:
            case GBCartridgeType.MBC5_RUMBLE:
            case GBCartridgeType.MBC5_RUMBLE_RAM:
            case GBCartridgeType.MBC5_RUMBLE_RAM_BAT:
                return GBCartridgeMBCType.MBC5;
            case GBCartridgeType.MBC6:
                return GBCartridgeMBCType.MBC6;
            case GBCartridgeType.MBC7_SENS_RUMBLE_RAM_BAT:
                return GBCartridgeMBCType.MBC7;
            default:
                return GBCartridgeMBCType.Unknown;
        }
    }
    
    public bool IsMBC2()
    {
        switch (CartridgeType)
        {
            //any of these we continue, otherwise return without doing anything
            case GBCartridgeType.MBC2:
            case GBCartridgeType.MBC2_BAT:
                return true;
            default:
                return false;
        }
    }
    
    internal bool IsMBC3()
    {
        switch (CartridgeType)
        {
            //any of these we continue, otherwise return without doing anything
            case GBCartridgeType.MBC3:
            case GBCartridgeType.MBC3_RAM:
            case GBCartridgeType.MBC3_RAM_BAT:
            case GBCartridgeType.MBC3_TIMER_BAT:
            case GBCartridgeType.MBC3_TIMER_RAM_BAT:
                return true;
            default:
                return false;
        }
    }

    public bool IsMBC5()
    {
        switch (CartridgeType)
        {
            //any of these we continue, otherwise return without doing anything
            case GBCartridgeType.MBC5:
            case GBCartridgeType.MBC5_RAM:
            case GBCartridgeType.MBC5_RAM_BAT:
            case GBCartridgeType.MBC5_RUMBLE:
            case GBCartridgeType.MBC5_RUMBLE_RAM:
            case GBCartridgeType.MBC5_RUMBLE_RAM_BAT:
                return true;
            default:
                return false;
        }
    }
    
    public bool HasRumble()
    {
        switch (CartridgeType)
        {
            case GBCartridgeType.MBC5_RUMBLE:
            case GBCartridgeType.MBC5_RUMBLE_RAM:
            case GBCartridgeType.MBC5_RUMBLE_RAM_BAT:
            case GBCartridgeType.MBC7_SENS_RUMBLE_RAM_BAT:
                return true;
            default:
                return false;
        }
    }

    private void SetupBankSizes()
    {
        switch (ROMInfo)
        {
            case 0x00:
                {
                    ROMBankCount = 2;
                }
                break;
            case 0x01:
                {
                    ROMBankCount = 4;
                }
                break;
            case 0x02:
                {
                    ROMBankCount = 8;
                }
                break;
            case 0x03:
                {
                    ROMBankCount = 16;
                }
                break;
            case 0x04:
                {
                    ROMBankCount = 32;
                }
                break;
            case 0x05:
                {
                    ROMBankCount = 64;
                }
                break;
            case 0x06:
                {
                    ROMBankCount = 128;
                }
                break;
            case 0x07:
                {
                    ROMBankCount = 256;
                }
                break;
            case 0x08:
                {
                    ROMBankCount = 512;
                }
                break;
            case 0x52:
                {
                    ROMBankCount = 72;
                }
                break;
            case 0x53:
                {
                    ROMBankCount = 80;
                }
                break;
            case 0x54:
                {
                    ROMBankCount = 96;
                }
                break;
        }

        if (CartridgeType.ToString().Contains("RAM"))
        {
            switch (RAMInfo)
            {
                case 0:
                case 1:
                {
                    //nothing
                }
                    break;
                case 2://8 KiB
                {
                    RAMBankSize = 8192;
                    RAMBankCount = 1;
                }
                    break;
                case 3://32 KiB
                {
                    RAMBankSize = 8192;
                    RAMBankCount = 4;
                }
                    break;
                case 4://128 KiB
                {
                    RAMBankSize = 8192;
                    RAMBankCount = 16;
                }
                    break;
                case 5://64 KiB
                {
                    RAMBankSize = 8192;
                    RAMBankCount = 8;
                }
                    break;
            }
        }
        else
        {//force set these to reset just in case
            RAMBankCount = 0;
            RAMBankSize = 0;
        }
    }

}
