﻿using System;

public class MBC1Registers
{
    public byte lowRomBank = 0x01;
    public byte highRomBank = 0x00;
    public bool bankingMode = false;

    public byte highRomBankOffset => (byte)(highRomBank << 5);
}

public class MBC3Registers
{
    public byte RAMRegister = 0x00;
    public byte latchRegister = 0x01;
    public bool RTCLatched => RTCisLatched;

    public byte[] RTC = new byte[5];
    public byte[] RTCL = new byte[5];
    private bool RTCisLatched = false;

    public void LatchRTC()
    {
        RTCisLatched = !RTCisLatched;
        if (RTCisLatched)
        {
            Array.Copy(RTC, RTCL, 5);
        }
    }

    public byte GetRTCData()
    {
        int index = RAMRegister - 0x08;
        if (RTCisLatched)
        {
            return RTCL[index];
        }
        return RTC[index];
    }

    public void SetRTCData(byte data)
    {
        int index = RAMRegister - 0x08;
        RTC[index] = data;
    }

    public void ClockRTC()
    {
        //timer halted?
        if ((RTC[4] & 0x40) != 0x00)
            return;
        //up the time
        RTC[0]++;
        if (RTC[0] > 59)
        {
            RTC[1]++;
            RTC[0] = 0;
            if (RTC[1] > 59)
            {
                RTC[2]++;
                RTC[1] = 0;
                if (RTC[2] > 23)
                {
                    RTC[3]++;
                    RTC[2] = 0;
                    if (RTC[3] == 0)
                    {
                        if ((RTC[4] & 0x01) == 0)
                        {
                            RTC[4]++;
                        }
                        else
                        {
                            //set day overflow bit
                            RTC[4] = (byte)(RTC[4] | 0x80);
                            //reset day bit to 0
                            RTC[4] = (byte)(RTC[4] & 0xFE);
                        }
                    }
                }
            }
        }
    }
}

public class MBC5Registers
{
    public byte lowRomBank = 0x00;
    public byte highRomBank = 0x00;

    public byte highRomBankOffset => (byte)(highRomBank << 8);
}