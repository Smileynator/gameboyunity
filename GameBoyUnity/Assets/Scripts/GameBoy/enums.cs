﻿public enum CPUReg16 { AF, BC, DE, HL, SP, PC }

public enum CPUReg8 { A, B, C, D, E, H, L, BCasPTR, DEasPTR, HLasPTR, ImmediateValue, Immediate2AsPTR }

public enum PPUMode { H_Blank_0 = 0, V_Blank_1 = 1, ScanlineOAM_2 = 2, ScanlineVRAM_3 = 3}

public enum Interrupt { V_Blank = 1 << 0, STAT_LCDC = 1 << 1, TimerOverflow = 1 << 2, SerialTransfer = 1 << 3, Joypad = 1 << 4 }

public enum AudioChannel { Channel1 = 0, Channel2 = 1, Channel3 = 2 , Channel4 = 3}