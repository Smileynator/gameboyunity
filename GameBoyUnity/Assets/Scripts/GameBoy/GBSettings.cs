﻿using System;
using UnityEngine;

[Serializable]
public class GBSettings
{
    public bool skipBootrom = false;
    public bool limitFramerate = true;
    public double desiredFrameLength = 16.7427062988;
    public float masterVolume = 0.2f;
    public int audioSampleRate => AudioSettings.outputSampleRate;
    public bool[] audioChannelsOn = new bool[] { true,true,true,true};
}
