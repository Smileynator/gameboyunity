public struct ButtonPressedStates
{ 
    public bool A;
    public bool B;
    public bool Select;
    public bool Start;
    public bool Right;
    public bool Left;
    public bool Up;
    public bool Down;

    public byte DirectionByte()
    {
        int output = System.Convert.ToInt32(Right) | 
                     System.Convert.ToInt32(Left) << 1 | 
                     System.Convert.ToInt32(Up) << 2 | 
                     System.Convert.ToInt32(Down) << 3;
        output = ~output & 0x0F;//invert lower 4 bits
        return (byte)output;
    }

    public byte ButtonsByte()
    {
        int output = System.Convert.ToInt32(A) | 
                     System.Convert.ToInt32(B) << 1 | 
                     System.Convert.ToInt32(Select) << 2 | 
                     System.Convert.ToInt32(Start) << 3;
        output = ~output & 0x0F;//invert lower 4 bits
        return (byte)output;
    }
}