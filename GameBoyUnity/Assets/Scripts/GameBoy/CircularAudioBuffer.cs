using System;

public class CircularAudioBuffer<T>
{
    public int ReadBehindCounter => readBehindCounter;
    private readonly T[] buffer;
    private readonly int channels;//the minimal increment
    private int readPointer;
    private int writePointer;
    private int readBehindCounter;

    /// <summary>
    /// runs off of the assumption that Channels is the amount of samples we will always read and write.
    /// </summary>
    public CircularAudioBuffer(int capacity, int channels)
    {
        buffer = new T[capacity];
        this.channels = channels;
    }

    public void Write(ref T[] bufferToWrite)
    {
        if(bufferToWrite.Length > buffer.Length)
            throw new ArgumentException(nameof(bufferToWrite)); //it cannot be longer than this

        lock (this)
        {
            if (writePointer + bufferToWrite.Length <= buffer.Length)
            {
                // Write the entire array if it fits without wrapping around
                Array.Copy(bufferToWrite, 0, buffer, writePointer, bufferToWrite.Length);
                // Update the writePointer
                writePointer += bufferToWrite.Length;
            }
            else
            {
                // Write done in 2 parts to get the overflow right
                int endBufferLength = buffer.Length - writePointer;
                Array.Copy(bufferToWrite, 0, buffer, writePointer, endBufferLength);
                int startBufferLength = bufferToWrite.Length - endBufferLength;
                Array.Copy(bufferToWrite, endBufferLength, buffer, 0, startBufferLength);
                // Update the writePointer
                writePointer = startBufferLength;
            }
            readBehindCounter += bufferToWrite.Length;
        }
    }
    
    public void Read(ref T[] bufferToFill)
    {
        if(bufferToFill.Length > buffer.Length)
            throw new ArgumentException(nameof(bufferToFill)); //it cannot be longer than this
        
        lock (this)
        {
            //immediately if buffer is starved
            if (readBehindCounter > bufferToFill.Length)
            {
                //read and catch up
                ReadBuffer(ref bufferToFill);
            }
        }
    }
    
    private void ReadBuffer(ref T[] bufferToFill)
    {
        if (readPointer + bufferToFill.Length <= buffer.Length)
        {
            // Read the entire array if it's contiguous in the buffer
            Array.Copy(buffer, readPointer, bufferToFill, 0, bufferToFill.Length);
            // Update the readPointer
            readPointer += bufferToFill.Length;
        }
        else
        {
            // Read done in 2 parts to get the overflow right
            int tillEndOfBuffer = buffer.Length - readPointer;
            Array.Copy(buffer, readPointer, bufferToFill, 0, tillEndOfBuffer);
            int fromStartOfBuffer = bufferToFill.Length - tillEndOfBuffer;
            Array.Copy(buffer, 0, bufferToFill, tillEndOfBuffer, fromStartOfBuffer);
            // Update the readPointer
            readPointer = fromStartOfBuffer;
        }
        readBehindCounter -= bufferToFill.Length;
    }
}
