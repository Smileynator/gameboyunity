using System;
using UnityEngine;

public class UnityButtonInput : MonoBehaviour, IGBButtonInput
{
    public ButtonState a, b, start, select, up, down, left, right;

    private ButtonPressedStates buttonState;
    
    private void Update()
    {
        bool A = a.IsHeld || Input.GetKey(KeyCode.Z);
        bool B = b.IsHeld || Input.GetKey(KeyCode.X);
        bool Start = start.IsHeld || Input.GetKeyDown(KeyCode.Return);
        bool Select = select.IsHeld || Input.GetKey(KeyCode.RightShift);
        bool Up = up.IsHeld || Input.GetKey(KeyCode.UpArrow);
        bool Down = down.IsHeld || Input.GetKey(KeyCode.DownArrow);
        bool Left = left.IsHeld || Input.GetKey(KeyCode.LeftArrow);
        bool Right = right.IsHeld || Input.GetKey(KeyCode.RightArrow);
        
        lock (this)
        {
            buttonState = new ButtonPressedStates()
            {
                A = A,
                B = B,
                Start = Start,
                Select = Select,
                Up = Up,
                Down = Down,
                Left = Left,
                Right = Right
            };
        }
    }

    ButtonPressedStates IGBButtonInput.GetButtonStates()
    {
        ButtonPressedStates returnValue;
        lock (this)
        {
            returnValue = buttonState;
        }
        return returnValue;
    }
}