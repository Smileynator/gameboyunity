using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UnityGBTester : MonoBehaviour
{
    public RawImage screenImage;
    public string testRomPath;
    public GBSettings settings;
    public Color32[] screenColors = new Color32[]
    {
        new Color(1f, 1f, 1f), new Color(0.66f, 0.66f, 0.66f), new Color(0.33f, 0.33f, 0.33f), new Color(0f, 0f, 0f)
    };
    public int framesToRun = 1000;
    
    /*private GBMB motherboard;
    private TestingScreen screen;
    private Texture2D screenTexture;
    private Color32[] textureBuffer;
    
    void Start()
    {
        screen = new TestingScreen();
        StartCoroutine(TestAllRoms());
    }

    private IEnumerator TestAllRoms()
    {
        //Get all roms
        var roms = EnumerateFiles(testRomPath);
        foreach (var rom in roms)
        {
            //Start the rom one by one
            Debug.Log("Testing ROM: " + Path.GetFileName(rom));
            StartTestRom(rom);
            //run rom for a fixed amount of frames
            for (int i = 0; i < framesToRun; i++)
            {
                motherboard.ProcessFrame();
            }
            //check output
            lock (screen.screenData)
            {
                if (screenTexture == null)
                {
                    screenTexture = new Texture2D(screen.screenSize.x, screen.screenSize.y, TextureFormat.RGBA32, false);
                    screenTexture.wrapMode = TextureWrapMode.Clamp;
                    screenTexture.filterMode = FilterMode.Point;
                    screenImage.texture = screenTexture;
                    textureBuffer = new Color32[screen.screenSize.x * screen.screenSize.y];
                }
                UpdateScreenTexture();
            }
            screenTexture.SetPixelData(textureBuffer,0,0);
            screenTexture.Apply();
            yield return null;
        }
    }
    
    private static IEnumerable<string> EnumerateFiles(string path)
    {
        if (Directory.Exists(path))
        {
            var gbFiles = Directory.EnumerateFiles(path, "*.gb");
            var gbcFiles = Directory.EnumerateFiles(path, "*.gbc");
            return gbFiles.Concat(gbcFiles);
        }
        else
        {
            throw new DirectoryNotFoundException("The rom directory does not exist.");
        }
    }

    void StartTestRom(string romPath)
    {
        if(motherboard != null)
        {
            motherboard.Destroy();
        }

        byte[] rom = OpenGameboyROM(romPath);
        motherboard = new GBMB(screen, null, null, null, settings, null, rom, null);
    }
    
    private byte[] OpenGameboyROM(string romPath)
    {
        byte[] gbRom = null;
        if (!File.Exists(romPath))
        {
            Debug.LogError($"Rom file not found! Path wrong? {romPath}");
            return null;
        }
        using (FileStream fs = new FileStream(romPath, FileMode.Open))
        {
            if(fs != null)
            {
                if(fs.Length > 16777216)
                {
                    Debug.Log("ROM FILE TOO BIG");
                }
                else
                {
                    gbRom = new byte[fs.Length];
                    int bytesWritten = fs.Read(gbRom, 0, (int)fs.Length);
                    Debug.Log("Loaded Rom " + bytesWritten + " bytes.");
                }
            }
            else
            {
                Debug.Log("ROM FILE NOT LOADABLE");
            }
        }
        return gbRom;
    }
    
    private void UpdateScreenTexture()
    {
        //we are locked here into SCREEN
        
        //Invert the row data we got
        for (int i = 0; i < screen.screenData.Length; i+= screen.screenSize.x)
        {
            //Inverse rows
            Array.Reverse(screen.screenData, i, screen.screenSize.x);
        }
        //used to invert the entire image to correctly draw to a unity texture
        int length = screen.screenData.Length-1;
        for (int i = 0; i < screen.screenData.Length; i++)
        {
            Color32 color =  screenColors[screen.screenData[length-i]];
            textureBuffer[i] = color;
        }
    }*/
}