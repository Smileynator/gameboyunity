using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Tries to render the Gameboy screen on the Unity main thread while not blocking the Gameboy.
/// </summary>
public class UnityScreen : MonoBehaviour, IGBScreenRenderer
{
    public RawImage screenRenderer;
    public Material materialToApply;
    public Color32[] screenColors = new Color32[] { new Color(0.878f, 0.973f, 0.816f), new Color(0.533f, 0.753f, 0.439f), new Color(0.1875f, 0.3828125f, 0.1875f), new Color(0.05859375f, 0.21875f, 0.05859375f) };
    
    Color32[] colorDefinitions => screenColors;
    
    private Texture2D screenTexture;
    private readonly ConcurrentQueue<Action> callQueue = new ();
    private byte[] screenData;
    private int screenWidth;
    private Color32[] textureBuffer;

    void IGBScreenRenderer.OnInitialize(int width, int height)
    {
        screenData = new byte[width * height];
        callQueue.Enqueue(() => Initialize(width, height));
    }

    void IGBScreenRenderer.OnFrameFinished(ref byte[] screenBuffer)
    {
        lock (screenData)
        {
            Array.Copy(screenBuffer, screenData, screenBuffer.Length);
        }
        callQueue.Enqueue(() => UpdateTexture());
    }

    private void LateUpdate()
    {
        while (callQueue.TryDequeue(out Action action))
        {
            action.Invoke();
        }
    }

    private void Initialize(int width, int height)
    {
        //Set up the render texture
        screenTexture = new Texture2D(width, height, TextureFormat.RGBA32, false);
        screenTexture.wrapMode = TextureWrapMode.Clamp;
        screenTexture.filterMode = FilterMode.Point;
        //Apply to RawImage
        if(screenRenderer != null)
        {
            screenRenderer.texture = screenTexture;
        }
        if(materialToApply != null)
        {
            materialToApply.mainTexture = screenTexture;
        }
        screenWidth = width;
        textureBuffer = new Color32[width * height];
    }

    private void UpdateTexture()
    {
        lock (screenData)
        {
            //Invert the row data we got
            for (int i = 0; i < screenData.Length; i+= screenWidth)
            {
               //Inverse rows
               Array.Reverse(screenData, i, screenWidth);
            }
            //used to invert the entire image to correctly draw to a unity texture
            int length = screenData.Length-1;
            for (int i = 0; i < screenData.Length; i++)
            {
                Color32 color = GhostingEffectPostProcessor(textureBuffer[i], colorDefinitions[screenData[length-i]]);
                textureBuffer[i] = color;
            }
        }
        screenTexture.SetPixelData(textureBuffer,0,0);
        screenTexture.Apply();
    }
    
    
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private Color32 GhostingEffectPostProcessor(Color32 currentColor, Color32 newColor)
    {
        var a = currentColor;
        var b = newColor;
        byte red = (byte)((a.r + b.r) >> 1);
        byte green = (byte)((a.g + b.g) >> 1);
        byte blue = (byte)((a.b + b.b) >> 1);
        return new Color32(red, green, blue, 0xFF);
    }
}
