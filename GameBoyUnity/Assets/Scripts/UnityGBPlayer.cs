﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System.IO;
using System.Text;
using Unity.Profiling;
using UnityEngine.Networking;
using UnityEngine.Serialization;

public class UnityGBPlayer : MonoBehaviour {
    public UnityButtonInput buttons;
    public UnityScreen screenRenderer;
    public UnityAudioChannel[] channels;
    [FormerlySerializedAs("gbCartImpl")] public UnityGBCartEvents iGBCartEvents;
    public Text frameCounterTxt;
    [ValueDropdown(nameof(GetAllRomsInSA))]
    public string romName;

    public GBSettings settings;

    private StringBuilder sb = new StringBuilder(64);
    private GBMB motherboard;
    private double idleTime = 0;
    private ProfilerRecorder fpsRecorder;
    private List<ProfilerRecorderSample> fpsValues;

    // Use this for initialization
    void Start () {
#if UNITY_ANDROID
        Application.targetFrameRate = 60;
        // Disable screen dimming
        Screen.sleepTimeout = 300;
#endif
#if UNITY_EDITOR
        StartROM(romName);
#endif
        
        StartCoroutine(ReallyLateUpdate());
        
        fpsRecorder = ProfilerRecorder.StartNew(ProfilerCategory.Internal, "Main Thread", 60);
        fpsValues = new List<ProfilerRecorderSample>(60);
    }

    void Update()
    {
        if (motherboard != null)
        {
            motherboard.ProcessFrame();
        }
    }

    private IEnumerator ReallyLateUpdate()
    {
        while (true)
        {
            //waits till really late in the update process
            yield return new WaitForEndOfFrame();
            if (motherboard != null)
            {
                idleTime = motherboard.SyncFrameRate();
            }

            UpdateFrameCounter();
        }
    }

    /// <summary>
    /// Filename with extension as romname
    /// </summary>
    public void StartROM(string romName)
    {
        if(motherboard != null)
        {
            motherboard.WriteSavedataToDisk();
            motherboard.Destroy();
        }

        byte[] gameRom = OpenGameboyROM(romName);
        byte[] bootRom = null;
        if (!settings.skipBootrom)
        {
            bootRom = OpenBootROM("GBBOOT_DMG_ROM.bin");
        }
        
        string savePath = Path.Join(Application.persistentDataPath,romName);
        savePath = Path.ChangeExtension(savePath, ".usav");
        Debug.Log($"Save path: {savePath}");

        motherboard = new GBMB(screenRenderer, buttons, channels, iGBCartEvents, settings, savePath, gameRom, bootRom);
    }
    
    private byte[] OpenGameboyROM(string romName)
    {
        string romPath = Path.Join(Application.streamingAssetsPath,romName);
        byte[] gbRom = null;
#if UNITY_ANDROID && !UNITY_EDITOR
        var request = UnityWebRequest.Get(romPath);
        request.SendWebRequest();
        while (!request.isDone)
        {
            if (request.result != UnityWebRequest.Result.InProgress)
            {
                break;
            }
        }
        if (request.result == UnityWebRequest.Result.Success)
        {
            if (request.downloadedBytes > 16777216)
            {
                Debug.Log("ROM FILE TOO BIG");
            }
            else
            {
                gbRom = request.downloadHandler.data;
                Debug.Log("Loaded Rom " + gbRom.Length + " bytes.");
            }
        }
#else
        if (!File.Exists(romPath))
        {
            Debug.LogError($"Rom file not found! Path wrong? {romPath}");
            return null;
        }
        using (FileStream fs = new FileStream(romPath, FileMode.Open))
        {
            if(fs.Length > 16777216)
            {
                Debug.Log("ROM FILE TOO BIG");
            }
            else
            {
                gbRom = new byte[fs.Length];
                int bytesWritten = fs.Read(gbRom, 0, (int)fs.Length);
                Debug.Log("Loaded Rom " + bytesWritten + " bytes.");
            }
        }
#endif
        return gbRom;
    }
    
    private byte[] OpenBootROM(string bootRomName)
    {
        string path = Path.Join(Application.streamingAssetsPath,bootRomName);
        byte[] bootRom = null;
#if UNITY_ANDROID
        var request = UnityWebRequest.Get(path);
        request.SendWebRequest();
        while (!request.isDone)
        {
            if (request.result != UnityWebRequest.Result.InProgress)
            {
                break;
            }
        }
        if (request.result == UnityWebRequest.Result.Success)
        {
            bootRom = request.downloadHandler.data;
        }
#else
        using (FileStream fs = new FileStream(path, FileMode.Open))
        {
            bootRom = new byte[fs.Length];
            fs.Read(bootRom, 0, (int)fs.Length);
        }
#endif
        if (bootRom.Length == 0)
            return null;
        return bootRom;
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            if (motherboard != null)
            {
                motherboard.WriteSavedataToDisk();
            }
        }
    }

    private void OnApplicationQuit()
    {
        if (motherboard != null)
        {
            motherboard.WriteSavedataToDisk();
        }
    }

    private int updateFrameCounter = 0;
    private void UpdateFrameCounter()
    {
        //don't update so often
        if (updateFrameCounter != 0)
        {
            updateFrameCounter--;
            return;
        }
        updateFrameCounter = 5;
        
        fpsValues.Clear();
        fpsRecorder.CopyTo(fpsValues);
        double frameTime = 0;
        for (var i = 0; i < fpsValues.Count; ++i)
            frameTime += fpsValues[i].Value;
        frameTime /= fpsValues.Count;
        frameTime /= 1000000;

        double framePerc = (16.7427062988 / frameTime) * 100;//percentage of desired framerate of 59.727500569606

        sb.Clear();
        sb.Append(frameTime.ToString("#.00"));
        sb.Append("ms\n");
        sb.Append(framePerc.ToString("#.0"));
        sb.Append("%\nidle ");
        sb.Append(idleTime.ToString("#.00"));
        sb.Append("ms\n");
        frameCounterTxt.text = sb.ToString();
    }

    private static IEnumerable GetAllRomsInSA()
    {
        string path = Application.streamingAssetsPath;
        int lengthAssetPath = path.Length + 1;
        return DirSearch(path, lengthAssetPath);
    }

    public static List<string> DirSearch(string sDir, int cropLength = 0)
    {
        List<string> results = new List<string>();
        foreach (string f in Directory.GetFiles(sDir))
        {
            if (f.EndsWith(".gb") || f.EndsWith(".gbc"))
            {
                string shortName = f.Remove(0, cropLength);
                results.Add(shortName);
            }
        }

        foreach (string d in Directory.GetDirectories(sDir))
        {
            results.AddRange(DirSearch(d, cropLength));
        }
        return results;
    }
}
