using UnityEngine;

public class UnityGBCartEvents : MonoBehaviour, IGBCartEvents
{
    private bool edgeDetect = false;
    
    private void Awake()
    {
#if UNITY_ANDROID
        Vibration.Init();
#endif
    }

    public void SetRumble(bool enabled)
    {
        //prevent spamming of the bool making a lot of native calls
        if (edgeDetect != enabled)
        {
            edgeDetect = enabled;
#if UNITY_ANDROID
            if (enabled)
            {
                Vibration.VibrateAndroid(5000);
            }
            else
            {
                Vibration.CancelAndroid();
            }  
#endif
        }
    }
}
