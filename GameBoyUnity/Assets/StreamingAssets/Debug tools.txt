https://gbdev.gg8.se/wiki/articles/Test_ROMs

GBBOOT_DMG_ROM: PASSED
cpu_instr and 01-11: PASSED
instr_timing: PASSED
CPU_REGS: PASSED (with and without bootrom)
DMG_ACID2: PASSED

DMG_sound: GB specific audio test - PASS 1 2 3 4 6 7, FAIL REST

halt_bug: tests a halt bug - FAILED 2A6CE34B
interrupt_time: tests timing on interrupts - FAILED 1B9D7039
mem_timing: tests memory timing - FAILED 1-2-3 all on 01
mem_timing 1: tests memory timing - FAILED 01?
oam_bug: tests OAM bug - FAILED 01 02 04 05 07 08

MBC1: FAILED MultiCard8MB rest PASSED
MBC2: PASSED
MBC3: PASSED - Failed RTC!
MBC5: PASSED

GB notes: 1050Hz - 2081Hz
My Notes: 1046Hz - 2001Hz

Bugs:
AUDIO does NOT sync with accuracy, it sycns per frame. Solution would be:
Store audio state start of frame
process register changes live
store register changes and clocks since start of frame
store end of frame, and reset to start of frame state
when audio buffer is requested at end of frame, take the total clocks it took, and divide that to the frame buffer, and actually re-process the audio given start of frame state
leave audio in end of frame state as intended
It'll be a bit nutty.

Timing of audio should share TIMA clock or something? unsure.
A lot of visual bugs remain
noise channel frequency/deepness is too deep? not as light as it should be. Maybe too much noise/noise method.